#pragma once
#ifdef D2DUIFRAMEWORK_DLL_EXPORTS
#define D2DUIFRAMEWORK_DLL_API __declspec(dllexport)
#else
#define D2DUIFRAMEWORK_DLL_API __declspec(dllimport)

#endif

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
// Windows 헤더 파일:
#include <windows.h>
#include <iostream>
using std::cout;
using std::endl;

#include <math.h>
#include <time.h>
#include <conio.h>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <unordered_map>
#include <thread>
#include <atomic>
#include <algorithm>

//Folder File List
#include <filesystem>
namespace fs = std::experimental::filesystem;

//File Drag & Drop
#include <shellapi.h>

// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <wrl.h>
using Microsoft::WRL::ComPtr;

//DirectX3D
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXCollision.h>
#include <DirectXColors.h>
using namespace DirectX;
using namespace DirectX::PackedVector;
#define XMASSERT assert 
#define XMVectorPermute( a, b, c) XMVectorPermute(a,b, c.i[0], c.i[1], c.i[2], c.i[3] )
#define _DECLSPEC_ALIGN_16_ __declspec(align(16))
#include <d3d11_4.h>
#include <d3dcompiler.h> 
#include <dxgi1_4.h>
#include <DxErr.h>
#include <DirectXTex.h>
//	#include <DDSTextureLoader.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "DXErr.lib")
#pragma comment(lib, "dxgi.lib")
//stdio.h 대용품
#pragma comment(lib, "legacy_stdio_definitions.lib")
#ifdef _DEBUG
	#pragma comment(lib, "DirectXTex_Debug.lib")
#else
	#pragma comment(lib, "DirectXTex.lib")
#endif

//Direct2D
#include <d2d1_2.h>
#include <dwrite.h>
#pragma comment(lib, "D2D1.lib")
#pragma comment(lib, "dwrite.lib")

//IME
#include <imm.h>
#pragma comment(lib, "imm32")


#define	SAFE_RELEASE(x)		if(x) x->Release(); 

#define HR(x)                                              \
{                                                          \
	HRESULT hr = (x);                                      \
	if(FAILED(hr))                                         \
	{                                                      \
		DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, true); \
		exit(-1);										   \
	}                                                      \
}

template<class T>
void CreateConstantBuffers(ID3D11Device * pDevice, ID3D11Buffer **pcbBuffer) {
	D3D11_BUFFER_DESC d3dBufferDesc;
	ZeroMemory(&d3dBufferDesc, sizeof(D3D11_BUFFER_DESC));

	d3dBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	d3dBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	d3dBufferDesc.ByteWidth = sizeof(T);
	d3dBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	HR(pDevice->CreateBuffer(&d3dBufferDesc, NULL, pcbBuffer));
}

template<class T>
void UpdateConstantBuffer(ID3D11DeviceContext *pDeviceContext, ID3D11Buffer *pcbBuffer, const T& Data) {
	D3D11_MAPPED_SUBRESOURCE MappedResource;
	pDeviceContext->Map(pcbBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);
	T* pData = (T*)MappedResource.pData;
	memcpy(pData, &Data, sizeof(T));
	pDeviceContext->Unmap(pcbBuffer, 0);
}

#define VERIFY(x) assert(x)
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

constexpr double PI = 3.14159265358979323846;
constexpr double CIRCLE_RAD = PI * 2;
constexpr double CIRCLE_THIRD = CIRCLE_RAD / 3.0;
constexpr double CIRCLE_THIRD_1 = 0;
constexpr double CIRCLE_THIRD_2 = CIRCLE_THIRD;
constexpr double CIRCLE_THIRD_3 = CIRCLE_THIRD * 2;

constexpr byte VK_0				= 0X30;
constexpr byte VK_1				= 0X31;
constexpr byte VK_2				= 0X32;
constexpr byte VK_3				= 0X33;
constexpr byte VK_4				= 0X34;
constexpr byte VK_5				= 0X35;
constexpr byte VK_6				= 0X36;
constexpr byte VK_7				= 0X37;
constexpr byte VK_8				= 0X38;
constexpr byte VK_9				= 0X39;
constexpr byte VK2_0			= 0X60;
constexpr byte VK2_1			= 0X61;
constexpr byte VK2_2			= 0X62;
constexpr byte VK2_3			= 0X63;
constexpr byte VK2_4			= 0X64;
constexpr byte VK2_5			= 0X65;
constexpr byte VK2_6			= 0X66;
constexpr byte VK2_7			= 0X67;
constexpr byte VK2_8			= 0X68;
constexpr byte VK2_9			= 0X69;
constexpr byte VK_A				= 0x41;
constexpr byte VK_B				= 0x42;
constexpr byte VK_C				= 0x43;
constexpr byte VK_D				= 0x44;
constexpr byte VK_E				= 0x45;
constexpr byte VK_F				= 0x46;
constexpr byte VK_G				= 0x47;
constexpr byte VK_H				= 0x48;
constexpr byte VK_I				= 0x49;
constexpr byte VK_J				= 0x4A;
constexpr byte VK_K				= 0x4B;
constexpr byte VK_L				= 0x4C;
constexpr byte VK_M				= 0x4D;
constexpr byte VK_N				= 0x4E;
constexpr byte VK_O				= 0x4F;
constexpr byte VK_P				= 0x50;
constexpr byte VK_Q				= 0x51;
constexpr byte VK_R				= 0x52;
constexpr byte VK_S				= 0x53;
constexpr byte VK_T				= 0x54;
constexpr byte VK_U				= 0x55;
constexpr byte VK_V				= 0x56;
constexpr byte VK_W				= 0x57;
constexpr byte VK_X				= 0x58;
constexpr byte VK_Y				= 0x59;
constexpr byte VK_Z				= 0x5A;
constexpr byte KEY_FOWARD		= 1<<0;
constexpr byte KEY_BACKWARD		= 1<<1;
constexpr byte KEY_LEFTWARD		= 1<<2;
constexpr byte KEY_RIGHTWARD	= 1<<3;
constexpr byte KEY_UPWARD		= 1<<4;
constexpr byte KEY_DOWNWARD		= 1<<5;
constexpr byte KEY_RUN			= 1<<6;
