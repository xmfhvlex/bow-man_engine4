#pragma once
#ifdef D2DUIFRAMEWORK_DLL_EXPORTS
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllexport)
#else
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllimport)
#endif

#define MAX_LENGTH	128

struct ChatData;

class CImeTextSystem{
private:
	HIMC		m_hIMC;
	int			m_nPivot				= 0;
	bool		m_bIme					= false;
		
	WCHAR		m_wcText[MAX_LENGTH];

	ChatData	*m_pChatData			= nullptr;

public:
	CImeTextSystem(HWND hWnd);
	~CImeTextSystem(){}

	void D2DUIFRAMEWORK_DLL_API Initialize(HWND hWnd){
	//	m_hIMC = ImmGetContext(hWnd);
	//	CreateCaret(hWnd, NULL, 2, 15);
	//	ShowCaret(hWnd);
	}
	void D2DUIFRAMEWORK_DLL_API Destroy(HWND hWnd){
	//	ImmReleaseContext(hWnd, m_hIMC);
	}

	int D2DUIFRAMEWORK_DLL_API TextProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

	void D2DUIFRAMEWORK_DLL_API SetChatList(ChatData *pChatList){
		m_pChatData = pChatList;	
	}

	WCHAR D2DUIFRAMEWORK_DLL_API * GetStr(){
		return m_wcText;		
	}
};