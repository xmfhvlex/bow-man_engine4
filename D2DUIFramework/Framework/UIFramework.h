#pragma once
#ifdef D2DUIFRAMEWORK_DLL_EXPORTS
#define D2DUIFRAMEWORK_DLL_API __declspec(dllexport)
#else
#define D2DUIFRAMEWORK_DLL_API __declspec(dllimport)
#endif

#include "D2DUIFramework.h"
#include "ImeFramework.h"
#include "UiComponent.h"
#include "UIObject.h"
#include "UIObjectManager.h"