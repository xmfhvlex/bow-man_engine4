#pragma once
#ifdef D2DUIFRAMEWORK_DLL_EXPORTS
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllexport)
#else
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllimport)

#endif
//#include "ChatDataBase.h"


const int gElementSizeY = 20;
const int gInterval = 2;

enum UiCoordType : int{
	LEFT_WARD		= 1,
	RIGHT_WARD		= 2,
	DOWN_WARD		= 4,
	UP_WARD			= 8,
	SYNC_X_SIZE		= 16,
	SYNC_Y_SIZE		= 32,
	RIGHT_MOST		= 64,
	DOWN_MOST		= 128,
	NOT_LINKED		= 256,		//이 UI가 다음 UI의 좌표값에 연관하지 않을 때
};

enum UiElement : int{
	TITLE		= 1,
	RESIZE		= 2,
	EXIT		= 4,
	MINIMIZE	= 8,
	ON_TOP		= 16,
};


class CDataType {
private:

public:
	CDataType() {}
	virtual ~CDataType() {}

	virtual const type_info& GetType() const = 0;
	virtual void PrintData() = 0;
	virtual std::wstring ToString() = 0;
};

template<typename TYPE>
class CRefDataType : public CDataType {
private:
	std::reference_wrapper<TYPE> m_Data;

public:
	CRefDataType(TYPE& Data) : m_Data(Data) {}
	virtual ~CRefDataType() {}

	const type_info& GetType() const override { return typeid(TYPE); }
	void PrintData() override { 
		m_Data++;
		cout << m_Data << endl; 
	}

	std::wstring ToString() override {
		WCHAR buff[100];
		wsprintf(buff, L"%d", m_Data.get());
		return buff;
	}
//	TYPE& GetData() { return m_Data; }
};

//using UpdateMethodData = std::pair<std::shared_ptr<Entity>, std::function<bool(void)>>;

class CUiObject{
private:
	//void					*m_pArrData[4];
	XMFLOAT2				m_vPosition		= XMFLOAT2(0, 0);
	XMFLOAT2				m_vSize			= XMFLOAT2(20, 20);
	CUiObject				*m_pSuperParent	= this;
	CUiObject				*m_pParent		= this;
	//CUiObject				**m_ppOuterInterface = nullptr;

	std::list<CUiObject*>	m_lpChild;
	
	//Behavior
	std::function<int(CUiObject * pThis, XMFLOAT2 & pvPos)>	 FuncCoordUpdate;
	std::function<void(CUiObject * pThis)> FuncUpdate;
	std::function<void(CUiObject * pThis, ID2D1RenderTarget * pD2DRenderTarget)> FuncRender;

	//bool					(*FuncMouseInput)(CUiObject* pThis, HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);
	//void					(*FuncKeyInput)(CUiObject* pThis, HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

	CDataType*				_data;

public:
	UINT m_uCoordAttri	= 0; 
	inline static CUiObject*		m_pSelected;
	inline static CUiObject*		m_pDragged;
	inline static CUiObject*		m_pSelectedChild;
	inline static CUiObject*		m_pMouseOnObject;
	inline static int				m_nMinSizeX = 200;
	inline static int				m_nMinSizeY = 20;
	inline static int				m_nIntervalX = 2;
	inline static int				m_nIntervalY = 2;

public:
	D2DUIFRAMEWORK_DLL_API CUiObject();
	D2DUIFRAMEWORK_DLL_API CUiObject(UINT uiCoordAttribute) : CUiObject(){
		m_uCoordAttri = uiCoordAttribute;
	}
	D2DUIFRAMEWORK_DLL_API ~CUiObject(){}

	template<typename T>
	D2DUIFRAMEWORK_DLL_API void SetData(T& data) {
		_data = new CRefDataType(data);
	}

	D2DUIFRAMEWORK_DLL_API CDataType* GetData() {
		return _data;
	}

	D2DUIFRAMEWORK_DLL_API void Create(){}
	D2DUIFRAMEWORK_DLL_API void Release(){
		for(CUiObject* pChild : m_lpChild) 
			pChild->Release();
	//	(m_ppOuterInterface) ? (*m_ppOuterInterface) = nullptr : NULL;
		delete this;
	}

	D2DUIFRAMEWORK_DLL_API void D2DUIFRAMEWORK_DLL_APIMouseInput(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam){
	//	FuncMouseInput(this, hWnd, message, wParam, lParam);
	}

	D2DUIFRAMEWORK_DLL_API void KeyInput(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam){
	//	FuncKeyInput(this, hWnd, message, wParam, lParam);
	}

	D2DUIFRAMEWORK_DLL_API void RearrangeCoord();

	D2DUIFRAMEWORK_DLL_API void Update(){
	//	FuncUpdate(this);
		for(CUiObject* pChild : m_lpChild) pChild->Update();
	}
	D2DUIFRAMEWORK_DLL_API void Render(ComPtr<ID2D1RenderTarget> d2DRenderTarget);

	D2DUIFRAMEWORK_DLL_API void Move(XMFLOAT2 vPosition);

	D2DUIFRAMEWORK_DLL_API bool Collision(const XMFLOAT2& vPos){
	//	if(vPos.x<m_vPosition.x || m_vPosition.x+m_vSize.x<vPos.x) return false;
	//	if(vPos.y<m_vPosition.y || m_vPosition.y+m_vSize.y<vPos.y) return false;
		return true;
	}

	D2DUIFRAMEWORK_DLL_API void AddChild(CUiObject* pChild, bool bAddSize=true){
		pChild->SetParent(this);
		pChild->SetSuperParent(m_pSuperParent);
		m_lpChild.push_back(pChild);
		if(bAddSize)
			AddSize(pChild);
		else
			RearrangeCoord();
	}
	D2DUIFRAMEWORK_DLL_API void AddSize(CUiObject* pChild) {
		XMFLOAT2 vChildSize = pChild->GetSize();
		XMFLOAT2 vSize = GetSize();
	//	vSize.x += vChildSize.x;
		vSize.y += (vChildSize.y + gInterval);
		m_pParent->SetSize(vSize);
	}

	//Setter
	template<class T> 
	D2DUIFRAMEWORK_DLL_API void SetData(T *pData, int index = 0){
	//	m_pArrData[index] = static_cast<void*>(pData);	
	}
	D2DUIFRAMEWORK_DLL_API void SetSuperParent(CUiObject* pSuperParent) {
		m_pSuperParent = pSuperParent;
		for (CUiObject* pChild : m_lpChild)
			pChild->SetSuperParent(pSuperParent);
	}
	D2DUIFRAMEWORK_DLL_API void SetParent(CUiObject* pParent){
		m_pParent = pParent;
	}
	D2DUIFRAMEWORK_DLL_API void SetOuterInterface(CUiObject** ppOuterInterface) {
	//	m_ppOuterInterface = ppOuterInterface;
	}
	D2DUIFRAMEWORK_DLL_API void SetPosition(const XMFLOAT2 & vPosition){
		m_vPosition = vPosition;	
		RearrangeCoord();
	}
	D2DUIFRAMEWORK_DLL_API void SetPosition(const int nPosX, const int nPosY){
		SetPosition(XMFLOAT2(static_cast<float>(nPosX), static_cast<float>(nPosY)));
	}
	D2DUIFRAMEWORK_DLL_API void SetSize(const XMFLOAT2& vSize){
		m_vSize = vSize;
		RearrangeCoord();
	}
	D2DUIFRAMEWORK_DLL_API void SetSize(const int nSizeX, const int nSizeY){
		SetSize(XMFLOAT2(nSizeX, nSizeY));	
	}
	D2DUIFRAMEWORK_DLL_API void SetSizeX(const int nSizeX) {
		m_vSize.x = nSizeX;
	}
	D2DUIFRAMEWORK_DLL_API void SetCoordFuncUpdate(int (*pFunc)(CUiObject* pUiObject, XMFLOAT2& pvPos)) {
	//	FuncCoordUpdate = pFunc;
	}
	D2DUIFRAMEWORK_DLL_API void SetUpdateFunc(void (*pFunc)(CUiObject* pUiObject)){
	//	FuncUpdate = pFunc;
	}
	D2DUIFRAMEWORK_DLL_API void SetRenderFunc(void (*pFunc)(CUiObject* pUiObject, ID2D1RenderTarget *pD2DRenderTarget)){
		FuncRender = pFunc;
	}
	D2DUIFRAMEWORK_DLL_API void SetMouseInputFunc(bool (*pFunc)(CUiObject* pUiObject, HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)) {
	//	FuncMouseInput = pFunc;
	}
	D2DUIFRAMEWORK_DLL_API void SetKeyInputFunc(void (*pFunc)(CUiObject* pUiObject, HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)) {
	//	FuncKeyInput = pFunc;
	}

	//Normal Getter
	//D2DUIFRAMEWORK_DLL_API void * GetData(int index = 0){
	////	return m_pArrData[index];	
	//	return nullptr;
	//}
	D2DUIFRAMEWORK_DLL_API CUiObject * GetSuperParent(){
		return m_pSuperParent;
	}
	D2DUIFRAMEWORK_DLL_API CUiObject * GetParent(){
		return m_pParent;	
	}
	D2DUIFRAMEWORK_DLL_API const XMFLOAT2 & GetPosition(){
		return m_vPosition;
	}
	D2DUIFRAMEWORK_DLL_API const XMFLOAT2 & GetSize(){
		return m_vSize;
	}

	D2DUIFRAMEWORK_DLL_API bool MouseInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, bool bCollision = true) {
		if (bCollision) {
			XMFLOAT2 vMousePos(LOWORD(lParam), HIWORD(lParam));

			if (Collision(vMousePos) == false)
				return false;
		}

		for (CUiObject* pChild : m_lpChild)
			if (pChild->MouseInputProc(hWnd, message, wParam, lParam))
				return true;

		return true;
	//	return FuncMouseInput(this, hWnd, message, wParam, lParam);
	}
};