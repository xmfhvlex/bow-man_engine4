#pragma once
#ifdef D2DUIFRAMEWORK_DLL_EXPORTS
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllexport)
#else
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllimport)

#endif

class CTexture;
class CShaderPack;
class CUiObject;
class CImeTextSystem;

__declspec(selectany) bool			gbLMouseDown = false;
__declspec(selectany) XMFLOAT2		gvMousePrev;
__declspec(selectany) CTexture*		gpDraggedTexture = nullptr;
__declspec(selectany) CShaderPack*	gpDraggedShaderPack = nullptr;

#define UiManager				CUiManager::GetInstance()
#define SystemMessage(wText)	CUiManager::GetInstance()->AddSystemMessage(wText)

#define WM_MYMSG	0x1000

class CUiManager{
private:
	inline static CUiManager *m_pInstance = nullptr;

	std::list<CUiObject*>	m_lpUiObject;
	CUiObject*				m_pTextureDataBase;

	/*ChatData				m_SystemChatData;*/
	//CImeTextSystem			*m_pSystemChat;

private:
	CUiManager(){}
	~CUiManager(){}

public:
	static CUiManager *GetInstance(){
		if (m_pInstance == nullptr) {
			m_pInstance = new CUiManager;
		}
		return m_pInstance;
	}

	D2DUIFRAMEWORK_DLL_API void Initialize(HWND hWnd);
	D2DUIFRAMEWORK_DLL_API void Release();

	D2DUIFRAMEWORK_DLL_API CUiObject * CreateUi(std::wstring wsTitle, UINT UiElement, bool bParentFlag = true);
	D2DUIFRAMEWORK_DLL_API void AddUi(CUiObject* pUiObject);
	D2DUIFRAMEWORK_DLL_API bool RemoveUi(CUiObject* pUiObject);

	D2DUIFRAMEWORK_DLL_API void SetUiOnTop(CUiObject* pUiObject);

//		static_cast<WPARAM>(CUiObject::m_pMouseOnObject);
//		SendMessage(hWnd, WM_MYMSG, NULL, NULL);

	D2DUIFRAMEWORK_DLL_API void Update(float fElapsedTime);
	D2DUIFRAMEWORK_DLL_API void Render(ComPtr<ID2D1RenderTarget> d2DRenderTarget);

	D2DUIFRAMEWORK_DLL_API bool MouseInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	D2DUIFRAMEWORK_DLL_API bool KeyInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	D2DUIFRAMEWORK_DLL_API void AddSystemMessage(const std::wstring & sysMsg){
//		m_SystemChatData.AddSystemMessage(sysMsg);
	}
};
