#pragma once
#ifdef D2DUIFRAMEWORK_DLL_EXPORTS
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllexport)
#else
	#define D2DUIFRAMEWORK_DLL_API __declspec(dllimport)
#endif

//dummy class
class CUiManager;

enum class BRUSH_COLOR : int{
	WHITE,
	BLACK,
	RED,
	GREEN,
	BLUE,
	MAGENTA,
	YELLOW,
	CYAN,
	WHITE_A25,
	WHITE_A50,
	WHITE_A75,
};

//__declspec(selectany) ID2D1RenderTarget						*gpD2DRenderTarget = nullptr;
//__declspec(selectany) IDWriteTextFormat						*gpTextFormat = nullptr;
//__declspec(selectany) IDWriteTextFormat						*gpWorldTextFormat = nullptr;
//__declspec(selectany) std::vector<ID2D1SolidColorBrush*>		*gpvpD2DBrush;
//__declspec(selectany) ID2D1SolidColorBrush*					gpD2DBrush = nullptr;
//__declspec(selectany) ID3D11ShaderResourceView				*gpShaderResource = nullptr;

struct ChatData;

//Direct 2D Framework
#define UIFramework	D2DUIFramework::GetInstance()

class D2DUIFramework
{
private:
	D2DUIFramework(){
		//m_pRenderTarget		= nullptr;
		//m_pD2DFactory		= nullptr;
		//m_pWirteFactory		= nullptr;
		//m_pTextFormat		= nullptr;
	}
	~D2DUIFramework(){
		Release();
	}

public:
	D2DUIFRAMEWORK_DLL_API static D2DUIFramework* GetInstance(){
		if (m_pInstance == nullptr) {
			m_pInstance = new D2DUIFramework;
		}
		return m_pInstance;	
	}

//	void Initialize(HWND hWnd, IDXGISwapChain* pDXGISwapChain);
	D2DUIFRAMEWORK_DLL_API void Initialize(HWND hWnd, ComPtr<ID3D11Device> d3dDevice, ComPtr<ID3D11DeviceContext> d3dDeviceContext, ComPtr<ID3D11Texture2D> backBuffer);
	D2DUIFRAMEWORK_DLL_API void Release(){
		_renderTarget->Release();
		_d2DFactory->Release();
		_wirteFactory->Release();
		_textFormat->Release();

		//SAFE_RELEASE(m_pRenderTarget);
		//SAFE_RELEASE(m_pD2DFactory);
		//SAFE_RELEASE(m_pWirteFactory);
		//SAFE_RELEASE(m_pTextFormat);
		//for(ID2D1SolidColorBrush *pD2DBrush : m_vpD2DBrush) SAFE_RELEASE(pD2DBrush);
	}

	D2DUIFRAMEWORK_DLL_API void Update(float fElapsedTime);
	D2DUIFRAMEWORK_DLL_API void Render();

//	ID2D1DeviceContext D2DUIFRAMEWORK_DLL_API * GetDeviceContext() {
////		return m_pD2DeviceContext;
//		return nullptr;
//	}
//	ID2D1RenderTarget D2DUIFRAMEWORK_DLL_API * GetRenderTarget() {
////		return m_pRenderTarget;
//		return nullptr;
//	}

	D2DUIFRAMEWORK_DLL_API bool MouseInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	D2DUIFRAMEWORK_DLL_API bool KeyInputProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

	D2DUIFRAMEWORK_DLL_API CUiManager* GetUIManager();

private:
	inline static D2DUIFramework* m_pInstance = nullptr;

	ComPtr<ID3D11Device>						_d3dDevice = nullptr;
	ComPtr<ID3D11DeviceContext>					_d3dDeviceContext = nullptr;

	ComPtr<ID2D1DeviceContext>					_d2dDeviceContext = nullptr;
	ComPtr<ID2D1Factory>						_d2DFactory = nullptr;
	ComPtr<IDWriteFactory>						_wirteFactory = nullptr;



	//_declspec(selectany) ID3D11Device* gpDevice = nullptr;
	//_declspec(selectany) UINT SCREEN_WIDTH = 0;
	//_declspec(selectany) UINT SCREEN_HEIGHT = 0;
	//inline HWND gHwnd;
	//inline HINSTANCE gHinstance;
	//constexpr int Max_Entity_Count = 100'000;

public:
	inline static UINT _nScreenWidth = 0;
	inline static UINT _nScreenHeight = 0;
	inline static ComPtr<IDWriteTextFormat>	_textFormat = nullptr;
	inline static ComPtr<ID2D1SolidColorBrush> _solidBrush = nullptr;
	inline static std::vector< ComPtr<ID2D1SolidColorBrush>> _vd2DBrush;
	inline static ComPtr<ID2D1RenderTarget> _renderTarget = nullptr;
};


//ID2D1Bitmap * LoadTexture(const WCHAR * pwTextureFile);
