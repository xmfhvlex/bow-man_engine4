#define D2DUIFRAMEWORK_DLL_EXPORTS

#include "stdafx.h"
#include "UiObject.h"
#include "D2DUIFramework.h"
#include "UiComponent.h"
//#include "ImeFramework.h"


CUiObject::CUiObject() {
	FuncCoordUpdate	= UpdateCoord;
	//FuncUpdate			= UpdateDefault;
	FuncRender = RenderDefault;
	//FuncMouseInput		= MouseInputDefault;
	//FuncKeyInput		= KeyInputDefault;
}

void CUiObject::RearrangeCoord(){
	if(m_pSuperParent == this){
		if(m_vPosition.x<0) m_vPosition.x = 0;
		if(m_vPosition.y<0) m_vPosition.y = 0;
		if(m_vPosition.x> D2DUIFramework::_nScreenWidth-m_vSize.x) m_vPosition.x = D2DUIFramework::_nScreenWidth-m_vSize.x;
		if(m_vPosition.y> D2DUIFramework::_nScreenHeight-m_vSize.y) m_vPosition.y = D2DUIFramework::_nScreenHeight-m_vSize.y;
	}

	XMFLOAT2 vNewPos = GetPosition();
	for(CUiObject* pChild : m_lpChild){
		pChild->FuncCoordUpdate(pChild,vNewPos);
		pChild->RearrangeCoord();
	}
}

void CUiObject::Render(ComPtr<ID2D1RenderTarget> d2DRenderTarget){
	float fMinX = m_vPosition.x + 2;
	float fMinY = m_vPosition.y + 2;
	float fMaxX = m_vPosition.x+m_vSize.x - 2;
	float fMaxY = m_vPosition.y+m_vSize.y - 2;

	if(m_pSuperParent == this)
		d2DRenderTarget->PushAxisAlignedClip(D2D1::RectF(fMinX-1,fMinY-1,fMaxX+1,fMaxY+1),D2D1_ANTIALIAS_MODE_PER_PRIMITIVE);
	
	FuncRender(this, d2DRenderTarget.Get());

	for(CUiObject* pChild : m_lpChild) 
		pChild->Render(d2DRenderTarget);

	if (m_pSuperParent == this) {
		d2DRenderTarget->PopAxisAlignedClip();

		//D2DUIFramework::_solidBrush->SetColor(D2D1_COLOR_F{ 0, 0, 0, 1.3f });
		//float fParentMinX = m_pSuperParent->GetPosition().x + 2;
		//float fParentMinY = m_pSuperParent->GetPosition().y + 2;
		//float fParentMaxX = m_pSuperParent->GetPosition().x + m_pSuperParent->GetSize().x - 2;
		//float fParentMaxY = m_pSuperParent->GetPosition().y + m_pSuperParent->GetSize().y - 2;
	//	d2DRenderTarget->DrawRectangle(D2D1_RECT_F{ fParentMinX, fParentMinY, fParentMaxX, fParentMaxY }, D2DUIFramework::_solidBrush.Get(), 1);
	//	d2DRenderTarget->DrawRoundedRectangle(D2D1_ROUNDED_RECT{ D2D1_RECT_F{ fParentMinX, fParentMinY, fParentMaxX, fParentMaxY } , 10, 10}, D2DUIFramework::_solidBrush.Get());
	}
	//if (m_pSelected == this) {
	//	D2DUIFramework::_solidBrush->SetColor(D2D1_COLOR_F{ 1, 1, 1, 1 });
	//	float fParentMinX = m_pSuperParent->GetPosition().x + 2;
	//	float fParentMinY = m_pSuperParent->GetPosition().y + 2;
	//	float fParentMaxX = m_pSuperParent->GetPosition().x + m_pSuperParent->GetSize().x - 2;
	//	float fParentMaxY = m_pSuperParent->GetPosition().y + m_pSuperParent->GetSize().y - 2;
	//	d2DRenderTarget->DrawRectangle(D2D1_RECT_F{ fParentMinX, fParentMinY, fParentMaxX, fParentMaxY }, D2DUIFramework::_solidBrush.Get(), 1);
	//}
}

void CUiObject::Move(XMFLOAT2 vPosition){
	m_vPosition.x += vPosition.x;
	m_vPosition.y += vPosition.y;

	if(m_vPosition.x<0) m_vPosition.x = 0;
	if(m_vPosition.y<0) m_vPosition.y = 0;
	if(m_vPosition.x> D2DUIFramework::_nScreenWidth-m_vSize.x) m_vPosition.x = D2DUIFramework::_nScreenWidth -m_vSize.x;
	if(m_vPosition.y> D2DUIFramework::_nScreenHeight-m_vSize.y) m_vPosition.y = D2DUIFramework::_nScreenHeight -m_vSize.y;
}
