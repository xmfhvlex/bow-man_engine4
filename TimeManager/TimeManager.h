#pragma once

#ifdef TIME_MANAGER_DLL_EXPORTS
#define TIME_MANAGER_DLL_API __declspec(dllexport)
#else
#define TIME_MANAGER_DLL_API __declspec(dllimport)
#endif

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <stdlib.h>
#include <iostream>

#define TimeMgr TimeManager::GetInstance()

class TimeManager {
private:
	TimeManager() {}
	~TimeManager() {}

public:
	static TIME_MANAGER_DLL_API TimeManager* GetInstance();

	//No Lock On (nLockFPS = 0)
	void TIME_MANAGER_DLL_API Initialize(int nLockFPS = 0);

	float TIME_MANAGER_DLL_API Update();

	float TIME_MANAGER_DLL_API GetDeltaTime();

	float TIME_MANAGER_DLL_API GetAvgDeltaTime();

private:
	inline static TimeManager* m_pInstance;

	// Time Lock이 유지된 시간
	double		_waitTime = 0;
	// Wait Time + Advance Time (Scene Update에 사용되는 최종 Elapsed Time)
	double		_deltaTime = -1;

	double		_secondsPerCount = 0;

	// Time Lock 시간
	double		_lockTime = 0;

	int			_sumCount = 0;
	double		_sumDeltaTime = 0;
	double		_avgDeltaTime = 0;

	__int64		_prevTime = 0;
	__int64		_currTime = 0;
};
