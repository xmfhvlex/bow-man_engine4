#define TIME_MANAGER_DLL_EXPORTS

#include "TimeManager.h"
#include <stdexcept>

//CTimeManager* CTimeManager::m_pInstance = nullptr;

TimeManager * TimeManager::GetInstance()
{
	if (m_pInstance == nullptr) {
		m_pInstance = new TimeManager;
		m_pInstance->Initialize();
	}
	return m_pInstance;
}

void TimeManager::Initialize(int nLockFPS)
{
	__int64 countsPereSec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPereSec);
	_secondsPerCount = 1.0 / static_cast<double>(countsPereSec);
	QueryPerformanceCounter((LARGE_INTEGER*)&_prevTime);
	_currTime = 0;
	_lockTime = (nLockFPS > 0) ? 1.0f / nLockFPS : 0;
}

float TimeManager::Update()
{
	//Real Elapsed Time ���
	QueryPerformanceCounter((LARGE_INTEGER*)&_currTime);
	auto advanceTime = (_currTime - _prevTime) * _secondsPerCount;
	_deltaTime = advanceTime;

	//FPS Time Lock
	__int64	nPrevTime;
	__int64	nCurrTime;

	QueryPerformanceCounter((LARGE_INTEGER*)&nPrevTime);
	while (true)
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&nCurrTime);
		_deltaTime += (nCurrTime - nPrevTime) * _secondsPerCount;
		if (_deltaTime >= _lockTime)
		{
			_waitTime = _deltaTime - advanceTime;

			if (_deltaTime < 0.0) {
				_deltaTime = 0.0;
			}
			break;
		}
		nPrevTime = nCurrTime;
	}
	QueryPerformanceCounter((LARGE_INTEGER*)&_prevTime);

	//Avg Info
	_sumCount++;
	_sumDeltaTime += _deltaTime;

	if (_sumDeltaTime >= 1) {
		_avgDeltaTime = _sumDeltaTime / _sumCount;
		_sumCount = 0;
		_sumDeltaTime = 0;
	}

	return static_cast<float>(_deltaTime);
}

float TimeManager::GetDeltaTime() {
	return static_cast<float>(_deltaTime);
}

float TimeManager::GetAvgDeltaTime()
{
	return static_cast<float>(_avgDeltaTime);
}
