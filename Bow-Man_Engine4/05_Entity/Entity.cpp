#include "stdafx.h"
#include "Entity.h"

#ifdef DIRECT_X_11_RENDERER

#include "Behavior.h"

#include "Camera.h"
#include "CameraManager.h"

//#include "ModelAssembly.h"
//#include "ConstantBuffer.h"
//#include "Shader.h"
//#include "Texture.h"
//#include "Framework.h"
//#include "Camera.h"
//#include "Scene.h"
//#include "Picking.h"
//#include "UiObject.h"
//#include "UtilityFunction.h"
//#include "ObjectStrategy.h"
//#include "RenderState.h"

Entity::Entity() {
	XMStoreFloat4x4(&m_mtxIdentity, XMMatrixIdentity());

	_mtxWorld = m_mtxIdentity;
	_vMoveRight = XMFLOAT3(1, 0, 0);
	_vMoveUp = XMFLOAT3(0, 1, 0);
	_vMoveFoward = XMFLOAT3(0, 0, 1);
	_AABB = BoundingBox(XMFLOAT3(-0.5, -0.5, -0.5), XMFLOAT3(0.5, 0.5, 0.5));
}

Entity::~Entity() 
{
//	cout << "Entity 소멸" << endl; 
}

void Entity::Release()
{
	for (const auto& behavior : _behaviors) {
		behavior->Release();
	}
	_behaviors.clear();
}

void Entity::AddToManager(const std::shared_ptr<Behavior>& behavior)
{	
	if (behavior->GetTypeName() == typeid(Camera).name()) {
		cout << "카메라 매니저에 추가 됨" << endl;
		CameraMgr->AddCamera(behavior);
	}
	behavior->AddToBehaviorMgr();
}

void Entity::Move(const XMVECTOR & vVelocity) {
	SetPosition(GetPosition() + vVelocity);
}

void Entity::Move(const XMFLOAT3 & vVelocity) {
	Move(XMLoadFloat3(&vVelocity));
}

void Entity::Move(float x, float y, float z) {
	Move(XMFLOAT3(x, y, z));
}

void Entity::Rotate(const XMVECTOR & vAxis, float fDegree)
{
	XMMATRIX mtxRotate;
	mtxRotate = XMMatrixRotationAxis(vAxis, XMConvertToRadians(fDegree));
	mtxRotate = XMMatrixMultiply(mtxRotate, GetMatrix());
	XMStoreFloat4x4(&_mtxWorld, mtxRotate);

	_vMoveRight = { _mtxWorld._11, _mtxWorld._12, _mtxWorld._13 };
	_vMoveUp = { _mtxWorld._21, _mtxWorld._22, _mtxWorld._23 };
	_vMoveFoward = { _mtxWorld._31, _mtxWorld._32, _mtxWorld._33 };
}

void Entity::Rotate(const XMFLOAT3& f3Axis, float fDegree) {
	Rotate(XMLoadFloat3(&f3Axis), fDegree);
}

//void CEntity::Rotate(AXIS axis, float fDegree) {
//	XMMATRIX mtxRotate;
//
//	XMFLOAT3 d3dxvAxis;
//	switch (axis) {
//	case AXIS::X: d3dxvAxis = XMFLOAT3(1, 0, 0); break;
//	case AXIS::Y: d3dxvAxis = XMFLOAT3(0, 1, 0); break;
//	case AXIS::Z: d3dxvAxis = XMFLOAT3(0, 0, 1); break;
//	}
//	mtxRotate = XMMatrixRotationAxis(XMLoadFloat3(&d3dxvAxis), XMConvertToRadians(fDegree));
//	mtxRotate = XMMatrixMultiply(mtxRotate, GetWorldMatrix());
//	XMStoreFloat4x4(&m_mtxWorld, mtxRotate);
//}
void Entity::Rotate(float xfDgree, float yfDgree, float zfDgree) {
	XMMATRIX mtxRotate;
	mtxRotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(xfDgree), XMConvertToRadians(yfDgree), XMConvertToRadians(zfDgree));
	mtxRotate = XMMatrixMultiply(mtxRotate, GetMatrix());
	XMStoreFloat4x4(&_mtxWorld, mtxRotate);

	_vMoveRight = { _mtxWorld._11, _mtxWorld._12, _mtxWorld._13 };
	_vMoveUp = { _mtxWorld._21, _mtxWorld._22, _mtxWorld._23 };
	_vMoveFoward = { _mtxWorld._31, _mtxWorld._32, _mtxWorld._33 };
}

//World Matrix를 서로 직교화 시켜준다.
void Entity::OrthogonolizeWorldMatrix() {
	//XMFLOAT3 d3dxvRight = GetAxisVector(AXIS::X);
	//XMFLOAT3 d3dxvUp = GetAxisVector(AXIS::Y);
	//XMFLOAT3 d3dxvLook = GetAxisVector(AXIS::Z);

	//D3DXVec3Normalize(&d3dxvLook, &d3dxvLook);
	//D3DXVec3Cross(&d3dxvRight, &d3dxvUp, &d3dxvLook);
	//D3DXVec3Normalize(&d3dxvRight, &d3dxvRight);
	//D3DXVec3Cross(&d3dxvUp, &d3dxvLook, &d3dxvRight);
	//D3DXVec3Normalize(&d3dxvUp, &d3dxvUp);

	//SetAxisVector(AXIS::X, d3dxvRight);
	//SetAxisVector(AXIS::Y, d3dxvUp);
	//SetAxisVector(AXIS::Z, d3dxvLook);
}

void Entity::Update(float fElapsedTime) {
	//for (const auto& behavior : _behaviors) {
	//	behavior->Update(fElapsedTime);
	//}
}

void Entity::SetPosition(const XMFLOAT3 & vPosition) {
	_mtxWorld._41 = vPosition.x;
	_mtxWorld._42 = vPosition.y;
	_mtxWorld._43 = vPosition.z;
//	m_pModelAssembly->GetAABB().Transform(m_AABB, GetWorldMatrix());
}

void Entity::SetPosition(const XMVECTOR & vVector) {
	XMFLOAT3 vPosition;
	XMStoreFloat3(&vPosition, vVector);
	_mtxWorld._41 = vPosition.x;
	_mtxWorld._42 = vPosition.y;
	_mtxWorld._43 = vPosition.z;
//	m_pModelAssembly->GetAABB().Transform(m_AABB, GetWorldMatrix());
}

void Entity::SetPosition(float x, float y, float z) {
	_mtxWorld._41 = x;
	_mtxWorld._42 = y;
	_mtxWorld._43 = z;
//	m_pModelAssembly->GetAABB().Transform(m_AABB, GetWorldMatrix());
}

void Entity::SetDirection(const XMFLOAT3 & vDirection) {
	XMVECTOR vLook = GetAxisZ();
	XMVECTOR vNewDirection = XMVector3Normalize(XMLoadFloat3(&vDirection));

	if (XMVector3Equal(vNewDirection, vLook)) return;

	if (XMVector3Equal(vNewDirection, -vLook)) 
	{
		XMVECTOR vRightAxis = XMVector3Cross({0, 1, 0, 0}, vLook);
		XMVECTOR vAxis = XMVector3Cross(vLook, vRightAxis);
		Rotate(vAxis, 180);
	}
	else 
	{
		XMVECTOR vAxis = XMVector3Cross(vLook, vNewDirection);

		XMFLOAT3 f3Dot;
		XMStoreFloat3(&f3Dot, XMVector3Dot(vLook, vNewDirection));
		float fRadian = XMScalarACos(f3Dot.x);
		Rotate(vAxis, XMConvertToDegrees(fRadian));
	}
}

void Entity::SetDirection(float x, float y, float z) {
	SetDirection(XMFLOAT3{ x, y, z });
}

void Entity::SetAxisX(const XMVECTOR & vVector) {
	XMFLOAT3 vAxis;
	XMStoreFloat3(&vAxis, vVector);
	SetAxisX(vAxis);
}

void Entity::SetAxisX(XMFLOAT3 vVector) {
	_mtxWorld._11 = vVector.x, _mtxWorld._12 = vVector.y, _mtxWorld._13 = vVector.z;
	_vMoveRight = vVector;
}

void Entity::SetAxisY(const XMVECTOR & vVector) {
	XMFLOAT3 vAxis;
	XMStoreFloat3(&vAxis, vVector);
	SetAxisY(vAxis);
}

void Entity::SetAxisY(XMFLOAT3 vVector) {
	_mtxWorld._21 = vVector.x, _mtxWorld._22 = vVector.y, _mtxWorld._23 = vVector.z;
	_vMoveUp = vVector;
}

void Entity::SetAxisZ(const XMVECTOR & vVector) {
	XMFLOAT3 vAxis;
	XMStoreFloat3(&vAxis, vVector);
	SetAxisZ(vAxis);
}

void Entity::SetAxisZ(XMFLOAT3 vVector) {
	_mtxWorld._31 = vVector.x, _mtxWorld._32 = vVector.y, _mtxWorld._33 = vVector.z;
	_vMoveFoward = vVector;
}

const XMFLOAT3 * Entity::GetF3Right() {
	return &_vMoveRight;
}

XMVECTOR Entity::GetRight() {
	return XMLoadFloat3(&_vMoveRight);
}

const XMFLOAT3 * Entity::GetF3UpRef() {
	return &_vMoveUp;
}

XMVECTOR Entity::GetUp() {
	return XMLoadFloat3(&_vMoveUp);
}

const XMFLOAT3 & Entity::GetF3Foward() {
	return _vMoveFoward;
}

XMVECTOR Entity::GetFoward() {
	return XMLoadFloat3(&_vMoveFoward);
}

XMVECTOR Entity::GetAxisX() {
	return XMLoadFloat3(&XMFLOAT3(_mtxWorld._11, _mtxWorld._12, _mtxWorld._13));
}

XMVECTOR Entity::GetAxisY() {
	return XMLoadFloat3(&XMFLOAT3(_mtxWorld._21, _mtxWorld._22, _mtxWorld._23));
}

XMVECTOR Entity::GetAxisZ() {
	return XMLoadFloat3(&XMFLOAT3(_mtxWorld._31, _mtxWorld._32, _mtxWorld._33));
}

XMFLOAT3 Entity::GetF3Position() {
	return XMFLOAT3(_mtxWorld._41, _mtxWorld._42, _mtxWorld._43);
}

XMVECTOR Entity::GetPosition() {
	return XMLoadFloat3(&XMFLOAT3(_mtxWorld._41, _mtxWorld._42, _mtxWorld._43));
}

const XMFLOAT4X4 & Entity::GetF4x4Matrix() {
	return _mtxWorld;
}

XMMATRIX Entity::GetMatrix() {
	return XMLoadFloat4x4(&_mtxWorld);
}

void Entity::SetDestroied() {
	_isDestroied = true;
}

bool Entity::IsDestroied() { return _isDestroied; }

#endif