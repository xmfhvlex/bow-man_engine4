#ifdef DIRECT_X_11_RENDERER

#pragma once

#include "BehaviorManager.h"

class Behavior;

class Entity : public std::enable_shared_from_this<Entity>
{
private:

public:
	Entity();
	virtual ~Entity();
	virtual void Release();

	template<class T>	std::shared_ptr<T> AddBehavior();
	template<class T, class Y> std::shared_ptr<T> AddBehavior(Y param);
	template<class T>	std::shared_ptr<T> GetBehavior();
	void AddToManager(const std::shared_ptr<Behavior>& behavior);

	virtual void Move(const XMVECTOR& vVelocity);
	virtual void Move(const XMFLOAT3& vVelocity);
	virtual void Move(float x, float y, float z);

	virtual void Rotate(const XMVECTOR& vAxis, float fDegree);
	virtual void Rotate(const XMFLOAT3& vAxis, float fDegree);
//	virtual void Rotate(AXIS axis, float fDegree);
	virtual void Rotate(float xfDgree, float yfDgree, float zfDgree);

	void OrthogonolizeWorldMatrix();

	virtual void Update(float fElapsedTime);

	virtual void SetPosition(const XMFLOAT3& vPosition);
	virtual void SetPosition(const XMVECTOR& vVector);
	virtual void SetPosition(float x, float y, float z);
	virtual void SetDirection(const XMFLOAT3& vDirection);
	virtual void SetDirection(float x, float y, float z);
	void SetAxisX(const XMVECTOR& vVector);
	void SetAxisX(XMFLOAT3 vVector);
	void SetAxisY(const XMVECTOR& vVector);
	void SetAxisY(XMFLOAT3 vVector);
	void SetAxisZ(const XMVECTOR& vVector);
	void SetAxisZ(XMFLOAT3 vVector);

	const XMFLOAT3* GetF3Right();
	XMVECTOR GetRight();
	const XMFLOAT3* GetF3UpRef();
	XMVECTOR GetUp();
	const XMFLOAT3& GetF3Foward();
	XMVECTOR GetFoward();
	XMVECTOR GetAxisX();
	XMVECTOR GetAxisY();
	XMVECTOR GetAxisZ();
	XMFLOAT3 GetF3Position();
	XMVECTOR GetPosition();
	const XMFLOAT4X4& GetF4x4Matrix();
	XMMATRIX GetMatrix();

	void SetDestroied();
	bool IsDestroied();

public:
	inline static XMFLOAT4X4			m_mtxIdentity;

protected:
	bool						_isDestroied = false;

	XMFLOAT4X4					_mtxWorld;
	XMFLOAT3					_vMoveRight;
	XMFLOAT3					_vMoveUp;
	XMFLOAT3					_vMoveFoward;

	BoundingBox					_AABB;
	
	std::vector<std::shared_ptr<Behavior>> _behaviors;
};

template<class T>
std::shared_ptr<T> Entity::AddBehavior() 
{
	//for (const auto& behavior : _behaviors) {

	//	//constexpr bool a = behavior->GetTypeName() == typeid(T).name();
	//	//static_assert(a, "NOOOOOO");

	//	if (typeid(*behavior) == typeid(T)) {
	//		return nullptr;
	//	}
	//}

	auto behavior = std::make_shared<T>(shared_from_this());
	_behaviors.push_back(behavior);
	AddToManager(behavior);
	return behavior;
}

template<class T, class Y>
std::shared_ptr<T> Entity::AddBehavior(Y param)
{
	auto behavior = std::make_shared<T>(shared_from_this(), param);
	_behaviors.push_back(behavior);
	AddToManager(behavior);
	return behavior;
}

template<class T>
std::shared_ptr<T> Entity::GetBehavior() {			//둘중 어떤 것이 옳을까??
																	//	const std::shared_ptr<Behavior>& GetBehavior() {
	for (const auto& behavior : _behaviors) {
	//	if (typeid(*behavior) == typeid(T)) {
	//		return std::dynamic_pointer_cast<T>(behavior);
	//	}

		auto castedBehavior = std::dynamic_pointer_cast<T>(behavior);
		if (castedBehavior != nullptr) {
			return castedBehavior;
		}
	}
	return nullptr;
}

#endif