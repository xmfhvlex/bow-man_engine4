#include "stdafx.h"
#include "Light.h"

#ifdef DIRECT_X_11_RENDERER


#include "LightManager.h"
#include "Entity.h"

bool Light::Behave()
{
	//	_entity->Rotate(0, fElapsedTime*20, 0);
	_lightData.positionWS = _entity->GetF3Position();
	_lightData.directionWS = _entity->GetF3Foward();
	LightMgr->AddLight(_lightData);
	return true;
}

void Light::SetLightData(const LightData & lightData) {
	_lightData = lightData;
	_entity->SetPosition(_lightData.positionWS);
	_entity->SetDirection(_lightData.directionWS);

	_lightData.directionWS = _entity->GetF3Foward();
}

#endif