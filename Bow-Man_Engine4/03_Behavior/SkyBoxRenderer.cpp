#include "stdafx.h"
#include "SkyBoxRenderer.h"

#ifdef DIRECT_X_11_RENDERER

#include "BehaviorManager.h"


//void SkyBoxRenderer::AddToBehaviorMgr()
//{
//	auto renderMethod = std::make_pair(GetEntity(), std::bind(&Behavior::Behave, this));
//	BehaviorMgr->_renderMethod[_renderLayer].push_back(move(renderMethod));
//}

#endif

bool SkyBoxRenderer::DepthPass() 
{
	if (Renderer::DepthPass() == false)
		return false;

	if (_mesh != nullptr)
		_mesh->Render();

	return true;
}
