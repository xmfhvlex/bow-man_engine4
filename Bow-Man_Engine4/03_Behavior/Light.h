#ifdef DIRECT_X_11_RENDERER

#pragma once

#include "Behavior.h"
#include "LightManager.h"

class Light : public Behavior
{
public:
	Light(const std::shared_ptr<Entity> & entity) : Behavior(entity) {}
	~Light() override {}

	bool Behave() override;

	void SetLightData(const LightData & lightData);


protected:

private:
	LightData	_lightData;
};

#endif