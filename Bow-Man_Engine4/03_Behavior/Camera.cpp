#include "stdafx.h"
#include "Camera.h"

#ifdef DIRECT_X_11_RENDERER
#include "Entity.h"
#include "Window.h"
#include "DirectX11Device.h"
#include "InputManager.h"
#include "InputController.h"


Camera::Camera(const std::shared_ptr<Entity> & entity) : Behavior(entity)
{
	if (_cameraBuffer.Get() == nullptr) {
		CreateConstantBuffers<CameraDataBuffer>(Device.Get(), _cameraBuffer.ReleaseAndGetAddressOf());
	}
	Behave();
}

bool Camera::Behave()
{
	//Set Viewport
	_viewport.TopLeftX = float(0);
	_viewport.TopLeftY = float(0);
	_viewport.Width = float(Window::screenWidth);
	_viewport.Height = float(Window::screenHeight);
	_viewport.MinDepth = 0.0;
	_viewport.MaxDepth = 1.0;
	ImmediateContext->RSSetViewports(1, &_viewport);

	//Set Camera Buffer to VGA
	XMFLOAT4X4 f4x4World = _entity->GetF4x4Matrix();
	XMFLOAT4X4 f4x4View;
	f4x4View._11 = f4x4World._11;
	f4x4View._12 = f4x4World._21;
	f4x4View._13 = f4x4World._31;
	f4x4View._14 = 0;
	f4x4View._21 = f4x4World._12;
	f4x4View._22 = f4x4World._22;
	f4x4View._23 = f4x4World._32;
	f4x4View._24 = 0;
	f4x4View._31 = f4x4World._13;
	f4x4View._32 = f4x4World._23;
	f4x4View._33 = f4x4World._33;
	f4x4View._34 = 0;

	XMVECTOR vVector;
	vVector = -XMVector3Dot(_entity->GetPosition(), _entity->GetAxisX());
	XMStoreFloat(&f4x4View._41, vVector);
	vVector = -XMVector3Dot(_entity->GetPosition(), _entity->GetAxisY());
	XMStoreFloat(&f4x4View._42, vVector);
	vVector = -XMVector3Dot(_entity->GetPosition(), _entity->GetAxisZ());
	XMStoreFloat(&f4x4View._43, vVector);
	f4x4View._44 = 1;

	XMMATRIX mtxProjection = XMMatrixPerspectiveFovLH(XMConvertToRadians(60), (float)Window::screenWidth / Window::screenHeight, 0.01f, 1000);
	XMMATRIX mtxView = XMLoadFloat4x4(&f4x4View);

	XMVECTOR det = XMMatrixDeterminant(mtxProjection);
	XMMATRIX mtxProjectionInverse = XMMatrixInverse(&det, mtxProjection);

	det = XMMatrixDeterminant(mtxView);
	XMMATRIX mtxViewInverse = XMMatrixInverse(&det, mtxView);

	XMMATRIX mtxViewProjection = XMMatrixMultiply(mtxView, mtxProjection);
	XMStoreFloat4x4(&_f4x4ViewProjection, mtxViewProjection);

	XMStoreFloat4x4(&_cameraData._mtxVP, XMMatrixTranspose(mtxViewProjection));
	XMStoreFloat4x4(&_cameraData._mtxView, XMMatrixTranspose(mtxView));
	XMStoreFloat4x4(&_cameraData._mtxProjection, XMMatrixTranspose(mtxProjection));
	XMStoreFloat4x4(&_cameraData._mtxProjectionInverse, XMMatrixTranspose(mtxProjectionInverse));
	XMStoreFloat4x4(&_cameraData._mtxViewInverse, XMMatrixTranspose(mtxViewInverse));
	XMStoreFloat4x4(&_cameraData._mtxWorld, XMMatrixTranspose(_entity->GetMatrix()));

	_cameraData._vCameraSetting.x = Window::screenWidth;
	_cameraData._vCameraSetting.y = Window::screenHeight;
	_cameraData._vCameraSetting.z = 0.01;
	_cameraData._vCameraSetting.w = 1000;


	//	m_CameraData.m_mtxOrtho;
	//	m_CameraData.m_mtxShadowVP;
	//	m_CameraData.m_vPerspectiveValue = XMFLOAT4(1.0f / m_f4x4Projection._11, 1.0f / m_f4x4Projection._22, m_f4x4Projection._43, -m_f4x4Projection._33);
	//	m_CameraData.m_vCameraPosition = XMFLOAT3(m_mtxWorld._41, m_mtxWorld._42, m_mtxWorld._43);
	//	m_CameraData.m_fNearPlaneDistant = g_fNearPlaneDistant;
	//	m_CameraData.m_vCameraDirection = XMFLOAT3(m_mtxWorld._31, m_mtxWorld._32, m_mtxWorld._33);
	//	m_CameraData.m_fFarPlaneDistant = g_fFarPlaneDistant;
	//	memcpy(m_CameraData.m_arrFrustum, m_f4FrustumPlanes, sizeof(m_f4FrustumPlanes));

	::UpdateConstantBuffer(ImmediateContext.Get(), _cameraBuffer.Get(), _cameraData);
	ImmediateContext->VSSetConstantBuffers(ECBuffer::CAMERA, 1, _cameraBuffer.GetAddressOf());
	ImmediateContext->PSSetConstantBuffers(ECBuffer::CAMERA, 1, _cameraBuffer.GetAddressOf());
	ImmediateContext->CSSetConstantBuffers(ECBuffer::CAMERA, 1, _cameraBuffer.GetAddressOf());
	return true;
}

#endif