#include "stdafx.h"
#include "Renderer.h"

#ifdef DIRECT_X_11_RENDERER

#include "Entity.h"
#include "DirectX11Device.h"

#include "Camera.h"
#include "CameraManager.h"
#include "ShaderManager.h"
#include "Material.h"
#include "IShader.h"
#include "RenderStateManager.h"

Renderer::Renderer(const std::shared_ptr<Entity> & entity) : Behavior(entity)
{
	_behaviorType = BehaviorType::Render;
	if (_worldViewProjectionBuffer.Get() == nullptr) {
		CreateConstantBuffers<CB_WVP>(Device.Get(), _worldViewProjectionBuffer.ReleaseAndGetAddressOf());
	}
}

Renderer::Renderer(const std::shared_ptr<Entity>& entity, const RenderLayer& layer) : Renderer(entity)
{
	_renderLayer = layer;

}

void Renderer::AddToBehaviorMgr()
{
	if (_renderLayer == RenderLayer::Depth) {
		auto depthMethod = std::make_pair(GetEntity(), std::bind(&Renderer::DepthPass, this));
		BehaviorMgr->_renderMethod[_renderLayer].push_back(move(depthMethod));
	}
	else {
		auto renderMethod = std::make_pair(GetEntity(), std::bind(&Behavior::Behave, this));
		BehaviorMgr->_renderMethod[_renderLayer].push_back(move(renderMethod));
	}

	//auto renderMethod = std::make_pair(GetEntity(), std::bind(&Behavior::Behave, this));
	//BehaviorMgr->_renderMethod[_renderLayer].push_back(move(renderMethod));

}

bool Renderer::DepthPass() 
{
	auto mainCamera = CameraMgr->GetCamera(0);
	auto wvp = _entity->GetMatrix() * mainCamera->GetViewProjection();

	CB_WVP wvpData;
	XMStoreFloat4x4(&wvpData._mtxWVP, XMMatrixTranspose(wvp));
	XMStoreFloat4x4(&wvpData._mtxWorld, XMMatrixTranspose(_entity->GetMatrix()));
	wvpData._entityIndex = _entityIndex;
	::UpdateConstantBuffer(ImmediateContext.Get(), _worldViewProjectionBuffer.Get(), wvpData);
	ImmediateContext->VSSetConstantBuffers(ECBuffer::WVP, 1, _worldViewProjectionBuffer.GetAddressOf());

	//	//Material Set
	if (_material != nullptr)
		_material->SetMeterialToGPU(ImmediateContext);

		//RenderState Set
	if(_renderStatePack != nullptr)
		_renderStatePack->SetStatePackToGPU(ImmediateContext);

		//Shader Set
	if (_shaderPack != nullptr)
		_shaderPack->Activate(ImmediateContext);

	return true;
}

bool Renderer::Behave()
{
	if (DirectX11Device::_entityOcclusion[_entityIndex] == 0)
		return false;

	auto mainCamera = CameraMgr->GetCamera(0);
	auto wvp = _entity->GetMatrix() * mainCamera->GetViewProjection();

	CB_WVP wvpData;
	XMStoreFloat4x4(&wvpData._mtxWVP, XMMatrixTranspose(wvp));
	XMStoreFloat4x4(&wvpData._mtxWorld, XMMatrixTranspose(_entity->GetMatrix()));
	wvpData._entityIndex = _entityIndex;
	::UpdateConstantBuffer(ImmediateContext.Get(), _worldViewProjectionBuffer.Get(), wvpData);
	ImmediateContext->VSSetConstantBuffers(ECBuffer::WVP, 1, _worldViewProjectionBuffer.GetAddressOf());

	//Material Set
	if (_material != nullptr)
		_material->SetMeterialToGPU(ImmediateContext);

	//RenderState Set
	if (_renderStatePack != nullptr) 
		_renderStatePack->SetStatePackToGPU(ImmediateContext);

	//Shader Set
	if (_shaderPack != nullptr)
		_shaderPack->Activate(ImmediateContext);

	return true;
}

void Renderer::SetRenderLayer(const RenderLayer & layer) {
	_renderLayer = layer;
}

void Renderer::SetShaderPack(const std::wstring& nameShaderpack)
{
	_shaderPack = ShaderMgr->GetShaderPack(nameShaderpack);
}

void Renderer::SetRenderStatePack(const std::wstring& nameRenderStatePack)
{
	auto renderStatePack = RenderStateMgr->GetRenderStatePack(nameRenderStatePack);

	if (renderStatePack == nullptr) {
		cout << "Error not Found : " << __FUNCTION__ << endl;
	}

	_renderStatePack = renderStatePack;
} 

#endif