#include "stdafx.h"
#include "InputController.h"

#ifdef DIRECT_X_11_RENDERER

#include "Entity.h"
#include "InputManager.h"

bool InputController::Behave()
{
	float xfDgree = InputMgr->GetYDelta();
	float yfDgree = InputMgr->GetXDelta();
	float zfDgree = 0;

	XMMATRIX mtxRotate;
	XMFLOAT4 vAxis;
	XMStoreFloat4(&vAxis, _entity->GetAxisY());

	if (yfDgree != 0.0f) {
		if (vAxis.y > 0) {
			mtxRotate = XMMatrixRotationAxis(XMVECTOR{ 0, 1, 0 }, XMConvertToRadians(yfDgree));
			_entity->SetAxisX(XMVector3TransformNormal(_entity->GetAxisX(), mtxRotate));
			_entity->SetAxisY(XMVector3TransformNormal(_entity->GetAxisY(), mtxRotate));
			_entity->SetAxisZ(XMVector3TransformNormal(_entity->GetAxisZ(), mtxRotate));
		}
		else {
			mtxRotate = XMMatrixRotationAxis(XMVECTOR{ 0, -1, 0 }, XMConvertToRadians(yfDgree));
			_entity->SetAxisX(XMVector3TransformNormal(_entity->GetAxisX(), mtxRotate));
			_entity->SetAxisY(XMVector3TransformNormal(_entity->GetAxisY(), mtxRotate));
			_entity->SetAxisZ(XMVector3TransformNormal(_entity->GetAxisZ(), mtxRotate));
		}
	}
	if (xfDgree != 0.0f) {
		mtxRotate = XMMatrixRotationAxis(_entity->GetAxisX(), XMConvertToRadians(xfDgree));
		_entity->SetAxisX(XMVector3TransformNormal(_entity->GetAxisX(), mtxRotate));
		_entity->SetAxisY(XMVector3TransformNormal(_entity->GetAxisY(), mtxRotate));
		_entity->SetAxisZ(XMVector3TransformNormal(_entity->GetAxisZ(), mtxRotate));
	}

	UCHAR pKeyBuffer[256];
	ZeroMemory(pKeyBuffer, 256);
	if (GetKeyboardState(pKeyBuffer)) {
		XMVECTOR velocity = XMVectorZero();

		if (pKeyBuffer[VK_W] & 0xF0) {
			velocity += _entity->GetFoward();
		}
		if (pKeyBuffer[VK_S] & 0xF0) {
			velocity -= _entity->GetFoward();
		}
		if (pKeyBuffer[VK_D] & 0xF0) {
			velocity += _entity->GetRight();
		}
		if (pKeyBuffer[VK_A] & 0xF0) {
			velocity -= _entity->GetRight();
		}
		if (pKeyBuffer[VK_E] & 0xF0) {
			velocity += _entity->GetUp();
		}
		if (pKeyBuffer[VK_Q] & 0xF0) {
			velocity -= _entity->GetUp();
		}

		velocity = XMVector3Normalize(velocity);
		velocity *= (pKeyBuffer[VK_SHIFT] & 0xF0) ? 5 : 1;
		_entity->Move(velocity * TimeMgr->GetDeltaTime());
	}
	return true;
}

#endif