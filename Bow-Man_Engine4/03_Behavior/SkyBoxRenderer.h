#ifdef DIRECT_X_11_RENDERER

#pragma once
#include "MeshRenderer.h"
#include "Mesh.h"

class SkyBoxRenderer : public MeshRenderer
{
public:
	SkyBoxRenderer(const std::shared_ptr<Entity> & entity) : MeshRenderer(entity) {}
	~SkyBoxRenderer() override {}

	bool DepthPass() override;

//	void AddToBehaviorMgr() override;
};

#endif