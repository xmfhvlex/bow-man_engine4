#ifdef DIRECT_X_11_RENDERER

#pragma once

#include "Behavior.h"

class Material;
class Shader;
class ShaderPack;
class RenderStatePack;

struct CB_WVP
{
	XMFLOAT4X4 _mtxWVP;
	XMFLOAT4X4 _mtxWorld;
	UINT _entityIndex;
	int _padding[3];
};

class Renderer : public Behavior
{
public:
	Renderer(const std::shared_ptr<Entity> & entity);
	Renderer(const std::shared_ptr<Entity>& entity, const RenderLayer& layer);
	~Renderer() override {}
	
	void AddToBehaviorMgr() override;

	virtual bool DepthPass();
	bool Behave() override;

	void SetRenderLayer(const RenderLayer& layer);
	void SetRenderStatePack(const std::wstring& nameRenderStatePack);
	void SetShaderPack(const std::wstring& nameShaderpack);

public:
	inline static UINT _entityIndex = 0;

protected:
	RenderLayer _renderLayer = RenderLayer::Opaque;

private:
	inline static ComPtr <ID3D11Buffer>	_worldViewProjectionBuffer = nullptr;

	std::shared_ptr<Material>			_material = nullptr;
	std::shared_ptr<RenderStatePack>	_renderStatePack = nullptr;
	std::shared_ptr<ShaderPack>			_shaderPack = nullptr;
};

#endif