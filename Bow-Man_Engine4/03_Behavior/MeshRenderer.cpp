#include "stdafx.h"
#include "MeshRenderer.h"
#ifdef DIRECT_X_11_RENDERER

#include "DirectX11Device.h"
#include "SectorMesh.h"

bool MeshRenderer::DepthPass() {
	if (Renderer::DepthPass() == false)
		return false;

	if (_mesh != nullptr)
		_mesh->Render();
	return true;
}

bool MeshRenderer::Behave()
{
	if (Renderer::Behave() == false)
		return false;

	if (_mesh != nullptr)
		_mesh->Render();

	return true;
}

#endif