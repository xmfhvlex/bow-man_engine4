#ifdef DIRECT_X_11_RENDERER

#pragma once

#include "Behavior.h"

class Skybox : public Behavior
{
public:
	Skybox (const std::shared_ptr<Entity> & entity) : Behavior(entity) {}
	~Skybox() override {}

	bool Behave() override;

protected:

private:
	
};

#endif