#ifdef DIRECT_X_11_RENDERER

#pragma once

#include "Behavior.h"

class InputController : public Behavior
{
public:
	InputController(const std::shared_ptr<Entity> & entity) : Behavior(entity) {}
	~InputController() override {}

	bool Behave() override;

protected:

private:

};

#endif