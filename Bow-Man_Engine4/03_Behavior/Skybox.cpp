#include "stdafx.h"
#include "Skybox.h"

#ifdef DIRECT_X_11_RENDERER


#include "Entity.h"
#include "Camera.h"
#include "CameraManager.h"

#include "BehaviorManager.h"

bool Skybox::Behave()
{
	GetEntity()->SetPosition(CameraMgr->GetCamera(0)->GetEntity()->GetPosition());
	GetEntity()->Rotate(0, TimeMgr->GetDeltaTime(), 0);
	return true;
}


#endif