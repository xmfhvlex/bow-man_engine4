#include "stdafx.h"
#include "Behavior.h"

#include "BehaviorManager.h"
#include "Entity.h"


Behavior::Behavior(const std::shared_ptr<Entity>& entiy)
{
	_entity = entiy;
}

Behavior::~Behavior() 
{
//	cout << "Behavior �Ҹ�" << endl; 
}

void Behavior::AddToBehaviorMgr() 
{
	auto methodData = std::make_pair(GetEntity(), std::bind(&Behavior::Behave, this));
	BehaviorMgr->_updateMethodList.push_back(move(methodData));
}
