#pragma once

#ifdef DIRECT_X_11_RENDERER

#include "Behavior.h"

struct CameraDataBuffer
{
	XMFLOAT4X4			_mtxView;
	XMFLOAT4X4			_mtxProjection;
	XMFLOAT4X4			_mtxVP;

	XMFLOAT4X4			_mtxProjectionInverse;
	XMFLOAT4X4			_mtxViewInverse;

	XMFLOAT4X4			_mtxWorld;
	XMFLOAT4			_vCameraSetting;

	//XMFLOAT4X4			m_mtxShadowVP;
	//XMFLOAT4			m_vPerspectiveValue;
	//XMFLOAT3			m_vCameraPosition;
	//float				m_fNearPlaneDistant;
	//XMFLOAT3			m_vCameraDirection;
	//float				m_fFarPlaneDistant;
	//XMFLOAT4			m_arrFrustum[6];
};

class Camera : public Behavior 
{
public:
	Camera(const std::shared_ptr<Entity> & entity);
	~Camera() override {}

	bool Behave() override;

	const XMFLOAT4X4 & GetF4x4ViewProjection() {
		return _f4x4ViewProjection;
	}

	XMMATRIX GetViewProjection() {
		return XMLoadFloat4x4(&_f4x4ViewProjection);
	}

protected:

private:
	inline static ComPtr <ID3D11Buffer>	_cameraBuffer = nullptr;
	D3D11_VIEWPORT	_viewport;
	CameraDataBuffer		_cameraData;
	XMFLOAT4X4		_f4x4ViewProjection;
};

#endif