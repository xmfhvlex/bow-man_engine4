#ifdef DIRECT_X_11_RENDERER

#pragma once
#include "Renderer.h"
#include "Mesh.h"

class MeshRenderer : public Renderer
{
public:
	MeshRenderer(const std::shared_ptr<Entity> & entity) : Renderer(entity) {}
	MeshRenderer(const std::shared_ptr<Entity>& entity, const RenderLayer& layer) : Renderer(entity, layer){}
	~MeshRenderer() override {}

	bool DepthPass() override;
	bool Behave() override;

	void SetMesh(std::shared_ptr<Mesh> mesh) {
		_mesh = mesh;
	}

protected:
	std::shared_ptr<Mesh> _mesh;

private:

};

#endif