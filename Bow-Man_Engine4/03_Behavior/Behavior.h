#pragma once

#define PROPERTY_BEG
#define PROPERTY_END

class Entity;

enum class BehaviorPriority {
	Update,
	Phisics,
	FrustumCulling,
	Render,
};

enum class BehaviorType {
	Update,
	Render,
	Phisics,
	EndCount
};

class Behavior : public std::enable_shared_from_this<Behavior> {
public:
	Behavior(const std::shared_ptr<Entity> & entiy);
	virtual ~Behavior();
	virtual void Release() {	_entity = nullptr;		}

	std::string GetTypeName() {		return typeid(*this).name();	}

	void SetEntity(const std::shared_ptr<Entity> & entity) {
		_entity = entity;
	}
	std::shared_ptr<Entity> GetEntity() { return _entity;  }
	virtual void AddToBehaviorMgr();

	virtual bool Behave() = 0;

	BehaviorType GetBehaviorType() { return _behaviorType; }

	//	template<class _Ty, class... _Types>
	//	void Test(std::string eventName, _Types&&... _Args) {
	//		const auto _Rx = new _Ty(_STD forward<_Types>(_Args)...);
	//	}

	//template<class _Types>
	//std::function<void(_Types)> * a;
//	std::unordered_map<std::string, std::function<void(_Types&&...)>> _callbacks;
//
//	template<class _Ty, class... _Types>
//	void AddEvent(std::string event, std::function<void(_Types)> func)
//	{
////		_callbacks[event].push_back(func);
//	}
//
//	template<class _Ty, class... _Types>
//	void SendEvent(std::string event, _Types&&... _Args)
//	{
//		try {
//			auto vec = _callbacks.at(event);
//
//			for each (auto var in vec)
//			{
//				var(_Args);
//			}
//		}
//		catch (std::out_of_range) {
//	//		MessageBoxA(g_hwnd, ("Invalid Event : " + event).c_str(), "Invalid Callback Event", NULL);
//		}
//	}

	//template<class... _Types>
	//void AAA(_Types&&... _Args)
	//{
	//	BBB(_Args...);
	//}

	//void BBB(int a, int b, std::string c) {

	//	cout << a << b << c << endl;
	//}

public:

protected:
	BehaviorType _behaviorType = BehaviorType::Update;
	std::shared_ptr<Entity> _entity;
};