#include "stdafx.h"
#include "VulkanDevice.h"

#include "PathManager.h"

#ifdef VULKAN_RENDERER


#if (BUILD_ENABLE_VULKAN_RUNTIME_DEBUG==1 && defined VULKAN_RENDERER)
void VKErrorCheck(VkResult result)
{ 
	if (result < 0) {
		switch (result) {
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			std::cout << "VK_ERROR_OUT_OF_HOST_MEMORY" << std::endl;
			break;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			std::cout << "VK_ERROR_OUT_OF_DEVICE_MEMORY" << std::endl;
			break;
		case VK_ERROR_INITIALIZATION_FAILED:
			std::cout << "VK_ERROR_INITIALIZATION_FAILED" << std::endl;
			break;
		case VK_ERROR_DEVICE_LOST:
			std::cout << "VK_ERROR_DEVICE_LOST" << std::endl;
			break;
		case VK_ERROR_MEMORY_MAP_FAILED:
			std::cout << "VK_ERROR_MEMORY_MAP_FAILED" << std::endl;
			break;
		case VK_ERROR_LAYER_NOT_PRESENT:
			std::cout << "VK_ERROR_LAYER_NOT_PRESENT" << std::endl;
			break;
		case VK_ERROR_EXTENSION_NOT_PRESENT:
			std::cout << "VK_ERROR_EXTENSION_NOT_PRESENT" << std::endl;
			break;
		case VK_ERROR_FEATURE_NOT_PRESENT:
			std::cout << "VK_ERROR_FEATURE_NOT_PRESENT" << std::endl;
			break;
		case VK_ERROR_INCOMPATIBLE_DRIVER:
			std::cout << "VK_ERROR_INCOMPATIBLE_DRIVER" << std::endl;
			break;
		case VK_ERROR_TOO_MANY_OBJECTS:
			std::cout << "VK_ERROR_TOO_MANY_OBJECTS" << std::endl;
			break;
		case VK_ERROR_FORMAT_NOT_SUPPORTED:
			std::cout << "VK_ERROR_FORMAT_NOT_SUPPORTED" << std::endl;
			break;
		case VK_ERROR_SURFACE_LOST_KHR:
			std::cout << "VK_ERROR_SURFACE_LOST_KHR" << std::endl;
			break;
		case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
			std::cout << "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR" << std::endl;
			break;
		case VK_SUBOPTIMAL_KHR:
			std::cout << "VK_SUBOPTIMAL_KHR" << std::endl;
			break;
		case VK_ERROR_OUT_OF_DATE_KHR:
			std::cout << "VK_ERROR_OUT_OF_DATE_KHR" << std::endl;
			break;
		case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
			std::cout << "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR" << std::endl;
			break;
		case VK_ERROR_VALIDATION_FAILED_EXT:
			std::cout << "VK_ERROR_VALIDATION_FAILED_EXT" << std::endl;
			break;
		default:
			break;
		}
		assert(0 && "Vulkan runtime error.");
	}
}

uint32_t FindMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties * gpu_memory_properties, const VkMemoryRequirements * memory_requirements, const VkMemoryPropertyFlags required_properties)
{
	for (uint32_t i = 0; i < gpu_memory_properties->memoryTypeCount; ++i) {
		if (memory_requirements->memoryTypeBits & (1 << i)) {
			if ((gpu_memory_properties->memoryTypes[i].propertyFlags & required_properties) == required_properties) {
				return i;
			}
		}
	}
	assert(0 && "Couldn't find proper memory type.");
	return UINT32_MAX;
}

uint32_t FindMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties * gpu_memory_properties, const VkMemoryRequirements * memory_requirements)
{
	for (uint32_t i = 0; i < gpu_memory_properties->memoryTypeCount; ++i) {
		if (memory_requirements->memoryTypeBits & (1 << i)) {
			return i;
		}
	}
	assert(0 && "Couldn't find proper memory type.");
	return UINT32_MAX;
}
#else

#endif // BUILD_ENABLE_VULKAN_RUNTIME_DEBUG


#if BUILD_ENABLE_VULKAN_DEBUG

VKAPI_ATTR VkBool32 VKAPI_CALL
VulkanDebugCallback(
	VkDebugReportFlagsEXT		flags,
	VkDebugReportObjectTypeEXT	obj_type,
	uint64_t					src_obj,
	size_t						location,
	int32_t						msg_code,
	const char *				layer_prefix,
	const char *				msg,
	void *						user_data
)
{
	std::ostringstream stream;
	stream << "VKDBG: ";
	if (flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT) {
		stream << "INFO: ";
	}
	if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT) {
		stream << "WARNING: ";
	}
	if (flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) {
		stream << "PERFORMANCE: ";
	}
	if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
		stream << "ERROR: ";
	}
	if (flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT) {
		stream << "DEBUG: ";
	}
	stream << "@[" << layer_prefix << "]: ";
	stream << msg << std::endl;
	std::cout << stream.str();

#if defined( _WIN32 )
	if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
		//	MessageBox( NULL, stream.c_str(), L"Vulkan Error!", 0 );
	}
#endif

	return false;
}



//void CreateBuffer(VDeleter<VkDevice> device, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags property) 
//{
//	VkBufferCreateInfo bufferInfo = {};
//	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
//	bufferInfo.size = size;
//	bufferInfo.usage = usage;
//	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
//
//	VkBuffer buffer;
//	
//
//	VKErrorCheck(vkCreateBuffer(device, &bufferInfo, nullptr, &buffer));
//
//	VkMemoryRequirements memRequirements;
//	vkGetBufferMemoryRequirements(device, buffer, &memRequirements);
//
//	VkMemoryAllocateInfo allocInfo = {};
//	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
//	allocInfo.allocationSize = memRequirements.size;
//	allocInfo.memoryTypeIndex = FindMemoryTypeIndex(&_gpuMemoryProperties, &image_memory_requirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
//}


VulkanDevice::VulkanDevice()
{
}


VulkanDevice::~VulkanDevice()
{
}

//Init & DeInit-------------------------------------------------------------------------------------------------------------------
void VulkanDevice::InitRenderDevice()
{
	InitExtention();
	InitInstance();
//	InitDebug();
	InitSurface(gHinstance, gHwnd);
	InitDevice();
	InitSwapchain();
	InitSwapchainImages();
	InitDepthStencilImage();
	InitRenderPass();
	InitFramebuffers();
	InitVertexBuffer();
	InitSynchronizations();
	InitCommandPool();
	InitCommandBuffer();
	InitSemaphore();

	LoadShader();
}

void VulkanDevice::ResizeWindow()
{
	if (_vkInstance == VK_NULL_HANDLE || _swapchain == VK_NULL_HANDLE)
		return;

	//VKErrorCheck(vkWaitForFences(_device, 1, &_fenceSwapchainImage, VK_TRUE, UINT64_MAX));
	//VKErrorCheck(vkResetFences(_device, 1, &_fenceSwapchainImage));
	//VKErrorCheck(vkQueueWaitIdle(_graphicsQueue));

	DeInitFramebuffers();
	DeInitRenderPass();
	DeInitDepthStencilImage();
	DeInitSwapchainImages();
	DeInitSwapchain();
	InitSwapchain();
	InitSwapchainImages();
	InitDepthStencilImage();
	InitRenderPass();
	InitFramebuffers();
}

void VulkanDevice::DestroyRenderDevice()
{
//	ErrorCheck(vkWaitForFences(_device, 1, &_fenceSwapchainImage, VK_TRUE, UINT64_MAX));
//	ErrorCheck(vkResetFences(_device, 1, &_fenceSwapchainImage));
	VKErrorCheck(vkQueueWaitIdle(_graphicsQueue));
	UnLoadShader();
	DeInitSemaphore();
	DeInitCommandBuffer();
	DeInitCommandPool();
	DeInitSynchronizations();
	DeInitFramebuffers();
	DeInitRenderPass();
	DeInitDepthStencilImage();
	DeInitSwapchainImages();
	DeInitSwapchain();
	DeInitSurface();
//	DeInitDebug();
}

void VulkanDevice::InitExtention() 
{
	//Instance 확장
	_vInstanceExtensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);			//WSI (Window System Integration)확장
	_vInstanceExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);	//윈도우용 빌드

	//Device 확장
	_vDeviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);	//장치의 렌더링 된 이미지를 윈도우에 표시하도록 장치 확장.

//	_vInstanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
//	//Debug Layer & Extention
//	_debugCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
//	_debugCallbackCreateInfo.pfnCallback = VulkanDebugCallback;
//	_debugCallbackCreateInfo.flags =
////		VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
//		VK_DEBUG_REPORT_WARNING_BIT_EXT |
//		VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
//		VK_DEBUG_REPORT_ERROR_BIT_EXT |
//		0;
//

//	_vInstanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
	/*
	//	_instance_layers.push_back( "VK_LAYER_LUNARG_threading" );
	_instance_layers.push_back( "VK_LAYER_GOOGLE_threading" );
	_instance_layers.push_back( "VK_LAYER_LUNARG_draw_state" );
	_instance_layers.push_back( "VK_LAYER_LUNARG_image" );
	_instance_layers.push_back( "VK_LAYER_LUNARG_mem_tracker" );
	_instance_layers.push_back( "VK_LAYER_LUNARG_object_tracker" );
	_instance_layers.push_back( "VK_LAYER_LUNARG_param_checker" );
	*/
}

void VulkanDevice::InitInstance()
{
	VkApplicationInfo application_info{};
	application_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	application_info.pNext = nullptr;
	application_info.pApplicationName = "Bow-Man_Engine4 Vulkan";
	application_info.applicationVersion = VK_MAKE_VERSION(0, 1, 0);	// Major, Minor, Patch
	application_info.pEngineName = "Bow-Man_Engine4";
	application_info.engineVersion;
	application_info.apiVersion = VK_API_VERSION_1_1;	//사용할 Vulkan API 버전.
	
	VkInstanceCreateInfo instance_create_info{};
	instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instance_create_info.pNext = &_debugCallbackCreateInfo;
	instance_create_info.flags;
	instance_create_info.pApplicationInfo = &application_info;
	instance_create_info.enabledLayerCount = _vInstanceLayers.size();
	instance_create_info.ppEnabledLayerNames = _vInstanceLayers.data();
	instance_create_info.enabledExtensionCount = _vInstanceExtensions.size();
	instance_create_info.ppEnabledExtensionNames = _vInstanceExtensions.data();

	/*
		Vulkan API는 각각의 애플리케이션 State를 적재하기 위하여 vkInstance Object를 사용한다.
	*/
	VKErrorCheck(vkCreateInstance(
		&instance_create_info,
		nullptr, /*VkAllocationCallbacks - 사용자 메모리 관리자*/
		&_vkInstance)
	);
}

void VulkanDevice::InitSurface(HINSTANCE hInstance, HWND hWnd)
{
	_win32_instance = hInstance;
	_win32_window = hWnd;

	VkWin32SurfaceCreateInfoKHR create_info{};
	create_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	create_info.hinstance = _win32_instance;
	create_info.hwnd = _win32_window;
	VKErrorCheck(vkCreateWin32SurfaceKHR(_vkInstance, &create_info, nullptr, &_surface));
}

void VulkanDevice::DeInitSurface()
{
	vkDestroySurfaceKHR(_vkInstance, _surface, nullptr);
}

void VulkanDevice::InitDevice()
{
	uint32_t gpu_count = 0;
	vkEnumeratePhysicalDevices(_vkInstance, &gpu_count, nullptr);
	std::vector<VkPhysicalDevice> lstGpu(gpu_count);
	vkEnumeratePhysicalDevices(_vkInstance, &gpu_count, lstGpu.data());
		
	for (const auto& gpu : lstGpu ) 
	{
		uint32_t nQueueFamilly = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(gpu, &nQueueFamilly, nullptr);
		std::vector<VkQueueFamilyProperties> vQueueFamily(nQueueFamilly);
		vkGetPhysicalDeviceQueueFamilyProperties(gpu, &nQueueFamilly, vQueueFamily.data());

		std::vector<VkBool32> vSupport(nQueueFamilly);

		for (uint32_t i = 0; i < nQueueFamilly; i++)
		{
			VKErrorCheck(vkGetPhysicalDeviceSurfaceSupportKHR(gpu, i, _surface, &vSupport[i]));
		}

		for (uint32_t i = 0; i < nQueueFamilly; ++i) 
		{
			if (vQueueFamily[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) 
			{
				_gpu = gpu;

				if (_idxGraphicsQueueFamily == UINT32_MAX)
					_idxGraphicsQueueFamily = i;

				if (vSupport[i] == VK_TRUE) {
					_idxGraphicsQueueFamily = i;
					_idxPresentQueueFamily = i;
					break;
				}
			}
		}

		if (_gpu != VK_NULL_HANDLE)
			break;
	}
	
	//적절한 GPU를 찾지 못함
	if (_gpu == VK_NULL_HANDLE) {
		assert(0 && "Vulkan ERROR: Queue family supporting graphics not found.");
		std::exit(-1);
	}

	//GPU 정보 갈무리
	vkGetPhysicalDeviceProperties(_gpu, &_gpuProperties);
	vkGetPhysicalDeviceFeatures(_gpu, &_gpuFeatures);
	vkGetPhysicalDeviceMemoryProperties(_gpu, &_gpuMemoryProperties);

	std::vector<VkDeviceQueueCreateInfo> vQueueCreateInfo;
	{
		VkDeviceQueueCreateInfo device_queue_create_info{};
		device_queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		device_queue_create_info.queueFamilyIndex = _idxGraphicsQueueFamily;
		device_queue_create_info.queueCount = 1;
		device_queue_create_info.pQueuePriorities = std::vector<float>{ 1.0f }.data();
		vQueueCreateInfo.push_back(device_queue_create_info);
	}
	{
		VkDeviceQueueCreateInfo device_queue_create_info{};
		device_queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		device_queue_create_info.queueFamilyIndex = _idxPresentQueueFamily;
		device_queue_create_info.queueCount = 1;
		device_queue_create_info.pQueuePriorities = std::vector<float>{ 1.0f }.data();
		vQueueCreateInfo.push_back(device_queue_create_info);
	}

	VkDeviceCreateInfo device_create_info{};
	device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	device_create_info.queueCreateInfoCount = vQueueCreateInfo.size();
	device_create_info.pQueueCreateInfos = vQueueCreateInfo.data();
	device_create_info.enabledLayerCount = 0;
	device_create_info.ppEnabledLayerNames = nullptr;
	device_create_info.enabledExtensionCount = _vDeviceExtensions.size();
	device_create_info.ppEnabledExtensionNames = _vDeviceExtensions.data();
	device_create_info.pEnabledFeatures = &_gpuFeatures;

	VKErrorCheck(vkCreateDevice(_gpu, &device_create_info, nullptr, &_device));

	vkGetDeviceQueue(_device, _idxGraphicsQueueFamily, 0, &_graphicsQueue);
	vkGetDeviceQueue(_device, _idxPresentQueueFamily, 0, &_presentQueue);
}

PFN_vkCreateDebugReportCallbackEXT		fvkCreateDebugReportCallbackEXT = nullptr;
PFN_vkDestroyDebugReportCallbackEXT		fvkDestroyDebugReportCallbackEXT = nullptr;

void VulkanDevice::InitDebug()
{
	//vkGetInstanceProcAddr 특정 함수에 대한 포인터를 얻어온다.
	fvkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(_vkInstance, "vkCreateDebugReportCallbackEXT");
	fvkDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(_vkInstance, "vkDestroyDebugReportCallbackEXT");
	if (nullptr == fvkCreateDebugReportCallbackEXT || nullptr == fvkDestroyDebugReportCallbackEXT) {
		assert(0 && "Vulkan ERROR: Can't fetch debug function pointers.");
		std::exit(-1);
	}

	fvkCreateDebugReportCallbackEXT(_vkInstance, &_debugCallbackCreateInfo, nullptr, &_debug_report);
}

void VulkanDevice::DeInitDebug()
{
	fvkDestroyDebugReportCallbackEXT(_vkInstance, _debug_report, nullptr);
	_debug_report = VK_NULL_HANDLE;
}

#else

void VulkanRenderer::_SetupDebug() {};
void VulkanRenderer::_InitDebug() {};
void VulkanRenderer::_DeInitDebug() {};

#endif // BUILD_ENABLE_VULKAN_DEBUG

void VulkanDevice::InitSwapchain()
{
	uint32_t format_count = 0;
	VKErrorCheck(vkGetPhysicalDeviceSurfaceFormatsKHR(_gpu, _surface, &format_count, nullptr));
	if (format_count == 0) {
		assert(0 && "Surface formats missing.");
		std::exit(-1);
	}

	VKErrorCheck(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_gpu, _surface, &_surfaceCapabilities));
	if (_surfaceCapabilities.currentExtent.width < UINT32_MAX) {
		_surfaceSizeX = _surfaceCapabilities.currentExtent.width;
		_surfaceSizeY = _surfaceCapabilities.currentExtent.height;
		_nSwapchainImage = _surfaceCapabilities.minImageCount;
	}

	{
		std::vector<VkSurfaceFormatKHR> formats(format_count);
		VKErrorCheck(vkGetPhysicalDeviceSurfaceFormatsKHR(_gpu, _surface, &format_count, formats.data()));
		if (formats[0].format == VK_FORMAT_UNDEFINED) {
			_surfaceFormat.format = VK_FORMAT_B8G8R8A8_UNORM;
			_surfaceFormat.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
		}
		else {
			_surfaceFormat = formats[0];
		}
	}

	VkPresentModeKHR present_mode = VK_PRESENT_MODE_FIFO_KHR;
	{
		uint32_t present_mode_count = 0;
		VKErrorCheck(vkGetPhysicalDeviceSurfacePresentModesKHR(_gpu, _surface, &present_mode_count, nullptr));
		std::vector<VkPresentModeKHR> present_mode_list(present_mode_count);
		VKErrorCheck(vkGetPhysicalDeviceSurfacePresentModesKHR(_gpu, _surface, &present_mode_count, present_mode_list.data()));
		for (auto m : present_mode_list) {
			if (m == VK_PRESENT_MODE_MAILBOX_KHR)
				present_mode = m;
		}
	}

	VkSwapchainKHR oldSwapchain = _swapchain;

	VkSwapchainCreateInfoKHR swapchain_create_info{};
	swapchain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchain_create_info.surface = _surface;
	swapchain_create_info.minImageCount = _nSwapchainImage;
	swapchain_create_info.imageFormat = _surfaceFormat.format;
	swapchain_create_info.imageColorSpace = _surfaceFormat.colorSpace;
	swapchain_create_info.imageExtent.width = _surfaceSizeX;
	swapchain_create_info.imageExtent.height = _surfaceSizeY;
	swapchain_create_info.imageArrayLayers = 1;
	swapchain_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapchain_create_info.queueFamilyIndexCount = 2;
	swapchain_create_info.pQueueFamilyIndices = std::vector<uint32_t>{ _idxGraphicsQueueFamily, _idxPresentQueueFamily }.data();
	swapchain_create_info.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchain_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchain_create_info.presentMode = present_mode;
	swapchain_create_info.clipped = VK_TRUE;
	swapchain_create_info.oldSwapchain = oldSwapchain;
	VKErrorCheck(vkCreateSwapchainKHR(_device, &swapchain_create_info, nullptr, &_swapchain));
}

void VulkanDevice::DeInitSwapchain()
{
	vkDestroySwapchainKHR(_device, _swapchain, nullptr);
}

void VulkanDevice::InitSwapchainImages()
{
	VKErrorCheck(vkGetSwapchainImagesKHR(_device, _swapchain, &_nSwapchainImage, nullptr));
	_vSwapchainImage.resize(_nSwapchainImage);
	VKErrorCheck(vkGetSwapchainImagesKHR(_device, _swapchain, &_nSwapchainImage, _vSwapchainImage.data()));

	_vSwapchainImageView.resize(_nSwapchainImage);
	for (uint32_t i = 0; i < _nSwapchainImage; ++i) {
		VkImageViewCreateInfo image_view_create_info{};
		image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		image_view_create_info.image = _vSwapchainImage[i];
		image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
		image_view_create_info.format = _surfaceFormat.format;
		image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		image_view_create_info.subresourceRange.baseMipLevel = 0;
		image_view_create_info.subresourceRange.levelCount = 1;
		image_view_create_info.subresourceRange.baseArrayLayer = 0;
		image_view_create_info.subresourceRange.layerCount = 1;
		VKErrorCheck(vkCreateImageView(_device, &image_view_create_info, nullptr, &_vSwapchainImageView[i]));
	}
}

void VulkanDevice::DeInitSwapchainImages()
{
	for (auto view : _vSwapchainImageView) {
		vkDestroyImageView(_device, view, nullptr);
	}
	_vSwapchainImageView.clear();
}

void VulkanDevice::InitDepthStencilImage()
{
	{
		std::vector<VkFormat> try_formats{
			VK_FORMAT_D32_SFLOAT_S8_UINT,
			VK_FORMAT_D24_UNORM_S8_UINT,
			VK_FORMAT_D16_UNORM_S8_UINT,
			VK_FORMAT_D32_SFLOAT,
			VK_FORMAT_D16_UNORM
		};
		for (auto format : try_formats) {
			VkFormatProperties format_properties{};
			vkGetPhysicalDeviceFormatProperties(_gpu, format, &format_properties);
			if (format_properties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
				_depthStencilFormat = format;
				break;
			}
		}
		if (_depthStencilFormat == VK_FORMAT_UNDEFINED) {
			assert(0 && "Depth stencil format not selected.");
			std::exit(-1);
		}
		if ((_depthStencilFormat == VK_FORMAT_D32_SFLOAT_S8_UINT) ||
			(_depthStencilFormat == VK_FORMAT_D24_UNORM_S8_UINT) ||
			(_depthStencilFormat == VK_FORMAT_D16_UNORM_S8_UINT) ||
			(_depthStencilFormat == VK_FORMAT_S8_UINT)) {
			_isStencilAvailable = true;
		}
	}

	VkImageCreateInfo image_create_info{};
	image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image_create_info.flags = 0;
	image_create_info.imageType = VK_IMAGE_TYPE_2D;
	image_create_info.format = _depthStencilFormat;
	image_create_info.extent.width = _surfaceSizeX;
	image_create_info.extent.height = _surfaceSizeY;
	image_create_info.extent.depth = 1;
	image_create_info.mipLevels = 1;
	image_create_info.arrayLayers = 1;
	image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
	image_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
	image_create_info.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	image_create_info.queueFamilyIndexCount = VK_QUEUE_FAMILY_IGNORED;
	image_create_info.pQueueFamilyIndices = nullptr;
	image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	VKErrorCheck(vkCreateImage(_device, &image_create_info, nullptr, &_depthStencilImage));

	VkMemoryRequirements image_memory_requirements{};
	vkGetImageMemoryRequirements(_device, _depthStencilImage, &image_memory_requirements);

	uint32_t memory_index = FindMemoryTypeIndex(&_gpuMemoryProperties, &image_memory_requirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	VkMemoryAllocateInfo memory_allocate_info{};
	memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memory_allocate_info.allocationSize = image_memory_requirements.size;
	memory_allocate_info.memoryTypeIndex = memory_index;

	VKErrorCheck(vkAllocateMemory(_device, &memory_allocate_info, nullptr, &_depthStencilImageMemory));
	VKErrorCheck(vkBindImageMemory(_device, _depthStencilImage, _depthStencilImageMemory, 0));

	VkImageViewCreateInfo image_view_create_info{};
	image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	image_view_create_info.image = _depthStencilImage;
	image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	image_view_create_info.format = _depthStencilFormat;
	image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | (_isStencilAvailable ? VK_IMAGE_ASPECT_STENCIL_BIT : 0);
	image_view_create_info.subresourceRange.baseMipLevel = 0;
	image_view_create_info.subresourceRange.levelCount = 1;
	image_view_create_info.subresourceRange.baseArrayLayer = 0;
	image_view_create_info.subresourceRange.layerCount = 1;

	VKErrorCheck(vkCreateImageView(_device, &image_view_create_info, nullptr, &_depthStencilImageView));
}

void VulkanDevice::DeInitDepthStencilImage()
{
	vkDestroyImageView(_device, _depthStencilImageView, nullptr);
	vkFreeMemory(_device, _depthStencilImageMemory, nullptr);
	vkDestroyImage(_device, _depthStencilImage, nullptr);
}
void VulkanDevice::InitFramebuffers()
{
	_vFramebuffer.resize(_nSwapchainImage);
	for (uint32_t i = 0; i < _nSwapchainImage; ++i) {
		std::array<VkImageView, 2> attachments{};
		attachments[0] = _depthStencilImageView;
		attachments[1] = _vSwapchainImageView[i];

		VkFramebufferCreateInfo framebuffer_create_info{};
		framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebuffer_create_info.renderPass = _renderPass;
		framebuffer_create_info.attachmentCount = attachments.size();
		framebuffer_create_info.pAttachments = attachments.data();
		framebuffer_create_info.width = _surfaceSizeX;
		framebuffer_create_info.height = _surfaceSizeY;
		framebuffer_create_info.layers = 1;

		VKErrorCheck(vkCreateFramebuffer(_device, &framebuffer_create_info, nullptr, &_vFramebuffer[i]));
	}
}

void VulkanDevice::DeInitFramebuffers()
{
	for (auto f : _vFramebuffer) {
		vkDestroyFramebuffer(_device, f, nullptr);
	}
	_vFramebuffer.clear();
}

void VulkanDevice::InitVertexBuffer()
{
	std::vector<Vertex> vertices = {
		{ { 0.0f, -0.5f }, { 1.0f, 1.0f, 1.0f, 1.0f} },
		{ { 0.5f, 0.5f }, { 1.0f, 1.0f, 1.0f, 1.0f } },
		{ { -0.5f, 0.5f }, { 1.0f, 1.0f, 1.0f, 1.0f } }
	};

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = sizeof(vertices[0]) * vertices.size();
	bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;		//그래픽 큐에서만 사용되므로 독점적 접근 모드.
	
	VKErrorCheck(vkCreateBuffer(_device, &bufferInfo, nullptr, &_vertexBuffer));

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(_device, _vertexBuffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = FindMemoryTypeIndex(&_gpuMemoryProperties, &memRequirements, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	VKErrorCheck(vkAllocateMemory(_device, &allocInfo, nullptr, &_vertexBufferMemory));
	VKErrorCheck(vkBindBufferMemory(_device, _vertexBuffer, _vertexBufferMemory, 0));

	void* data;
	VKErrorCheck(vkMapMemory(_device, _vertexBufferMemory, 0, bufferInfo.size, 0, &data));
		memcpy(data, vertices.data(), (size_t) bufferInfo.size);
	vkUnmapMemory(_device, _vertexBufferMemory);
}

//렌더링 중 사용할 프레임 버퍼에 대한 정보 제공 : 존재하는 (컬러, 깊이) 버퍼, 샘플 수 제공.
void VulkanDevice::InitRenderPass()
{
	std::vector<VkAttachmentDescription> attachments(2);
	attachments[0].flags = 0;
	attachments[0].format = _depthStencilFormat;
	attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[0].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	attachments[1].flags = 0;
	attachments[1].format = _surfaceFormat.format;
	attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[1].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference sub_pass_0_depth_stencil_attachment{};
	sub_pass_0_depth_stencil_attachment.attachment = 0;
	sub_pass_0_depth_stencil_attachment.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	std::vector<VkAttachmentReference> sub_pass_0_color_attachments(1);
	sub_pass_0_color_attachments[0].attachment = 1; 
	sub_pass_0_color_attachments[0].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	std::vector<VkSubpassDescription> sub_passes(1);
	sub_passes[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	sub_passes[0].colorAttachmentCount = sub_pass_0_color_attachments.size();
	sub_passes[0].pColorAttachments = sub_pass_0_color_attachments.data();		// layout(location=0) out vec4 FinalColor;
	sub_passes[0].pDepthStencilAttachment = &sub_pass_0_depth_stencil_attachment;

	VkRenderPassCreateInfo render_pass_create_info{};
	render_pass_create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	render_pass_create_info.attachmentCount = attachments.size();
	render_pass_create_info.pAttachments = attachments.data();
	render_pass_create_info.subpassCount = sub_passes.size();
	render_pass_create_info.pSubpasses = sub_passes.data();

	VKErrorCheck(vkCreateRenderPass(_device, &render_pass_create_info, nullptr, &_renderPass));
}

void VulkanDevice::DeInitRenderPass()
{
	vkDestroyRenderPass(_device, _renderPass, nullptr);
}

void VulkanDevice::InitSynchronizations()
{
	VkFenceCreateInfo fence_create_info{};
	fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	vkCreateFence(_device, &fence_create_info, nullptr, &_fenceSwapchainImage);
}

void VulkanDevice::DeInitSynchronizations()
{
	vkDestroyFence(_device, _fenceSwapchainImage, nullptr);
}

void VulkanDevice::InitCommandPool() {
	VkCommandPoolCreateInfo pool_create_info{};
	pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	pool_create_info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	pool_create_info.queueFamilyIndex = _idxGraphicsQueueFamily;
	VKErrorCheck(vkCreateCommandPool(_device, &pool_create_info, nullptr, &_commandPool));
	
}

void VulkanDevice::DeInitCommandPool() {
	vkDestroyCommandPool(_device, _commandPool, nullptr);
}

void VulkanDevice::InitCommandBuffer() {
	_vCommandBuffer.resize(_vFramebuffer.size());

	VkCommandBufferAllocateInfo	command_buffer_allocate_info{};
	command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	command_buffer_allocate_info.commandPool = _commandPool;
	command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	command_buffer_allocate_info.commandBufferCount = _vCommandBuffer.size();
	VKErrorCheck(vkAllocateCommandBuffers(_device, &command_buffer_allocate_info, _vCommandBuffer.data()));
}

void VulkanDevice::DeInitCommandBuffer() {
	_vCommandBuffer.clear();
	//CommandPool 소멸시 자동 소멸
	//	vkFreeCommandBuffers(_device, _command_pool, 1, &_command_buffer);
}

void VulkanDevice::InitSemaphore() {
	VkSemaphoreCreateInfo semaphore_create_info{};
	semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	VKErrorCheck(vkCreateSemaphore(_device, &semaphore_create_info, nullptr, &_semaphoreWait));
	VKErrorCheck(vkCreateSemaphore(_device, &semaphore_create_info, nullptr, &_semaphoreSignal));
}

void VulkanDevice::DeInitSemaphore() {
	vkDestroySemaphore(_device, _semaphoreWait, nullptr);
	vkDestroySemaphore(_device, _semaphoreSignal, nullptr);
}

std::vector<char> ReadFile(const std::string& fileName) {
	std::ifstream file(fileName, std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		throw std::runtime_error("failed to open file : " + fileName);
	}

	size_t fileSize = static_cast<size_t>(file.tellg());
	std::vector<char> buffer(fileSize);
	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();

	return buffer;
}

void VulkanDevice::LoadShader()
{

	auto vShaderCode = ReadFile(PathA("glsl_shader") + "cube_vert" + ".spv");
	VDeleter<VkShaderModule> vsModule { _device, vkDestroyShaderModule };
	VkShaderModuleCreateInfo vsModuleInfo = {};
	vsModuleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	vsModuleInfo.codeSize = vShaderCode.size();
	vsModuleInfo.pCode = reinterpret_cast<uint32_t*>(vShaderCode.data());
	VKErrorCheck(vkCreateShaderModule(_device, &vsModuleInfo, nullptr, &vsModule));

	VkPipelineShaderStageCreateInfo vsStageInfo = {};
	vsStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vsStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vsStageInfo.module = vsModule;
	vsStageInfo.pName = "main";
	vsStageInfo.pSpecializationInfo = nullptr;
	
	auto fShaderCode = ReadFile(PathA("glsl_shader") + "cube_frag" + ".spv");
	VDeleter<VkShaderModule> fsModule{ _device, vkDestroyShaderModule };
	VkShaderModuleCreateInfo fsModuleInfo = {};
	fsModuleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	fsModuleInfo.codeSize = fShaderCode.size();
	fsModuleInfo.pCode = reinterpret_cast<uint32_t*>(fShaderCode.data());
	VKErrorCheck(vkCreateShaderModule(_device, &fsModuleInfo, nullptr, &fsModule));

	VkPipelineShaderStageCreateInfo fsStageInfo = {};
	fsStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fsStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fsStageInfo.module = fsModule;
	fsStageInfo.pName = "main";
	fsStageInfo.pSpecializationInfo = nullptr;

	std::vector<VkPipelineShaderStageCreateInfo> vecShaderStageInfo { vsStageInfo, fsStageInfo };









	std::array<VkVertexInputBindingDescription, 1> arrBindDesc = {};
	arrBindDesc[0].binding = 0;
	arrBindDesc[0].stride = sizeof(Vertex);
	arrBindDesc[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	std::array<VkVertexInputAttributeDescription, 2> arrAttributeDesc;
	arrAttributeDesc[0].binding = 0;
	arrAttributeDesc[0].location = 0;
	arrAttributeDesc[0].format = VK_FORMAT_R32G32_SFLOAT;
	arrAttributeDesc[0].offset = offsetof(Vertex, pos);
	arrAttributeDesc[1].binding = 0;
	arrAttributeDesc[1].location = 1;
	arrAttributeDesc[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
	arrAttributeDesc[1].offset = offsetof(Vertex, color);










	VkPipelineVertexInputStateCreateInfo vInputInfo = {};
	vInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vInputInfo.vertexBindingDescriptionCount = arrBindDesc.size();
	vInputInfo.pVertexBindingDescriptions = arrBindDesc.data();
	vInputInfo.vertexAttributeDescriptionCount = arrAttributeDesc.size();
	vInputInfo.pVertexAttributeDescriptions = arrAttributeDesc.data();

	VkPipelineInputAssemblyStateCreateInfo iInputInfo = {};
	iInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	iInputInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	iInputInfo.primitiveRestartEnable = VK_FALSE;

	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(GetVulkanSurfaceSize().width);
	viewport.height = static_cast<float>(GetVulkanSurfaceSize().height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor = {};
	scissor.offset = {0, 0};
	scissor.extent = GetVulkanSurfaceSize();

	VkPipelineViewportStateCreateInfo viewportStateInfo = {};
	viewportStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportStateInfo.viewportCount = 1;
	viewportStateInfo.pViewports = &viewport;
	viewportStateInfo.scissorCount = 1;
	viewportStateInfo.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizerStateInfo = {};
	rasterizerStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizerStateInfo.depthClampEnable = VK_FALSE;
//rasterizerStateInfo.depthClampEnable = VK_TRUE;
	rasterizerStateInfo.rasterizerDiscardEnable = VK_FALSE;
	rasterizerStateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizerStateInfo.lineWidth = 1.0f;
	rasterizerStateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizerStateInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizerStateInfo.depthBiasEnable = VK_FALSE;
	rasterizerStateInfo.depthBiasConstantFactor = 0.0f;
	rasterizerStateInfo.depthBiasClamp = 0.0f;
	rasterizerStateInfo.depthBiasSlopeFactor = 0.0f;

	VkPipelineMultisampleStateCreateInfo multisampleStateInfo = {};
	multisampleStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampleStateInfo.sampleShadingEnable = VK_FALSE;
	multisampleStateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampleStateInfo.minSampleShading = 1.0f;
	multisampleStateInfo.pSampleMask = nullptr;
	multisampleStateInfo.alphaToCoverageEnable = VK_FALSE;
	multisampleStateInfo.alphaToOneEnable = VK_FALSE;

	VkPipelineDepthStencilStateCreateInfo depthStencilStateInfo;
	depthStencilStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilStateInfo.pNext = NULL;
	depthStencilStateInfo.flags = 0;
	depthStencilStateInfo.depthTestEnable = VK_TRUE;
	depthStencilStateInfo.depthWriteEnable = VK_TRUE;
	depthStencilStateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	depthStencilStateInfo.depthBoundsTestEnable = VK_FALSE;
	depthStencilStateInfo.minDepthBounds = 0;
	depthStencilStateInfo.maxDepthBounds = 0;
	depthStencilStateInfo.stencilTestEnable = VK_FALSE;
	depthStencilStateInfo.back.failOp = VK_STENCIL_OP_KEEP;
	depthStencilStateInfo.back.passOp = VK_STENCIL_OP_KEEP;
	depthStencilStateInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	depthStencilStateInfo.back.compareMask = 0;
	depthStencilStateInfo.back.reference = 0;
	depthStencilStateInfo.back.depthFailOp = VK_STENCIL_OP_KEEP;
	depthStencilStateInfo.back.writeMask = 0;
	depthStencilStateInfo.front = depthStencilStateInfo.back;

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colorBlendStateInfo = {};
	colorBlendStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendStateInfo.logicOpEnable = VK_FALSE;
	colorBlendStateInfo.logicOp = VK_LOGIC_OP_COPY;
	colorBlendStateInfo.attachmentCount = 1;
	colorBlendStateInfo.pAttachments = &colorBlendAttachment;
	colorBlendStateInfo.blendConstants[0] = 0.0f;
	colorBlendStateInfo.blendConstants[1] = 0.0f;
	colorBlendStateInfo.blendConstants[2] = 0.0f;
	colorBlendStateInfo.blendConstants[3] = 0.0f;

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 0;
	pipelineLayoutInfo.pSetLayouts = nullptr;
	pipelineLayoutInfo.pushConstantRangeCount = 0;
	pipelineLayoutInfo.pPushConstantRanges = 0;
	VKErrorCheck(vkCreatePipelineLayout(_device, &pipelineLayoutInfo, nullptr, &_pipelineLayout));

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = vecShaderStageInfo.size();
	pipelineInfo.pStages = vecShaderStageInfo.data();
	pipelineInfo.pVertexInputState = &vInputInfo;
	pipelineInfo.pInputAssemblyState = &iInputInfo;
	pipelineInfo.pViewportState = &viewportStateInfo;
	pipelineInfo.pRasterizationState = &rasterizerStateInfo;
	pipelineInfo.pMultisampleState = &multisampleStateInfo;
	pipelineInfo.pDepthStencilState = &depthStencilStateInfo;
	pipelineInfo.pColorBlendState = &colorBlendStateInfo;
	pipelineInfo.pDynamicState = nullptr;
	pipelineInfo.layout = _pipelineLayout;
	pipelineInfo.renderPass = _renderPass;
	pipelineInfo.subpass = 0;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.basePipelineIndex = -1;
	VKErrorCheck(vkCreateGraphicsPipelines(_device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &_pipeline));
}



void VulkanDevice::UnLoadShader()
{
	vkDestroyPipeline(_device, _pipeline, nullptr);
}

//Getter-------------------------------------------------------------------------------------------------------------------
const VkInstance VulkanDevice::GetVulkanInstance() const
{
	return _vkInstance;
}

const VkPhysicalDevice VulkanDevice::GetVulkanPhysicalDevice() const
{
	return _gpu;
}

const VkDevice VulkanDevice::GetVulkanDevice() const
{
	return _device;
}

const VkQueue VulkanDevice::GetVulkanQueue() const
{
	return _graphicsQueue;
}

const uint32_t VulkanDevice::GetVulkanGraphicsQueueFamilyIndex() const
{
	return _idxGraphicsQueueFamily;
}

const VkPhysicalDeviceProperties & VulkanDevice::GetVulkanPhysicalDeviceProperties() const
{
	return _gpuProperties;
}

const VkPhysicalDeviceMemoryProperties & VulkanDevice::GetVulkanPhysicalDeviceMemoryProperties() const
{
	return _gpuMemoryProperties;
}

VkRenderPass VulkanDevice::GetVulkanRenderPass()
{
	return _renderPass;
}

VkFramebuffer VulkanDevice::GetVulkanActiveFramebuffer()
{
	return _vFramebuffer[_activeSwapchainImageIndex];
}

VkExtent2D VulkanDevice::GetVulkanSurfaceSize()
{
	return { _surfaceSizeX, _surfaceSizeY };
}

//Render-------------------------------------------------------------------------------------------------------------------
void VulkanDevice::FrameAdvance()
{
	static float color_rotator = 0.0f;
	auto timer = std::chrono::steady_clock();
	auto last_time = timer.now();
	uint64_t frame_counter = 0;
	uint64_t fps = 0;

	color_rotator += 0.05;
	std::array<VkClearValue, 2> vClearValue{};
	vClearValue[0].depthStencil.depth = 0.0f;
	vClearValue[0].depthStencil.stencil = 0;
	vClearValue[1].color.float32[0] = std::sin(color_rotator + CIRCLE_THIRD_1) * 0.5 + 0.5;
	vClearValue[1].color.float32[1] = std::sin(color_rotator + CIRCLE_THIRD_2) * 0.5 + 0.5;
	vClearValue[1].color.float32[2] = std::sin(color_rotator + CIRCLE_THIRD_3) * 0.5 + 0.5;
	vClearValue[1].color.float32[3] = 1.0f;

	for (int i = 0; i < _vCommandBuffer.size(); ++i) {
		// Record command buffer
		VkCommandBufferBeginInfo command_buffer_begin_info{};
		command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		command_buffer_begin_info.pInheritanceInfo = nullptr;

		VKErrorCheck(vkBeginCommandBuffer(_vCommandBuffer[i], &command_buffer_begin_info));

			vkCmdSetLineWidth(_vCommandBuffer[i], 0);

			VkRenderPassBeginInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR;
			renderPassInfo.renderPass = _renderPass;
			renderPassInfo.framebuffer = _vFramebuffer[i];
			renderPassInfo.renderArea.offset = { 0, 0 };
			renderPassInfo.renderArea.extent = GetVulkanSurfaceSize();
			renderPassInfo.clearValueCount = vClearValue.size();
			renderPassInfo.pClearValues = vClearValue.data();

			vkCmdBeginRenderPass(_vCommandBuffer[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

				vkCmdBindPipeline(_vCommandBuffer[i], VK_PIPELINE_BIND_POINT_GRAPHICS, _pipeline);
		//		vkCmdBindDescriptorSets(_vCommandBuffer[i], VK_PIPELINE_BIND_POINT_GRAPHICS, _pipelineLayout, 0, 1, );



				VkBuffer vertexBuffers[] = { _vertexBuffer };
				VkDeviceSize offsets[] = { 0 };
				vkCmdBindVertexBuffers(_vCommandBuffer[i], 0, 1, vertexBuffers, offsets);



				vkCmdDraw(_vCommandBuffer[i], 3, 1, 0, 0);
		
			vkCmdEndRenderPass(_vCommandBuffer[i]);

			//VkRect2D render_area{};
			//render_area.offset.x = 0;
			//render_area.offset.y = 0;
			//render_area.extent = GetVulkanSurfaceSize();
			//
			//color_rotator += 0.01;
			//std::array<VkClearValue, 2> clear_values{};
			//clear_values[0].depthStencil.depth = 0.0f;
			//clear_values[0].depthStencil.stencil = 0;
			//clear_values[1].color.float32[0] = std::sin(color_rotator + CIRCLE_THIRD_1) * 0.5 + 0.5;
			//clear_values[1].color.float32[1] = std::sin(color_rotator + CIRCLE_THIRD_2) * 0.5 + 0.5;
			//clear_values[1].color.float32[2] = std::sin(color_rotator + CIRCLE_THIRD_3) * 0.5 + 0.5;
			//clear_values[1].color.float32[3] = 1.0f;
			//
			//VkRenderPassBeginInfo render_pass_begin_info{};
			//render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			//render_pass_begin_info.renderPass = GetVulkanRenderPass();
			//render_pass_begin_info.framebuffer = GetVulkanActiveFramebuffer();
			//render_pass_begin_info.renderArea = render_area;
			//render_pass_begin_info.clearValueCount = clear_values.size();
			//render_pass_begin_info.pClearValues = clear_values.data();
			//
			//vkCmdBeginRenderPass(_command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
			//
			//	vkCmdBindPipeline(_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _pipeline);
			//	
			//	vkCmdBindDescriptorSets(_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, info.pipeline_layout, 0, 1, info.desc_set.data(), 0, NULL);
			//
			//	const VkDeviceSize offsets[1] = { 0 };
			//	vkCmdBindVertexBuffers(info.cmd, 0, 1, &info.vertex_buffer.buf, offsets);
			//
			//	vkCmdDraw(info.cmd, 12 * 3, 1, 0, 0);
			//vkCmdEndRenderPass(_command_buffer);
			//
			////VkAttachmentDescription attachments[2];
			////attachments[0].format = info.format;
			////attachments[0].samples = NUM_SAMPLES;
			////attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			////attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			////attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			////attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			////attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			////attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			////attachments[0].flags = 0;
			////attachments[0].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			////attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			//
			////VkImageMemoryBarrier prePresentBarrier = {};
			////prePresentBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			////prePresentBarrier.pNext = NULL;
			////prePresentBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			////prePresentBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			////prePresentBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			////prePresentBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			////prePresentBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			////prePresentBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			////prePresentBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			////prePresentBarrier.subresourceRange.baseMipLevel = 0;
			////prePresentBarrier.subresourceRange.levelCount = 1;
			////prePresentBarrier.subresourceRange.baseArrayLayer = 0;
			////prePresentBarrier.subresourceRange.layerCount = 1;
			////prePresentBarrier.image = info.buffers[info.current_buffer].image;
			////vkCmdPipelineBarrier(info.cmd, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
			////	VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0,
			////	NULL, 1, &prePresentBarrier);
			//
			////do {
			////	res = vkWaitForFences(info.device, 1, &drawFence, VK_TRUE, FENCE_TIMEOUT);
			////} while (res == VK_TIMEOUT);

		VKErrorCheck(vkEndCommandBuffer(_vCommandBuffer[i]));
	}

	BeginRender();
		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		VkSemaphore waitSemaphores[] = { _semaphoreWait };
		VkSemaphore signalSemaphores[] = { _semaphoreSignal };
		// Submit command buffer
		VkSubmitInfo submit_info{};
		submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submit_info.pWaitDstStageMask = waitStages;
		submit_info.commandBufferCount = 1;
		submit_info.pCommandBuffers = &_vCommandBuffer[_activeSwapchainImageIndex];
		submit_info.waitSemaphoreCount = 1;
		submit_info.pWaitSemaphores = waitSemaphores;
		submit_info.signalSemaphoreCount = 1;
		submit_info.pSignalSemaphores = signalSemaphores;
		VKErrorCheck(vkQueueSubmit(_graphicsQueue, 1, &submit_info, VK_NULL_HANDLE));
	EndRender({ _semaphoreSignal });
}

void VulkanDevice::BeginRender()
{
	VKErrorCheck(vkAcquireNextImageKHR(
		_device,
		_swapchain,
		UINT64_MAX,
		_semaphoreWait,
		_fenceSwapchainImage,
		&_activeSwapchainImageIndex));
	VKErrorCheck(vkWaitForFences(_device, 1, &_fenceSwapchainImage, VK_TRUE, UINT64_MAX));
	VKErrorCheck(vkResetFences(_device, 1, &_fenceSwapchainImage));
	VKErrorCheck(vkQueueWaitIdle(_graphicsQueue));
}

void VulkanDevice::EndRender(std::vector<VkSemaphore> wait_semaphores)
{
	VkResult present_result = VkResult::VK_RESULT_MAX_ENUM;

	VkPresentInfoKHR present_info{};
	present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present_info.waitSemaphoreCount = wait_semaphores.size();
	present_info.pWaitSemaphores = wait_semaphores.data();
	present_info.swapchainCount = 1;
	present_info.pSwapchains = &_swapchain;
	present_info.pImageIndices = &_activeSwapchainImageIndex;
	present_info.pResults = &present_result;
	VKErrorCheck(vkQueuePresentKHR(_graphicsQueue, &present_info));
	VKErrorCheck(present_result);
}

/*	
	AquireNextImage
	cmd Begin Render Pass




	End Command Buffer

	Queue Submit
	Fence
	Present
*/
#endif