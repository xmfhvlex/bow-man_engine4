#pragma once

/*
	Renderer Interface
	Vulkan, Direct11, etc...
*/



class IDevice
{
public:
	IDevice() {}
	virtual ~IDevice() {}

	virtual void InitRenderDevice() = 0;
	virtual void DestroyRenderDevice() = 0;
	virtual void ResizeWindow() = 0;
	virtual void LoadShader() = 0;
	virtual void UnLoadShader() = 0;

	virtual void FrameAdvance() = 0;

private:

protected:
	inline static IDevice			*	_instance = nullptr;

#if USE_FRAMEWORK_GLFW
	GLFWwindow						*	_glfw_window					= nullptr;
#elif VK_USE_PLATFORM_WIN32_KHR
	HINSTANCE							_win32_instance					= NULL;
	HWND								_win32_window					= NULL;
	std::string							_win32_class_name;
	static uint64_t						_win32_class_id_counter;
#elif VK_USE_PLATFORM_XCB_KHR
	xcb_connection_t				*	_xcb_connection					= nullptr;
	xcb_screen_t					*	_xcb_screen						= nullptr;
	xcb_window_t						_xcb_window						= 0;
	xcb_intern_atom_reply_t			*	_xcb_atom_window_reply			= nullptr;
#endif

	UINT		_nScreenWidth;
	UINT		_nScreenHeight;

};