#pragma once
#include "IDevice.h"

#ifdef VULKAN_RENDERER

struct Vertex
{
	glm::vec2 pos;
	glm::vec4 color;
};

#if (BUILD_ENABLE_VULKAN_RUNTIME_DEBUG==1 && defined VULKAN_RENDERER)
	void VKErrorCheck(VkResult result);
	uint32_t FindMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties * gpu_memory_properties, const VkMemoryRequirements * memory_requirements, const VkMemoryPropertyFlags required_properties);
#else

#endif

#define GraphicsAPI VulkanDevice::GetInstance()

class VulkanDevice : public IDevice
{
public:
	static IDevice* GetInstance() {
		if (_instance == nullptr) {
			_instance = new VulkanDevice();
			_instance->InitRenderDevice();
		}
		return _instance;
	}

	VulkanDevice();
	virtual ~VulkanDevice();

	void										InitRenderDevice() override;
	void										DestroyRenderDevice() override;
	void										ResizeWindow() override;

	void										LoadShader() override;
	void										UnLoadShader() override;

	void										FrameAdvance() override;

private:
	void										InitExtention();

	void										InitInstance();

	void										InitDevice();

	void										InitDebug();
	void										DeInitDebug();

	void										InitSurface(HINSTANCE hInstance, HWND hWnd);
	void										DeInitSurface();

	void										InitSwapchain();
	void										DeInitSwapchain();

	void										InitSwapchainImages();
	void										DeInitSwapchainImages();

	void										InitDepthStencilImage();
	void										DeInitDepthStencilImage();

	void										InitRenderPass();
	void										DeInitRenderPass();

	void										InitFramebuffers();
	void										DeInitFramebuffers();

	void										InitVertexBuffer();

	void										InitSynchronizations();
	void										DeInitSynchronizations();

	void										InitCommandPool();
	void										DeInitCommandPool();

	void										InitCommandBuffer();
	void										DeInitCommandBuffer();

	void										InitSemaphore();
	void										DeInitSemaphore();

	void										BeginRender();
	void										EndRender(std::vector<VkSemaphore> wait_semaphores);

//Getter
	const VkInstance							GetVulkanInstance()	const;
	const VkPhysicalDevice						GetVulkanPhysicalDevice() const;
	const VkDevice								GetVulkanDevice() const;
	const VkQueue								GetVulkanQueue() const;
	const uint32_t								GetVulkanGraphicsQueueFamilyIndex() const;
	const VkPhysicalDeviceProperties		&	GetVulkanPhysicalDeviceProperties() const;
	const VkPhysicalDeviceMemoryProperties	&	GetVulkanPhysicalDeviceMemoryProperties() const;
	VkRenderPass								GetVulkanRenderPass();
	VkFramebuffer								GetVulkanActiveFramebuffer();
	VkExtent2D									GetVulkanSurfaceSize();

private:
	inline static VulkanDevice				*	_instance;

//Vulkan과 Application간의 연결
	VDeleter<VkInstance>						_vkInstance {vkDestroyInstance};

//System에서 사용가능한 GPU
	VkPhysicalDevice							_gpu = VK_NULL_HANDLE;
//사용할 GPU의 소프트웨어 핸들러
	VDeleter <VkDevice>							_device{ vkDestroyDevice };
//사용 GPU 정보
	VkPhysicalDeviceFeatures					_gpuFeatures = {};
	VkPhysicalDeviceProperties					_gpuProperties = {};
	VkPhysicalDeviceMemoryProperties			_gpuMemoryProperties = {};

	uint32_t									_idxGraphicsQueueFamily = 0;
	uint32_t									_idxPresentQueueFamily = 0;

	VDeleter<VkBuffer>							_vertexBuffer{ _device, vkDestroyBuffer };
	VDeleter<VkDeviceMemory>					_vertexBufferMemory{ _device, vkFreeMemory };

//수행할 명령 큐
	VkQueue										_graphicsQueue = VK_NULL_HANDLE;
	VkQueue										_presentQueue = VK_NULL_HANDLE;

	std::vector<const char*>					_vInstanceLayers;
	std::vector<const char*>					_vInstanceExtensions;
	std::vector<const char*>					_vDeviceExtensions;
//	std::vector<const char*>					_device_layers;					// depricated

	VkDebugReportCallbackEXT					_debug_report = VK_NULL_HANDLE;
	VkDebugReportCallbackCreateInfoEXT			_debugCallbackCreateInfo = {};

//Surface
	//Vulkan과 Window System을 연결해주는 WSI(Window System Integration)
	VkSurfaceKHR								_surface = VK_NULL_HANDLE;
	VkSurfaceFormatKHR							_surfaceFormat = {};
	VkSurfaceCapabilitiesKHR					_surfaceCapabilities = {};
	uint32_t									_surfaceSizeX = 512;
	uint32_t									_surfaceSizeY = 512;

//Depth Stencil
	VkImage										_depthStencilImage = VK_NULL_HANDLE;
	VkDeviceMemory								_depthStencilImageMemory = VK_NULL_HANDLE;
	VkImageView									_depthStencilImageView = VK_NULL_HANDLE;
	VkFormat									_depthStencilFormat = VK_FORMAT_UNDEFINED;
	bool										_isStencilAvailable = false;

//SwapChain
	VkSwapchainKHR								_swapchain = VK_NULL_HANDLE;
	//Swap Chain에 연결되는 실제 Image
	std::vector<VkImage>						_vSwapchainImage;
	//Swap Chain에 대한 Meta Data (RGB Component, View Type(2D/3D), Surface format, Mipmap, etc...)
	std::vector<VkFramebuffer>					_vFramebuffer;
	std::vector<VkImageView>					_vSwapchainImageView;
	uint32_t									_nSwapchainImage = 2;
	uint32_t									_activeSwapchainImageIndex = UINT32_MAX;

//Render Pass
	VkRenderPass								_renderPass = VK_NULL_HANDLE;

//Command
	std::vector<VkCommandBuffer>				_vCommandBuffer;
	VkCommandPool								_commandPool = VK_NULL_HANDLE;

//Pipeline
	VkPipelineLayout							_pipelineLayout = VK_NULL_HANDLE;
	VkPipeline									_pipeline = VK_NULL_HANDLE;

//Synchronize
	VkFence										_fenceSwapchainImage = VK_NULL_HANDLE;
	VkSemaphore									_semaphoreWait = VK_NULL_HANDLE;
	VkSemaphore									_semaphoreSignal = VK_NULL_HANDLE;
};

#endif