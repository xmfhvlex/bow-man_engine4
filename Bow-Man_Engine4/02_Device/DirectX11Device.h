#pragma once

#ifdef DIRECT_X_11_RENDERER
#include "IDevice.h"

enum ECBuffer : int {
	ETC,
	CAMERA,
	WVP,
	LIGHT,
	MATERIAL,
};


#define GraphicsAPI  DirectX11Device::GetInstance()
#define Device DirectX11Device::GetInstance()->GetDevice()
#define ImmediateContext DirectX11Device::GetInstance()->GetImmediate()

class DirectX11Device : public IDevice
{
public:
	static DirectX11Device* GetInstance() {
		if (_instance == nullptr) {
			_instance = new DirectX11Device();
			_instance->InitRenderDevice();
		}
		return _instance;
	}

	void CreateDevice();
	void CreateSwapChain();
	void CreateDepthStencilBuffer();
	void CreateBuffers();

	void InitRenderDevice() override;
	void DestroyRenderDevice() override;
	void ResizeWindow() override;
	void LoadShader() override;
	void UnLoadShader() override;

	void FrameAdvance() override;

	ComPtr <ID3D11Device> GetDevice();
	ComPtr <ID3D11DeviceContext> GetImmediate();

private:
	DirectX11Device() {}
	~DirectX11Device() {}

private:
	inline static DirectX11Device* _instance;

	//Direct 3D
	ComPtr <ID3D11Device>			_device = nullptr;
	ComPtr <ID3D11DeviceContext>	_immediateContext = nullptr;
	ComPtr <ID3D11DeviceContext>	_deferredContext = nullptr;
	
	//Frame Buffers
	ComPtr <IDXGISwapChain>				_dxgiSwapChain = nullptr;
	ComPtr <ID3D11Texture2D>			_texBackBuffer = nullptr;
	ComPtr <ID3D11RenderTargetView>		_rtvBackBuffer = nullptr;
	ComPtr <ID3D11ShaderResourceView>	_srvBackBuffer = nullptr;
	ComPtr <ID3D11UnorderedAccessView>	_uavBackBuffer = nullptr;

	//Depth Stencil BUffer
	ComPtr <ID3D11Texture2D>			_texDepthStencil = nullptr;
	ComPtr <ID3D11DepthStencilView>		_dsvDepthStencil = nullptr;
	ComPtr <ID3D11DepthStencilView>		_dsvDepthStencilReadOnly = nullptr;
	ComPtr <ID3D11ShaderResourceView>	_srvDepthStencil = nullptr;

	//Muti-Sampling
	UINT								_n4xMSAAQuality = 0;

	ComPtr <ID3D11InputLayout>			_inputLayout = nullptr;
	ComPtr <ID3D11ComputeShader>		_csCalculateFrustum = nullptr;
	ComPtr <ID3D11ComputeShader>		_csLightCulling = nullptr;
	ComPtr <ID3D11ComputeShader>		_csOcclusion = nullptr;

	ComPtr <ID3D11Buffer>				_costbuffer1 = nullptr;

public:
	inline static UINT*					_entityOcclusion;
};

#endif