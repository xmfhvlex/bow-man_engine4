#include "stdafx.h"
#include "DirectX11Device.h"

#ifdef DIRECT_X_11_RENDERER
//#include "D2DFramework.h"
//#include "UiObject.h"

#include "Utility.h"
#include "ShaderManager.h"
#include "InputManager.h"

#include "Entity.h"
#include "Camera.h"
#include "CameraManager.h"
#include "EntityManager.h"

#include "InputController.h"
#include "MeshRenderer.h"
#include "SkyBoxRenderer.h"
#include "SectorMesh.h"
#include "SkyBoxMesh.h"
#include "CubeMesh.h"
#include "Skybox.h"
#include "Light.h"

#include "PathManager.h"
#include "BehaviorManager.h"
#include "RenderStateManager.h"

#include <UIFramework.h>

//#define DIR_LIGHT	0
//#define POINT_LIGHT 1
//#define SPOT_LIGHT	2
//struct Light
//{
//	XMFLOAT3	positionWS;
//	float		range;
//	//----------------------
//	XMFLOAT3	positionVS;
//	float		spotlightAngle;
//	//----------------------
//	XMFLOAT3	directionWS;
//	float		intensity;
//	//----------------------
//	XMFLOAT3	directionVS;
//	UINT		type;
//	//----------------------
//	XMFLOAT3	color;
//	bool		enabled;
//	//----------------------
//};
//
//#define MAX_LIGHT_COUNT 100
//
//struct LightList
//{
//	Light gArrDirLight[MAX_LIGHT_COUNT];
//	int gLightPivot;
//	int padding[3];
//};


struct EtcDataBuffer
{
	UINT nWidth = 0;
	UINT nHeight = 0;
	UINT dispatchX = 0;
	UINT dispatchY = 0;
	UINT maxEntityCount = 0;
	UINT padding[3];
};

struct Plane
{
	XMFLOAT3 normal;
	float distance;
};

struct Frustum
{
	Plane planes[4];
};

void DirectX11Device::CreateDevice() {
	//UINT dwCreateDeviceFlags = D3D11_CREATE_DEVICE_SINGLETHREADED;
	//D2D1_FACTORY_TYPE_MULTI_THREADED;
	UINT dwCreateDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT | D3D11_CREATE_DEVICE_SINGLETHREADED
		;
#ifdef _DEBUG
	dwCreateDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE DriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};
	UINT nDriverTypes = sizeof(DriverTypes) / sizeof(D3D_DRIVER_TYPE);

	D3D_FEATURE_LEVEL pFeatureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};

	UINT nFeatureLevels = _ARRAYSIZE(pFeatureLevels);
	D3D_DRIVER_TYPE nDriverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL nFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	HRESULT hResult = S_OK;

	for (UINT i = 0; i < nDriverTypes; i++)
	{
		nDriverType = DriverTypes[i];
		if (SUCCEEDED(hResult = D3D11CreateDevice(NULL, nDriverType, NULL, dwCreateDeviceFlags, pFeatureLevels, nFeatureLevels, D3D11_SDK_VERSION, &_device, &nFeatureLevel, &_immediateContext))) break;
	}
	if (!_device) {
		MessageBox(NULL, L"D3D11Device Creation Failure", L"Error", NULL);
		exit(-1);
	}
//	_device->CreateDeferredContext(NULL, &_deferredContext);
}

//#define _WITH_MSAA4_MULTISAMPLING

void DirectX11Device::CreateSwapChain() {
	HRESULT hResult = S_OK;

	RECT rcClient;
	GetClientRect(gHwnd, &rcClient);
	_nScreenWidth = rcClient.right - rcClient.left;
	_nScreenHeight = rcClient.bottom - rcClient.top;

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	::ZeroMemory(&dxgiSwapChainDesc, sizeof(dxgiSwapChainDesc));
	dxgiSwapChainDesc.BufferCount = 2;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 120;
	//dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	//UNORDERED_ACCESS가 있으면 멀티 샘플링 텍스쳐를 생성할 수 없는듯 하다
//	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT | DXGI_USAGE_UNORDERED_ACCESS;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = gHwnd;
	dxgiSwapChainDesc.Windowed = TRUE;
	dxgiSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_SEQUENTIAL;
	/*전체화면 모드로 전환할 때 현재의 후면 버퍼 설정에 가장 잘 부합하는 디스플레이 모드가 자동적으로 선택된다*/
	dxgiSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
#ifdef _WITH_MSAA4_MULTISAMPLING
	HR(hResult = _device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &_n4xMSAAQuality));
	dxgiSwapChainDesc.SampleDesc.Count = 4;
	dxgiSwapChainDesc.SampleDesc.Quality = _n4xMSAAQuality - 1;
#else
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
#endif

	ComPtr <IDXGIFactory1> dxgiFactory = NULL;
	HR(hResult = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void **)dxgiFactory.ReleaseAndGetAddressOf()));
	ComPtr <IDXGIDevice> dxgiDevice = NULL;
	HR(hResult = _device->QueryInterface(__uuidof(IDXGIDevice), (void **)dxgiDevice.ReleaseAndGetAddressOf()));

	if (_dxgiSwapChain == nullptr) {
		HR(hResult = dxgiFactory->CreateSwapChain(dxgiDevice.Get(), &dxgiSwapChainDesc, _dxgiSwapChain.ReleaseAndGetAddressOf()));
	}
	else {
		_texBackBuffer.Reset();
		_rtvBackBuffer.Reset();
		_srvBackBuffer.Reset();
		_uavBackBuffer.Reset();
	//'	HR(hResult = _dxgiSwapChain->ResizeBuffers(2, 0, 0, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));
	}

	//DXGI_USAGE a;
	//ID3D11Texture2D *pBackBuffer;
	//HR(m_pDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&pBackBuffer));
	//pBackBuffer->IDXGIResource::GetUsage(&a);

	HR(_dxgiSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)_texBackBuffer.ReleaseAndGetAddressOf()));
	HR(_device->CreateRenderTargetView(_texBackBuffer.Get(), NULL, _rtvBackBuffer.ReleaseAndGetAddressOf()));

	//D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	//::ZeroMemory(&SRVDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	//SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	//SRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//SRVDesc.Texture2D.MipLevels = 1;
	//HR(_device->CreateShaderResourceView(_texBackBuffer.Get(), &SRVDesc, _srvBackBuffer.ReleaseAndGetAddressOf()));

	//D3D11_UNORDERED_ACCESS_VIEW_DESC UAViewDesc;
	//UAViewDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//UAViewDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
	//UAViewDesc.Texture2D.MipSlice = 0;
	//HR(_device->CreateUnorderedAccessView(_texBackBuffer.Get(), &UAViewDesc, _uavBackBuffer.ReleaseAndGetAddressOf()));
}

void DirectX11Device::CreateDepthStencilBuffer() {
	// Create depth stencil texture
	D3D11_TEXTURE2D_DESC BufferDesc;
	ZeroMemory(&BufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
	BufferDesc.Width = _nScreenWidth;
	BufferDesc.Height = _nScreenHeight;
	BufferDesc.MipLevels = 1;
	BufferDesc.ArraySize = 1;
	BufferDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	BufferDesc.Usage = D3D11_USAGE_DEFAULT;
	BufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	BufferDesc.CPUAccessFlags = 0;
	BufferDesc.MiscFlags = 0;
#ifdef _WITH_MSAA4_MULTISAMPLING
	BufferDesc.SampleDesc.Count = 4;
	BufferDesc.SampleDesc.Quality = _n4xMSAAQuality - 1;
#else
	BufferDesc.SampleDesc.Count = 1;
	BufferDesc.SampleDesc.Quality = 0;
#endif

	HR(_device->CreateTexture2D(&BufferDesc, NULL, _texDepthStencil.ReleaseAndGetAddressOf()));

	//DepthStencilView
	D3D11_DEPTH_STENCIL_VIEW_DESC DepthStencilViewDesc;
	ZeroMemory(&DepthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	DepthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
#ifdef _WITH_MSAA4_MULTISAMPLING
	DepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
#else
	DepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
#endif
	DepthStencilViewDesc.Texture2D.MipSlice = 0;
	HR(_device->CreateDepthStencilView(_texDepthStencil.Get(), &DepthStencilViewDesc, _dsvDepthStencil.ReleaseAndGetAddressOf()));

	DepthStencilViewDesc.Flags = D3D11_DSV_READ_ONLY_DEPTH | D3D11_DSV_READ_ONLY_STENCIL;
	HR(_device->CreateDepthStencilView(_texDepthStencil.Get(), &DepthStencilViewDesc, _dsvDepthStencilReadOnly.ReleaseAndGetAddressOf()));

	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	::ZeroMemory(&SRVDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
#ifdef _WITH_MSAA4_MULTISAMPLING
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
#else
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
#endif
	SRVDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	SRVDesc.Texture2D.MipLevels = 1;
	SRVDesc.Texture2D.MostDetailedMip = 0;
	HR(_device->CreateShaderResourceView(_texDepthStencil.Get(), &SRVDesc, _srvDepthStencil.ReleaseAndGetAddressOf()));

	//D2D1_BITMAP_PROPERTIES bmpprops;
	//bmpprops.dpiX = 96.0f;
	//bmpprops.dpiY = 96.0f;
	//bmpprops.pixelFormat = { DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_IGNORE };

	//IDXGISurface *pDxgiSurface = nullptr;
	//HR(m_pTexDepthStencil->QueryInterface(&pDxgiSurface));
	//D2DFramework->GetRenderTarget()->CreateSharedBitmap(__uuidof(IDXGISurface), (void*)pDxgiSurface, &bmpprops, &m_pBitDepthStencil);
}

ComPtr<ID3D11Texture2D> texAlbedo = nullptr;
ComPtr<ID3D11RenderTargetView> rtvAlbedo = nullptr;
ComPtr<ID3D11ShaderResourceView> srvAlbedo = nullptr;
ComPtr<ID3D11Texture2D> texNormal = nullptr;
ComPtr<ID3D11RenderTargetView> rtvNormal = nullptr;
ComPtr<ID3D11ShaderResourceView> srvNormal = nullptr;
ComPtr<ID3D11Texture2D> texHDR = nullptr;
ComPtr<ID3D11RenderTargetView> rtvHDR = nullptr;
ComPtr<ID3D11ShaderResourceView> srvHDR = nullptr;
ComPtr<ID3D11UnorderedAccessView> uavHDR = nullptr;

ComPtr<ID3D11Texture2D> texOcclusion = nullptr;
ComPtr<ID3D11RenderTargetView> rtvOcclusion = nullptr;
ComPtr<ID3D11ShaderResourceView> srvOcclusion = nullptr;
ComPtr<ID3D11UnorderedAccessView> uavOcclusion = nullptr;

ComPtr <ID3D11Buffer> buffOcclusionFlag = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavOcclusionFlag = nullptr;

ComPtr <ID3D11Buffer> buffFrustumBuffer = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavFrustumBuffer = nullptr;
ComPtr<ID3D11ShaderResourceView> srvFrustumBuffer = nullptr;


ComPtr <ID3D11Buffer> buffOLightIndexCounter = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavOLightIndexCounter = nullptr;

ComPtr <ID3D11Buffer> buffOLightIndexList = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavOLightIndexList = nullptr;
ComPtr <ID3D11ShaderResourceView>	srvOLightIndexList = nullptr;

ComPtr <ID3D11Texture2D> texOLightGrid = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavOLightGrid = nullptr;
ComPtr <ID3D11ShaderResourceView>	srvOLightGrid = nullptr;


ComPtr <ID3D11Buffer> buffTLightIndexCounter = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavTLightIndexCounter = nullptr;

ComPtr <ID3D11Buffer> buffTLightIndexList = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavTLightIndexList = nullptr;
ComPtr <ID3D11ShaderResourceView>	srvTLightIndexList = nullptr;

ComPtr <ID3D11Texture2D> texTLightGrid = nullptr;
ComPtr <ID3D11UnorderedAccessView>	uavTLightGrid = nullptr;
ComPtr <ID3D11ShaderResourceView>	srvTLightGrid = nullptr;

ID3D11RenderTargetView		*RTV_NULL[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
ID3D11UnorderedAccessView	*UAV_NULL[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
ID3D11ShaderResourceView	*SRV_NULL[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
ID3D11Buffer				*BUF_NULL[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };

ComPtr<ID3D11SamplerState>	_samplerState;

UINT dispatchX;
UINT dispatchY;

void DirectX11Device::CreateBuffers()
{
	D3D11_TEXTURE2D_DESC texDesc;
	InitMemory(texDesc);
	texDesc.Width = _nScreenWidth;
	texDesc.Height = _nScreenHeight;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	InitMemory(rtvDesc);
#ifdef _WITH_MSAA4_MULTISAMPLING
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
#else
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
#endif

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	InitMemory(srvDesc);
#ifdef _WITH_MSAA4_MULTISAMPLING
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
#else
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
#endif
	srvDesc.Texture2D.MipLevels = 1;

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
#ifdef _WITH_MSAA4_MULTISAMPLING
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
#else
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
#endif
	uavDesc.Texture2D.MipSlice = 0;

	////Albedo
	//texDesc.Format = rtvDesc.Format = srvDesc.Format = uavDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//HR(_device->CreateTexture2D(&texDesc, nullptr, texAlbedo.ReleaseAndGetAddressOf()));
	//HR(_device->CreateRenderTargetView(texAlbedo.Get(), &rtvDesc, rtvAlbedo.ReleaseAndGetAddressOf()));
	//HR(_device->CreateShaderResourceView(texAlbedo.Get(), &srvDesc, srvAlbedo.ReleaseAndGetAddressOf()));

	////Normal
	//texDesc.Format = rtvDesc.Format = srvDesc.Format = uavDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	//HR(_device->CreateTexture2D(&texDesc, nullptr, texNormal.ReleaseAndGetAddressOf()));
	//HR(_device->CreateRenderTargetView(texNormal.Get(), &rtvDesc, rtvNormal.ReleaseAndGetAddressOf()));
	//HR(_device->CreateShaderResourceView(texNormal.Get(), &srvDesc, srvNormal.ReleaseAndGetAddressOf()));

	//HDR
	texDesc.Format = rtvDesc.Format = srvDesc.Format = uavDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	HR(_device->CreateTexture2D(&texDesc, nullptr, texHDR.ReleaseAndGetAddressOf()));
	HR(_device->CreateRenderTargetView(texHDR.Get(), &rtvDesc, rtvHDR.ReleaseAndGetAddressOf()));
	HR(_device->CreateShaderResourceView(texHDR.Get(), &srvDesc, srvHDR.ReleaseAndGetAddressOf()));
	HR(_device->CreateUnorderedAccessView(texHDR.Get(), &uavDesc, uavHDR.ReleaseAndGetAddressOf()));

	//Constant Buffer
	UINT nWidth = 0;
	UINT nHeight = 0;
	UINT padding[2];
	dispatchX = (UINT)ceil(static_cast<float>(_nScreenWidth) / 16);
	dispatchY = (UINT)ceil(static_cast<float>(_nScreenHeight)/ 16);
	//std::tuple<UINT, UINT, UINT, UINT>

	CreateConstantBuffers<EtcDataBuffer>(_device.Get(), _costbuffer1.ReleaseAndGetAddressOf());
	UpdateConstantBuffer(_immediateContext.Get(), _costbuffer1.Get(), EtcDataBuffer{ _nScreenWidth, _nScreenHeight, dispatchX, dispatchY, Max_Entity_Count });

	//Occlusion
	texDesc.Format = rtvDesc.Format = srvDesc.Format = uavDesc.Format = DXGI_FORMAT_R32_UINT;
	HR(_device->CreateTexture2D(&texDesc, nullptr, texOcclusion.ReleaseAndGetAddressOf()));
	HR(_device->CreateRenderTargetView(texOcclusion.Get(), &rtvDesc, rtvOcclusion.ReleaseAndGetAddressOf()));
	HR(_device->CreateShaderResourceView(texOcclusion.Get(), &srvDesc, srvOcclusion.ReleaseAndGetAddressOf()));
	HR(_device->CreateUnorderedAccessView(texOcclusion.Get(), &uavDesc, uavOcclusion.ReleaseAndGetAddressOf()));

	//Occlusion Buffer
	D3D11_BUFFER_DESC sbDesc;
	sbDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	sbDesc.CPUAccessFlags = 0;
	sbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	sbDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	sbDesc.StructureByteStride = sizeof(UINT);
	sbDesc.ByteWidth = sizeof(UINT) * Max_Entity_Count;
	sbDesc.Usage = D3D11_USAGE_DEFAULT;
	HR(_device->CreateBuffer(&sbDesc, NULL, buffOcclusionFlag.ReleaseAndGetAddressOf()));

	D3D11_UNORDERED_ACCESS_VIEW_DESC sbUAVDesc;
	sbUAVDesc.Buffer.FirstElement = 0;
	sbUAVDesc.Buffer.Flags = 0;
	sbUAVDesc.Buffer.NumElements = Max_Entity_Count;
	sbUAVDesc.Format = DXGI_FORMAT_UNKNOWN;
	sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	HR(_device->CreateUnorderedAccessView(buffOcclusionFlag.Get(), &sbUAVDesc, uavOcclusionFlag.ReleaseAndGetAddressOf()));

	//Frustum Buffer
	sbDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
	sbDesc.CPUAccessFlags = 0;
	sbDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	sbDesc.StructureByteStride = sizeof(Frustum);
	sbDesc.ByteWidth = sizeof(Frustum) * dispatchX * dispatchY;
	sbDesc.Usage = D3D11_USAGE_DEFAULT;
	HR(_device->CreateBuffer(&sbDesc, NULL, buffFrustumBuffer.ReleaseAndGetAddressOf()));

	sbUAVDesc.Buffer.FirstElement = 0;
	sbUAVDesc.Buffer.Flags = 0;
	sbUAVDesc.Buffer.NumElements = dispatchX * dispatchY;
	sbUAVDesc.Format = DXGI_FORMAT_UNKNOWN;
	sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	HR(_device->CreateUnorderedAccessView(buffFrustumBuffer.Get(), &sbUAVDesc, uavFrustumBuffer.ReleaseAndGetAddressOf()));

	InitMemory(srvDesc);
	srvDesc.Buffer.ElementWidth = sizeof(Frustum);
	srvDesc.Buffer.NumElements = dispatchX * dispatchY;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	HR(_device->CreateShaderResourceView(buffFrustumBuffer.Get(), &srvDesc, srvFrustumBuffer.ReleaseAndGetAddressOf()));




	//Opaque Light Index Counter
	sbDesc.StructureByteStride = sizeof(UINT);
	sbDesc.ByteWidth = sizeof(UINT);
	HR(_device->CreateBuffer(&sbDesc, NULL, buffOLightIndexCounter.ReleaseAndGetAddressOf()));

		sbUAVDesc.Buffer.NumElements = 1;
		sbUAVDesc.Format = DXGI_FORMAT_UNKNOWN;
		sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		HR(_device->CreateUnorderedAccessView(buffOLightIndexCounter.Get(), &sbUAVDesc, uavOLightIndexCounter.ReleaseAndGetAddressOf()));

	sbDesc.StructureByteStride = sizeof(UINT);
	sbDesc.ByteWidth = sizeof(UINT) * dispatchX * dispatchY * 256;
	HR(_device->CreateBuffer(&sbDesc, NULL, buffOLightIndexList.ReleaseAndGetAddressOf()));

		sbUAVDesc.Buffer.NumElements = dispatchX * dispatchY * 256;
		sbUAVDesc.Format = DXGI_FORMAT_UNKNOWN;
		sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		HR(_device->CreateUnorderedAccessView(buffOLightIndexList.Get(), &sbUAVDesc, uavOLightIndexList.ReleaseAndGetAddressOf()));

		InitMemory(srvDesc);
		srvDesc.Buffer.ElementWidth = sizeof(UINT);
		srvDesc.Buffer.NumElements = dispatchX * dispatchY * 256;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		HR(_device->CreateShaderResourceView(buffOLightIndexList.Get(), &srvDesc, srvOLightIndexList.ReleaseAndGetAddressOf()));

	texDesc.Width = dispatchX;
	texDesc.Height = dispatchY;
	texDesc.ArraySize = 1;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	texDesc.Format = DXGI_FORMAT_R32G32_UINT;
	HR(_device->CreateTexture2D(&texDesc, NULL, texOLightGrid.ReleaseAndGetAddressOf()));

		sbUAVDesc.Buffer.NumElements = dispatchX * dispatchY;
		sbUAVDesc.Format = DXGI_FORMAT_R32G32_UINT;
		sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		HR(_device->CreateUnorderedAccessView(texOLightGrid.Get(), &sbUAVDesc, uavOLightGrid.ReleaseAndGetAddressOf()));

		InitMemory(srvDesc);
		srvDesc.Texture2D.MipLevels = 1;
		srvDesc.Format = DXGI_FORMAT_R32G32_UINT;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		HR(_device->CreateShaderResourceView(texOLightGrid.Get(), &srvDesc, srvOLightGrid.ReleaseAndGetAddressOf()));



	//Transparent Light Index Counter
	sbDesc.StructureByteStride = sizeof(UINT);
	sbDesc.ByteWidth = sizeof(UINT);
	HR(_device->CreateBuffer(&sbDesc, NULL, buffTLightIndexCounter.ReleaseAndGetAddressOf()));

	sbUAVDesc.Buffer.NumElements = 1;
	sbUAVDesc.Format = DXGI_FORMAT_UNKNOWN;
	sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	HR(_device->CreateUnorderedAccessView(buffTLightIndexCounter.Get(), &sbUAVDesc, uavTLightIndexCounter.ReleaseAndGetAddressOf()));

	sbDesc.StructureByteStride = sizeof(UINT);
	sbDesc.ByteWidth = sizeof(UINT) * dispatchX * dispatchY * 256;
	HR(_device->CreateBuffer(&sbDesc, NULL, buffTLightIndexList.ReleaseAndGetAddressOf()));

	sbUAVDesc.Buffer.NumElements = dispatchX * dispatchY * 256;
	sbUAVDesc.Format = DXGI_FORMAT_UNKNOWN;
	sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	HR(_device->CreateUnorderedAccessView(buffTLightIndexList.Get(), &sbUAVDesc, uavTLightIndexList.ReleaseAndGetAddressOf()));

	InitMemory(srvDesc);
	srvDesc.Buffer.ElementWidth = sizeof(UINT);
	srvDesc.Buffer.NumElements = dispatchX * dispatchY * 256;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	HR(_device->CreateShaderResourceView(buffTLightIndexList.Get(), &srvDesc, srvTLightIndexList.ReleaseAndGetAddressOf()));

	texDesc.Width = dispatchX;
	texDesc.Height = dispatchY;
	texDesc.ArraySize = 1;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	texDesc.Format = DXGI_FORMAT_R32G32_UINT;
	HR(_device->CreateTexture2D(&texDesc, NULL, texTLightGrid.ReleaseAndGetAddressOf()));

	sbUAVDesc.Buffer.NumElements = dispatchX * dispatchY;
	sbUAVDesc.Format = DXGI_FORMAT_R32G32_UINT;
	sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
	HR(_device->CreateUnorderedAccessView(texTLightGrid.Get(), &sbUAVDesc, uavTLightGrid.ReleaseAndGetAddressOf()));

	InitMemory(srvDesc);
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Format = DXGI_FORMAT_R32G32_UINT;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	HR(_device->CreateShaderResourceView(texTLightGrid.Get(), &srvDesc, srvTLightGrid.ReleaseAndGetAddressOf()));
}


static int num = 1000;
PROCESS_MEMORY_COUNTERS_EX pmc;
void DirectX11Device::InitRenderDevice()
{
	CreateDevice();
	CreateSwapChain();
	CreateDepthStencilBuffer();
	CreateBuffers();
	LoadShader();	
	

	//Direct 2D UI Framework
	//gpDevice = _device.Get();
	//SCREEN_WIDTH = _nScreenWidth; 
	//SCREEN_HEIGHT = _nScreenHeight;
	UIFramework->Initialize(gHwnd, _device, _immediateContext, _texBackBuffer);


	auto uiManager = UIFramework->GetUIManager();
	CUiObject* pUiObject = uiManager->CreateUi(L"System", UiElement::TITLE | UiElement::EXIT | UiElement::MINIMIZE | UiElement::RESIZE);
	CUiObject* pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	pChild->SetSize(20, 20);
	pChild->SetData(pmc.WorkingSetSize);
	pChild->SetRenderFunc(RenderString);
	cout << num << endl;

//	pChild->SetData(&CScene::m_nRenderedObject);
	pUiObject->AddChild(pChild);
	pUiObject->SetPosition(100, 500);
}

void DirectX11Device::DestroyRenderDevice()
{

}

void DirectX11Device::ResizeWindow()
{
	_immediateContext->OMSetRenderTargets(_ARRAYSIZE(RTV_NULL), RTV_NULL, nullptr);
	_immediateContext->CSSetUnorderedAccessViews(0, _ARRAYSIZE(UAV_NULL), UAV_NULL, NULL);
	_immediateContext->CSSetShaderResources(0, _ARRAYSIZE(SRV_NULL), SRV_NULL);
	_immediateContext->CSSetConstantBuffers(0, _ARRAYSIZE(BUF_NULL), BUF_NULL);

	//_texDepthStencil.Reset();
	//_srvDepthStencil.Reset();
	//_dsvDepthStencilReadOnly.Reset();
	//_dsvDepthStencil.Reset();

	CreateSwapChain();
	CreateBuffers();
	CreateDepthStencilBuffer();

	UpdateConstantBuffer(_immediateContext.Get(), _costbuffer1.Get(), EtcDataBuffer{_nScreenWidth, _nScreenHeight, dispatchX, dispatchX, Max_Entity_Count });
}

void DirectX11Device::LoadShader()
{
	ShaderMgr->LoadShader(_device);

	ComPtr<ID3DBlob> shaderBlob = nullptr;

	HR(D3DReadFileToBlob(L"../Resource/Shader/HLSL/CS_CalculateFrustum.cso", shaderBlob.ReleaseAndGetAddressOf()));
	if (shaderBlob != nullptr)
	{
		HR(_device->CreateComputeShader(
			shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(),
			nullptr,
			_csCalculateFrustum.ReleaseAndGetAddressOf()
		));
	}

	HR(D3DReadFileToBlob(L"../Resource/Shader/HLSL/CS_LightCulling.cso", shaderBlob.ReleaseAndGetAddressOf()));
	if (shaderBlob != nullptr)
	{
		HR(_device->CreateComputeShader(
			shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(),
			nullptr,
			_csLightCulling.ReleaseAndGetAddressOf()
		));
	}

	HR(D3DReadFileToBlob(L"../Resource/Shader/HLSL/CS_Occlusion.cso", shaderBlob.ReleaseAndGetAddressOf()));
	if (shaderBlob != nullptr)
	{
		HR(_device->CreateComputeShader(
			shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(),
			nullptr,
			_csOcclusion.ReleaseAndGetAddressOf()
		));
	}
}

void DirectX11Device::UnLoadShader()
{

}

void DirectX11Device::FrameAdvance()
{
	static std::shared_ptr<Entity>			skybox;
	static std::shared_ptr<Entity>			sector;

	static ComPtr<ID3D11ShaderResourceView>	_srvTexture;
	static ComPtr<ID3D11ShaderResourceView>	_srvCubeMap;

	static ComPtr<ID3D11DepthStencilState> depthStencilState;

//	static std::shared_ptr<LightList>	lightList = std::make_shared<LightList>();
//	static ComPtr <ID3D11Buffer>		lightListBuffer;

	static bool flag = true;
	if (flag)
	{
		flag = false;

		ScratchImage image;
		//std::wstring wFileName;
		//std::wstring wFileFormat;
		//GetFileNameFormat(pwTextureFile, wFileName, wFileFormat);
		//SetName(wFileName);

		//if (wFileFormat == L"dds") {
		//	HR(LoadFromDDSFile(pwTextureFile, DDS_FLAGS_NONE, nullptr, image));
		//}
		//else if (wFileFormat == L"tga") {
		//	HR(LoadFromTGAFile(pwTextureFile, nullptr, image));
		//}
		//else {
		//	LoadFromWICFile(pwTextureFile, WIC_FLAGS_NONE, nullptr, image);
		//}

		HR(LoadFromWICFile(_TEXT("../Resource/Fonts/aaa_0.png"), WIC_FLAGS_NONE, nullptr, image));
		HR(CreateShaderResourceView(_device.Get(), image.GetImages(), image.GetImageCount(), image.GetMetadata(), _srvTexture.ReleaseAndGetAddressOf()));

		HR(LoadFromDDSFile(_TEXT("../Resource/Textures/SkyBox/Interstelar.dds"), WIC_FLAGS_NONE, nullptr, image));
		HR(CreateShaderResourceView(_device.Get(), image.GetImages(), image.GetImageCount(), image.GetMetadata(), _srvCubeMap.ReleaseAndGetAddressOf()));

		D3D11_SAMPLER_DESC SamplerDesc;
		ZeroMemory(&SamplerDesc, sizeof(D3D11_SAMPLER_DESC));	//CLAMP
		SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		SamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		SamplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		SamplerDesc.MinLOD = 0;
		SamplerDesc.MaxLOD = 9;
		_device->CreateSamplerState(&SamplerDesc, _samplerState.ReleaseAndGetAddressOf());

		std::shared_ptr<Behavior> behavior;
		std::shared_ptr<MeshRenderer> meshRendererBehavior;
		std::shared_ptr<Mesh> mesh;

		//Camera
		auto entity = EntityMgr->NewEntity();
		entity->Move(0, 1, 0);
		behavior = entity->AddBehavior<InputController>();
		behavior = entity->AddBehavior<Camera>();

		//Skybox
		mesh = std::make_shared<SkyBoxMesh>();
		skybox = EntityMgr->NewEntity();
		skybox->AddBehavior<Skybox>();
		meshRendererBehavior = skybox->AddBehavior<MeshRenderer>(RenderLayer::Depth);
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"DepthPrePass_FarPlane");
		meshRendererBehavior->SetRenderStatePack(L"depth_pass");
		meshRendererBehavior = skybox->AddBehavior<SkyBoxRenderer>();
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"skybox");
		meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");

		//Cube
		mesh = std::make_shared<CubeMesh>(100, 0.01f, 100);
		entity = EntityMgr->NewEntity();
		entity->Move(0, 0, 0);
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>(RenderLayer::Depth);
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"DepthPrePass");
		meshRendererBehavior->SetRenderStatePack(L"depth_pass");
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>();
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"Integrated");
		meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");

		//Cube
		mesh = std::make_shared<CubeMesh>(10, 5, 0.3);
		entity = EntityMgr->NewEntity();
		entity->Move(0, 2.5f, -10);
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>(RenderLayer::Depth);
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"DepthPrePass");
		meshRendererBehavior->SetRenderStatePack(L"depth_pass");
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>();
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"Integrated");
		meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");

		auto f = [](float a, float b) { return(a + (float)(rand() / (float)RAND_MAX) * (b - a)); };

		//Cube
		mesh = std::make_shared<CubeMesh>(0.5, 0.5, 0.5);
		for (int i = 0; i < 30000; ++i) 
		{
			entity = EntityMgr->NewEntity();
		//	entity->Move(f(0, 50), f(1, 50), f(0, 50));
			entity->Move(f(-100, 100), f(1, 100), f(100, 150));
			entity->SetDirection(f(-1, 1), f(-1, 1), f(-1, 1));
			meshRendererBehavior = entity->AddBehavior<MeshRenderer>(RenderLayer::Depth);
			meshRendererBehavior->SetMesh(mesh);
			meshRendererBehavior->SetShaderPack(L"DepthPrePass");
			meshRendererBehavior->SetRenderStatePack(L"depth_pass");
			meshRendererBehavior = entity->AddBehavior<MeshRenderer>();
			meshRendererBehavior->SetMesh(mesh);
			meshRendererBehavior->SetShaderPack(L"Integrated");
			meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");
		}

		//Sector
		mesh = std::make_shared<SectorMesh>(128, 50, 128, 128, true);
		sector = EntityMgr->NewEntity();
		sector->Move(0, 0.01f, 0);
		meshRendererBehavior = sector->AddBehavior<MeshRenderer>(RenderLayer::Depth);
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"DepthPrePass");
		meshRendererBehavior->SetRenderStatePack(L"depth_pass");
		meshRendererBehavior = sector->AddBehavior<MeshRenderer>();
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"diffused");
		meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");

		D3D11_DEPTH_STENCIL_DESC DepthStencilDesc;
		InitMemory(DepthStencilDesc);
		DepthStencilDesc.DepthEnable = true;
		DepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		DepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
		_device->CreateDepthStencilState(&DepthStencilDesc, depthStencilState.ReleaseAndGetAddressOf());

		//model = std::make_shared<Entity>();
		//model->Move(0, 0, 20);
		//behavior = model->AddBehavior<MeshRenderer>();
		//auto mesh = std::make_shared<Mesh>();
		//mesh->LoadObjFile(_device.Get(), PathA("model") + "IronMan.obj", 1.0f);
		//behavior->SetMesh(mesh);

	//	behavior->AAA(10, 20, "HELLO");

		//auto entity = std::make_shared<Entity>();
		//static auto behavior2 = entity->AddBehavior<MeshRenderer>();
		//static auto behavior3 = entity->AddBehavior<MeshRenderer>();
		//behavior2->Release();
		//entity->Release();

		//Light1
		LightData lightData;
		lightData.type = DIR_LIGHT;
		lightData.positionWS = XMFLOAT3{ 0, 20, 0 };
		lightData.positionVS = XMFLOAT3{ 0, 0, 0 };
		lightData.directionWS = XMFLOAT3{ 1, -5, 3 };
		lightData.directionVS = XMFLOAT3{ 0, 0, 0 };
		lightData.color = XMFLOAT3{ 0.2f, 0.2f, 0.2f };
		lightData.enabled = true;
		lightData.intensity = 1;
		lightData.spotlightAngle;
		mesh = std::make_shared<CubeMesh>(3, 3, 0.3);
		entity = EntityMgr->NewEntity();
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>(RenderLayer::Depth);
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"DepthPrePass");
		meshRendererBehavior->SetRenderStatePack(L"depth_pass");
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>();
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"Integrated");
		meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");
		auto lightBehavior = entity->AddBehavior<Light>();
		lightBehavior->SetLightData(lightData);

		mesh = std::make_shared<CubeMesh>(0.3, 0.3, 0.3);
		for(int i=0 ; i<10 ; ++i){
			for (int j = 0; j < 9; ++j) {
				lightData.type = POINT_LIGHT;
			//	lightData.range = 2;
			//	lightData.color = XMFLOAT3{ 1, 0, 0 };
			//	lightData.positionWS = XMFLOAT3{ 0 + j*5.0f, 0, 5  + i * 5.0f };
			//	lightData.directionWS = XMFLOAT3{ 0, 0, 1 };
				lightData.range = f(10, 30);
				lightData.color = XMFLOAT3{ f(0.3, 1), f(0.3, 1), f(0.3, 1) };
				lightData.positionWS = XMFLOAT3(f(-100, 100), f(1, 100), f(100, 150));
			//	lightData.positionWS = XMFLOAT3(f(-10, 10), f(1, 1), f(10, 80));
	//			lightData.positionWS = XMFLOAT3{ f(-10, 10), f(1, 20), f(10, 80) };
				lightData.directionWS = XMFLOAT3{ 0, 0, 1 };
				entity = EntityMgr->NewEntity();
				meshRendererBehavior = entity->AddBehavior<MeshRenderer>(RenderLayer::Depth);
				meshRendererBehavior->SetMesh(mesh);
				meshRendererBehavior->SetShaderPack(L"DepthPrePass");
				meshRendererBehavior->SetRenderStatePack(L"depth_pass");
				meshRendererBehavior = entity->AddBehavior<MeshRenderer>();
				meshRendererBehavior->SetMesh(mesh);
				meshRendererBehavior->SetShaderPack(L"Integrated");
				meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");
				lightBehavior = entity->AddBehavior<Light>();
				lightBehavior->SetLightData(lightData);
			}
		}

		lightData.type = SPOT_LIGHT;
		lightData.spotlightAngle = 45;
		lightData.range = 3;
		lightData.color = XMFLOAT3{ 1, 1, 0 };
		lightData.positionWS = XMFLOAT3{ -10, 0.01, 10 };
		lightData.directionWS = XMFLOAT3{ 0, 0, 1 };
		mesh = std::make_shared<CubeMesh>(0.3, 0.3, 0.3);
		entity = EntityMgr->NewEntity();
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>(RenderLayer::Depth);
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"DepthPrePass");
		meshRendererBehavior->SetRenderStatePack(L"depth_pass");
		meshRendererBehavior = entity->AddBehavior<MeshRenderer>();
		meshRendererBehavior->SetMesh(mesh);
		meshRendererBehavior->SetShaderPack(L"Integrated");
		meshRendererBehavior->SetRenderStatePack(L"occlusion_pass");
		lightBehavior = entity->AddBehavior<Light>();
		lightBehavior->SetLightData(lightData);
		
		//Compute Light Cell Frustum Culling
		dispatchX = (UINT)ceil(static_cast<float>(_nScreenWidth) / 16);
		dispatchY = (UINT)ceil(static_cast<float>(_nScreenHeight) / 16);
		UpdateConstantBuffer(_immediateContext.Get(), _costbuffer1.Get(), EtcDataBuffer{ _nScreenWidth, _nScreenHeight, dispatchX, dispatchY, Max_Entity_Count });
		ID3D11UnorderedAccessView *UAV2[] = { uavHDR.Get(), uavFrustumBuffer.Get() };
		_immediateContext->CSSetSamplers(0, 1, _samplerState.GetAddressOf());
		_immediateContext->CSSetUnorderedAccessViews(0, _ARRAYSIZE(UAV2), UAV2, NULL);
		_immediateContext->CSSetShaderResources(0, _ARRAYSIZE(SRV_NULL), SRV_NULL);
		_immediateContext->CSSetConstantBuffers(ECBuffer::ETC, 1, _costbuffer1.GetAddressOf());
		_immediateContext->CSSetShader(_csCalculateFrustum.Get(), NULL, 0);
		_immediateContext->Dispatch(dispatchX, dispatchY, 1);
		_immediateContext->CSSetUnorderedAccessViews(0, _ARRAYSIZE(UAV_NULL), UAV_NULL, NULL);
		_immediateContext->CSSetShaderResources(0, _ARRAYSIZE(SRV_NULL), SRV_NULL);
		_immediateContext->CSSetConstantBuffers(0, _ARRAYSIZE(BUF_NULL), BUF_NULL);

	//	CUiObject* pUiObject = UiManager->CreateUi(L"System", UiElement::TITLE | UiElement::EXIT | UiElement::MINIMIZE | UiElement::RESIZE);
	////	CUiObject* pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Visible Object : ");
	////	pChild->SetSize(20, 20);
	//////	pChild->SetData(&CScene::m_nRenderedObject);
	//////	pChild->SetRenderFunc(RenderInt);
	////	pUiObject->AddChild(pChild);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Visible Model : ");
	////	pChild->SetSize(20, 20);
	//////	pChild->SetData(&CScene::m_nRenderedModel);
	//////	pChild->SetRenderFunc(RenderInt);
	////	pUiObject->AddChild(pChild);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Visible Homo : ");
	////	pChild->SetSize(20, 20);
	//////	pChild->SetData(&CScene::m_nHomoModel);
	//////	pChild->SetRenderFunc(RenderInt);
	////	pUiObject->AddChild(pChild);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Visible Point Light : ");
	////	pChild->SetSize(20, 20);
	//////	pChild->SetData(&CScene::m_nPointLight);
	//////	pChild->SetRenderFunc(RenderInt);
	////	pUiObject->AddChild(pChild);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Visible Spot Light : ");
	////	pChild->SetSize(20, 20);
	//////	pChild->SetData(&CScene::m_nSpotLight);
	//////	pChild->SetRenderFunc(RenderInt);
	////	pUiObject->AddChild(pChild);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Num of Renderer : ");
	////	pChild->SetSize(20, 20);
	//////	pChild->SetData(&CScene::m_nRenderer);
	//////	pChild->SetRenderFunc(RenderInt);
	////	pUiObject->AddChild(pChild);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Update Burden : ");
	////	pChild->SetSize(20, 20);
	//////	pChild->SetData(&g_nBurdon);
	//////	pChild->SetRenderFunc(RenderInt);
	////	pUiObject->AddChild(pChild);
	////	pChild = new CUiObject(UiCoordType::DOWN_WARD | UiCoordType::SYNC_X_SIZE);
	////	pChild->SetTitle(L"Developer's Blog");
	////	pChild->SetSize(20, 20);
	////	pChild->SetData(new std::wstring(L"http://blog.naver.com/xmfhvlex"));
	//////	pChild->SetMouseInputFunc(MouseInputWebPage);
	//////	pChild->SetRenderFunc(RenderElement);
	////	pUiObject->AddChild(pChild);
	//	pUiObject->SetPosition(500, 500);
	}

	EntityMgr->Update();
	BehaviorMgr->Update();

	//Init Frame Buffer
	_immediateContext->ClearRenderTargetView(rtvOcclusion.Get(), std::vector<float>{0, 0, 0}.data());
	_immediateContext->ClearRenderTargetView(rtvHDR.Get(), std::vector<float>{0.2, 0.4, 0.8}.data());
	_immediateContext->ClearDepthStencilView(_dsvDepthStencil.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);

	//Depth Pass Render
	_immediateContext->OMSetRenderTargets(1, rtvOcclusion.GetAddressOf(), _dsvDepthStencil.Get());
	BehaviorMgr->Render(RenderLayer::Depth);
	_immediateContext->OMSetRenderTargets(_ARRAYSIZE(RTV_NULL), RTV_NULL, nullptr);

	//Occlusion
	D3D11_MAPPED_SUBRESOURCE MappedResource;
	_immediateContext->Map(buffOcclusionFlag.Get(), 0, D3D11_MAP_READ, 0, &MappedResource);
	ZeroMemory(MappedResource.pData, Max_Entity_Count);
	_immediateContext->Unmap(buffOcclusionFlag.Get(), 0);

	ID3D11UnorderedAccessView *UAV1[] = { uavOcclusionFlag.Get() };
	ID3D11ShaderResourceView *SRV1[] = { srvOcclusion.Get() };
	UpdateConstantBuffer(_immediateContext.Get(), _costbuffer1.Get(), EtcDataBuffer{ _nScreenWidth, _nScreenHeight, dispatchX, dispatchY, Max_Entity_Count });
	_immediateContext->CSSetUnorderedAccessViews(0, _ARRAYSIZE(UAV1), UAV1, NULL);
	_immediateContext->CSSetShaderResources(0, _ARRAYSIZE(SRV1), SRV1);
	_immediateContext->CSSetConstantBuffers(ECBuffer::ETC, 1, _costbuffer1.GetAddressOf());
	_immediateContext->CSSetShader(_csOcclusion.Get(), NULL, 0);
	_immediateContext->Dispatch(dispatchX, dispatchY, 1);
	_immediateContext->CSSetUnorderedAccessViews(0, _ARRAYSIZE(UAV_NULL), UAV_NULL, NULL);
	_immediateContext->CSSetShaderResources(0, _ARRAYSIZE(SRV_NULL), SRV_NULL);

	_immediateContext->Map(buffOcclusionFlag.Get(), 0, D3D11_MAP_READ, 0, &MappedResource);
	_entityOcclusion = (UINT*)MappedResource.pData;
	_immediateContext->Unmap(buffOcclusionFlag.Get(), 0);

	//Light Culling
	LightMgr->SetLightToGPU();
	ID3D11UnorderedAccessView *UAV[] = { uavOLightIndexCounter.Get(), uavOLightIndexList.Get(), uavOLightGrid.Get(), uavTLightIndexCounter.Get(), uavTLightIndexList.Get(), uavTLightGrid.Get(), uavHDR.Get() };
	ID3D11ShaderResourceView *SRV[] = { _srvDepthStencil.Get(), srvFrustumBuffer.Get() };
	UpdateConstantBuffer(_immediateContext.Get(), _costbuffer1.Get(), EtcDataBuffer{ _nScreenWidth, _nScreenHeight, dispatchX, dispatchY, Max_Entity_Count });
	_immediateContext->CSSetSamplers(0, 1, _samplerState.GetAddressOf());
	_immediateContext->CSSetUnorderedAccessViews(0, _ARRAYSIZE(UAV), UAV, NULL);
	_immediateContext->CSSetShaderResources(0, _ARRAYSIZE(SRV), SRV);
	_immediateContext->CSSetConstantBuffers(ECBuffer::ETC, 1, _costbuffer1.GetAddressOf());
	_immediateContext->CSSetShader(_csLightCulling.Get(), NULL, 0);
	_immediateContext->Dispatch(dispatchX, dispatchY, 1);
	_immediateContext->CSSetUnorderedAccessViews(0, _ARRAYSIZE(UAV_NULL), UAV_NULL, NULL);
	_immediateContext->CSSetShaderResources(0, _ARRAYSIZE(SRV_NULL), SRV_NULL);

	ID3D11ShaderResourceView *SRV2[] = { _srvCubeMap.Get(), srvOLightIndexList.Get(), srvOLightGrid.Get() };
	_immediateContext->OMSetRenderTargets(1, rtvHDR.GetAddressOf(), _dsvDepthStencil.Get());
	_immediateContext->PSSetSamplers(0, 1, _samplerState.GetAddressOf());
	_immediateContext->PSSetShaderResources(0, _ARRAYSIZE(SRV2), SRV2);
	BehaviorMgr->Render(RenderLayer::Opaque);
	_immediateContext->PSSetConstantBuffers(0, _ARRAYSIZE(BUF_NULL), BUF_NULL);
	_immediateContext->OMSetRenderTargets(_ARRAYSIZE(RTV_NULL), RTV_NULL, nullptr);

	//Final Path
	_immediateContext->OMSetDepthStencilState(nullptr, 1);
	_immediateContext->RSSetState(nullptr);
	_immediateContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	_immediateContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_UNKNOWN, 0);
	_immediateContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	_immediateContext->PSSetSamplers(0, 1, _samplerState.GetAddressOf());
	_immediateContext->OMSetRenderTargets(1, _rtvBackBuffer.GetAddressOf(), nullptr);
	_immediateContext->PSSetShaderResources(0, 1, srvHDR.GetAddressOf());
	ShaderMgr->Activate(_immediateContext, L"final_path");
	_immediateContext->Draw(4, 0);
	_immediateContext->PSSetShaderResources(0, _ARRAYSIZE(SRV_NULL), SRV_NULL);
	_immediateContext->OMSetRenderTargets(_ARRAYSIZE(RTV_NULL), RTV_NULL, nullptr);

	//Debug Layer로 수정 필요
	////Debug Render
	//_immediateContext->OMSetRenderTargets(1, _rtvBackBuffer.GetAddressOf(), _dsvDepthStencil.Get());
	//sector->GetBehavior<Renderer>()->Render();

	UIFramework->Render();

	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));

	_dxgiSwapChain->Present(0, 0);
}

ComPtr<ID3D11Device> DirectX11Device::GetDevice() {
	return _device;
}

ComPtr<ID3D11DeviceContext> DirectX11Device::GetImmediate() {
	return _immediateContext;
}

#endif