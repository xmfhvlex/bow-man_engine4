#ifdef DIRECT_X_11_RENDERER

#pragma once

enum class ShaderType : BYTE {
	None = 0,
	VS = 1<<0,
	GS = 1<<1,
	HS = 1<<2,
	DS = 1<<3,
	PS = 1<<4,
	CS = 1<<5,
};

class IShader {
public:
	IShader(const ComPtr<ID3D11Device>& device, std::wstring fileName) : _name(fileName) {}
	~IShader() {}

	virtual void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext) = 0;
	void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext, int x, int y, int z) {}

	const std::wstring& GetName() {
		return _name;
	}

	const ShaderType& GetType() {
		return _shaderType;
	}

protected:
	std::wstring _name;
	ShaderType _shaderType = ShaderType::None;
};

class CVertexShader : public IShader {
public:
	CVertexShader(const ComPtr<ID3D11Device>& device, std::wstring fileName, json layout);
	~CVertexShader() {}

	virtual void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext) override
	{
		deviceContext->IASetInputLayout(_inputLayout.Get());
		deviceContext->VSSetShader(_shader.Get(), NULL, 0);
	}

	void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext, int x, int y, int z) = delete;

private:
	ComPtr<ID3D11InputLayout>	_inputLayout;
	ComPtr<ID3D11VertexShader>	_shader;
};

class CPixelShader : public IShader {
public:
	CPixelShader(const ComPtr<ID3D11Device>& device, std::wstring fileName);
	~CPixelShader() {}

	virtual void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext) override
	{
		deviceContext->PSSetShader(_shader.Get(), NULL, 0);
	}

	void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext, int x, int y, int z) = delete;

private:
	ComPtr<ID3D11PixelShader>	_shader;
};

class CComputeShader : public IShader {
public:
	CComputeShader(const ComPtr<ID3D11Device>& device, std::wstring fileName);
	~CComputeShader() {}

	virtual void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext) override
	{

	}

	void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext, int x, int y, int z)
	{
		deviceContext->CSSetShader(_shader.Get(), NULL, 0);
		deviceContext->Dispatch(x, y, z);
	}

private:
	ComPtr<ID3D11ComputeShader>	_shader;
};

class ShaderPack {
public:
	ShaderPack(const std::wstring& name) : _name(name) {}

	void AddShader(const std::shared_ptr<IShader>& shader) {
		for (const auto& pvtShader : _shaders) {
			if (pvtShader->GetType() == shader->GetType())
				return;
		}
		_shaderTypeElement |= static_cast<BYTE>(shader->GetType());
		_shaders.push_back(shader);
	}

	void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext) 
	{
		BYTE flag;

		flag = static_cast<BYTE>(ShaderType::VS);
		if(_prevShaderTypeElement & flag && (_shaderTypeElement & flag) ^ flag)
			deviceContext->VSSetShader(nullptr, NULL, 0);
		flag = static_cast<BYTE>(ShaderType::GS);
		if (_prevShaderTypeElement & flag && (_shaderTypeElement & flag) ^ flag)
			deviceContext->GSSetShader(nullptr, NULL, 0);
		flag = static_cast<BYTE>(ShaderType::HS);
		if (_prevShaderTypeElement & flag && (_shaderTypeElement & flag) ^ flag)
			deviceContext->HSSetShader(nullptr, NULL, 0);
		flag = static_cast<BYTE>(ShaderType::DS);
		if (_prevShaderTypeElement & flag && (_shaderTypeElement & flag) ^ flag)
			deviceContext->DSSetShader(nullptr, NULL, 0);
		flag = static_cast<BYTE>(ShaderType::PS);
		if (_prevShaderTypeElement & flag && (_shaderTypeElement & flag) ^ flag)
			deviceContext->PSSetShader(nullptr, NULL, 0);

		for (const auto& shader : _shaders) {
			shader->Activate(deviceContext);
		}

		_prevShaderTypeElement = _shaderTypeElement;
	}

private:
	inline static BYTE _prevShaderTypeElement;
	BYTE _shaderTypeElement = 0;
	std::wstring _name;
	std::vector<std::shared_ptr<IShader>> _shaders;
};


#endif