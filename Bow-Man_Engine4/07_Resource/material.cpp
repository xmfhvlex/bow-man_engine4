#include "stdafx.h"
#include "Material.h"

#ifdef DIRECT_X_11_RENDERER

#include "DirectX11Device.h"

#endif

Material::Material() 
{
	if (_materialBuffer.Get() == nullptr) {
		CreateConstantBuffers<MaterialParam>(Device.Get(), _materialBuffer.ReleaseAndGetAddressOf());
	}
}

Material::~Material()
{

}

void Material::SetMeterialToGPU(const ComPtr<ID3D11DeviceContext>& deviceContext)
{
	deviceContext->PSSetConstantBuffers(ECBuffer::MATERIAL, 1, _materialBuffer.GetAddressOf());
	deviceContext->PSSetShaderResources(0, _srvTexture.size(), _srvTexture.data()); 
}

//void Material::SetMeterialToGPU(const ComPtr<ID3D11DeviceContext>& deviceContext)
//{
//	deviceContext->PSSetConstantBuffers(ECBuffer::MATERIAL, 1, _materialBuffer.GetAddressOf());
//	deviceContext->PSSetShaderResources(0, _srvTexture.size(), _srvTexture.data());
//}
