#include "stdafx.h"
#include "CubeMesh.h"

#ifdef DIRECT_X_11_RENDERER

#include "DirectX11Device.h"
#include "ShaderManager.h"

CubeMesh::CubeMesh(float fWidth, float fHeight, float fDepth)
{
	float fx = fWidth * 0.5f, fy = fHeight * 0.5f, fz = fDepth * 0.5f;

	//BACK
	_positions.push_back(XMFLOAT3(+fx, +fy, -fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));
	_positions.push_back(XMFLOAT3(+fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));		_positions.push_back(XMFLOAT3(-fx, +fy, -fz));
	//RIGNT
	_positions.push_back(XMFLOAT3(+fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));
	_positions.push_back(XMFLOAT3(+fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));		_positions.push_back(XMFLOAT3(+fx, +fy, -fz));
	//FRONT
	_positions.push_back(XMFLOAT3(-fx, +fy, +fz));		_positions.push_back(XMFLOAT3(-fx, -fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));
	_positions.push_back(XMFLOAT3(-fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));		_positions.push_back(XMFLOAT3(+fx, +fy, +fz));
	//LEFT
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, +fz));
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, +fz));		_positions.push_back(XMFLOAT3(-fx, +fy, +fz));
	//TOP
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, +fy, +fz));
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(+fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, +fy, -fz));
	//BOTTOM
	_positions.push_back(XMFLOAT3(-fx, -fy, +fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));
	_positions.push_back(XMFLOAT3(-fx, -fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));

//	for (int i = 0; i < _positions.size(); i++)
//		m_vvColor.push_back(XMFLOAT4(rand() % 255 / 255.0f, rand() % 255 / 255.0f, rand() % 255 / 255.0f, 1)); // + RANDOM_COLOR;

	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(1, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));
	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 0));

	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(1, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));
	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 0));

	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(1, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));
	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 0));

	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(1, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));
	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 0));

	//m_vvTexCoord1.push_back(XMFLOAT2(0, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 0));			m_vvTexCoord1.push_back(XMFLOAT2(1, 0));
	//m_vvTexCoord1.push_back(XMFLOAT2(0, 1));			m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(1, 1));

	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(1, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));
	//m_vvTexCoord1.push_back(XMFLOAT2(1, 0));			m_vvTexCoord1.push_back(XMFLOAT2(0, 1));			m_vvTexCoord1.push_back(XMFLOAT2(0, 0));

	_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));
	_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));

	_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));
	_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));

	_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));
	_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));

	_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));
	_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));

	_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));
	_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));

	_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));
	_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));

	_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	AddVertexBuffer(Device.Get(), _positions);
	AddVertexBuffer(Device.Get(), _normals);
	AddIndexBuffer(Device.Get());
}

#endif