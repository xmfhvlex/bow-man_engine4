#ifdef DIRECT_X_11_RENDERER

#pragma once

#include "Mesh.h"

class CubeMesh : public Mesh
{
public:
	CubeMesh(float fWidth, float fHeight, float fDepth);
	~CubeMesh() {}

protected:
	std::vector<XMFLOAT3> _normals;

private:

};

#endif