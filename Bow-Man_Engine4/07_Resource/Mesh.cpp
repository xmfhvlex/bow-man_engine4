#include "stdafx.h"
#include "Mesh.h"

#ifdef DIRECT_X_11_RENDERER

#include "DirectX11Device.h"
#include "ShaderManager.h"

void Mesh::LoadObjFile(ID3D11Device * pDevice, const std::string& fileName, float fScale)
{
	float fMaxX, fMaxY, fMaxZ, fMinX, fMinY, fMinZ;
	fMaxX = fMaxY = fMaxZ = -FLT_MAX;
	fMinX = fMinY = fMinZ = +FLT_MAX;

	std::vector<XMFLOAT3>	vvTempPosition;
	std::vector<XMFLOAT3>	vvTempNormal;
	std::vector<XMFLOAT2>	vvTempTexCoord;
	std::vector<XMFLOAT4>	vvTempColor;

	bool bFlag = true;
	UINT vModelDataPivot = -1;
	UINT idxIndex = 0;
	char cIgnore;

	std::ifstream fRead(fileName);
	if (fRead.fail()) {
		return;
	}

	std::string sData;
	while (!fRead.eof()) {
		std::getline(fRead, sData);

		if (sData.substr(0, 2) == "v ") {
			bFlag = true;

			std::istringstream sVector(sData.substr(2));
			float x, y, z;
			sVector >> x >> y >> z;

			x *= -fScale;
			y *= fScale;
			z *= fScale;
			vvTempPosition.push_back(XMFLOAT3(x, y, z));
		}
		else if (sData.substr(0, 2) == "vt") {
			std::istringstream sTexCoord(sData.substr(2));
			float u, v;
			sTexCoord >> u >> v;
			vvTempTexCoord.push_back(XMFLOAT2(u, -v));
		}
		else if (sData.substr(0, 2) == "vn") {
			std::istringstream sNormal(sData.substr(2));
			float x, y, z;
			sNormal >> x >> y >> z;
			vvTempNormal.push_back(XMFLOAT3(x, y, z));
		}
		else if (sData.substr(0, 2) == "f ") {
			goto WARP;
			while (std::getline(fRead, sData)) {
				if (sData.substr(0, 2) == "f ") {
				WARP:
					std::vector<UINT> vPositionIndex;
					std::vector<UINT> vTexCoordIndex;
					std::vector<UINT> vNormalIndex;
					UINT v, t, n;

					std::istringstream sFace(sData.substr(2));
					while (sFace >> v >> cIgnore >> t >> cIgnore >> n) {
						vPositionIndex.push_back(--v);
						vTexCoordIndex.push_back(--t);
						vNormalIndex.push_back(--n);

						(fMaxX < vvTempPosition[v].x) ? fMaxX = vvTempPosition[v].x : NULL;
						(fMaxY < vvTempPosition[v].y) ? fMaxY = vvTempPosition[v].y : NULL;
						(fMaxZ < vvTempPosition[v].z) ? fMaxZ = vvTempPosition[v].z : NULL;
						(fMinX > vvTempPosition[v].x) ? fMinX = vvTempPosition[v].x : NULL;
						(fMinY > vvTempPosition[v].y) ? fMinY = vvTempPosition[v].y : NULL;
						(fMinZ > vvTempPosition[v].z) ? fMinZ = vvTempPosition[v].z : NULL;
					}

					_indexies.push_back(idxIndex++);
					_indexies.push_back(idxIndex++);
					_indexies.push_back(idxIndex++);
					_indexies.push_back(idxIndex++);
					_indexies.push_back(idxIndex++);

					_positions.push_back(vvTempPosition[vPositionIndex[1]]);
					_positions.push_back(vvTempPosition[vPositionIndex[0]]);
					_positions.push_back(vvTempPosition[vPositionIndex[2]]);

					_diffuseTexCoords.push_back(vvTempTexCoord[vTexCoordIndex[1]]);
					_diffuseTexCoords.push_back(vvTempTexCoord[vTexCoordIndex[0]]);
					_diffuseTexCoords.push_back(vvTempTexCoord[vTexCoordIndex[2]]);

					XMFLOAT3 vNormal;
					XMStoreFloat3(&vNormal, XMVector3Normalize(XMLoadFloat3(&vvTempNormal[vNormalIndex[1]])));
					vNormal.x *= -1;
					_normals.push_back(vNormal);
					XMStoreFloat3(&vNormal, XMVector3Normalize(XMLoadFloat3(&vvTempNormal[vNormalIndex[0]])));
					vNormal.x *= -1;
					_normals.push_back(vNormal);
					XMStoreFloat3(&vNormal, XMVector3Normalize(XMLoadFloat3(&vvTempNormal[vNormalIndex[2]])));
					vNormal.x *= -1;
					_normals.push_back(vNormal);

					if (vPositionIndex.size() == 4) {
						_indexies.push_back(idxIndex++);
						_indexies.push_back(idxIndex++);
						_indexies.push_back(idxIndex++);

						_positions.push_back(vvTempPosition[vPositionIndex[2]]);
						_positions.push_back(vvTempPosition[vPositionIndex[0]]);
						_positions.push_back(vvTempPosition[vPositionIndex[3]]);

						_diffuseTexCoords.push_back(vvTempTexCoord[vTexCoordIndex[2]]);
						_diffuseTexCoords.push_back(vvTempTexCoord[vTexCoordIndex[0]]);
						_diffuseTexCoords.push_back(vvTempTexCoord[vTexCoordIndex[3]]);

						XMStoreFloat3(&vNormal, XMVector3Normalize(XMLoadFloat3(&vvTempNormal[vNormalIndex[2]])));
						vNormal.x *= -1;
						_normals.push_back(vNormal);
						XMStoreFloat3(&vNormal, XMVector3Normalize(XMLoadFloat3(&vvTempNormal[vNormalIndex[0]])));
						vNormal.x *= -1;
						_normals.push_back(vNormal);
						XMStoreFloat3(&vNormal, XMVector3Normalize(XMLoadFloat3(&vvTempNormal[vNormalIndex[3]])));
						vNormal.x *= -1;
						_normals.push_back(vNormal);
					}
					//	else if (vPositionIndex.size() >= 5)
					//		MyMessageBox(L"정점이 5개 이상");
				}
				else if 
					(sData.substr(0, 2) == "s ");
				else
					break;
			}
			float fSizeX = abs((fMaxX - fMinX) / 2);
			float fSizeY = abs((fMaxY - fMinY) / 2);
			float fSizeZ = abs((fMaxZ - fMinZ) / 2);

			idxIndex = 0;
			fMaxX = fMaxY = fMaxZ = -FLT_MAX;
			fMinX = fMinY = fMinZ = +FLT_MAX;
		}
	}
	fRead.close();

	_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	AddVertexBuffer(Device.Get(), _positions);
	AddVertexBuffer(Device.Get(), _normals);
//	AddVertexBuffer(Device.Get(), _diffuseTexCoords);
	AddIndexBuffer(Device.Get());
}

void Mesh::Render()
{
	ImmediateContext->IASetPrimitiveTopology(_primitiveTopology);	
	ImmediateContext->IASetVertexBuffers(0, _vertexBuffers->_nBuffer, _vertexBuffers->_buffers.data(), _vertexBuffers->_nStrides.data(), _vertexBuffers->_nOffsets.data());

	if (_indexBuffer) {
		ImmediateContext->IASetIndexBuffer(_indexBuffer.Get(), _indexBufferFormat, _indexBufferOffset);
		ImmediateContext->DrawIndexed(_indexies.size(), 0, 0);
	}
	else
		ImmediateContext->Draw(_positions.size(), 0);
}

#endif