#ifdef DIRECT_X_11_RENDERER

#pragma once

enum EvertexData {
	POSITION	= 1 << 0,
	COLOR		= 1 << 1,
	NORMAL		= 1 << 2,
};

struct VertexBuffers {
	std::vector<ID3D11Buffer*>			_buffers;
	std::vector<UINT>					_nStrides;
	std::vector<UINT>					_nOffsets;
	UINT								_nBuffer = 0;
};

class Mesh {
public:
	Mesh() {}
	~Mesh() {}

	void LoadObjFile(ID3D11Device * pDevice, const std::string& fileName, float fScale);

	template<typename T>
	void CreateBuffer(ID3D11Device * pDevice, T & vData, UINT nBindFlags, D3D11_USAGE usage, UINT nCPUAccessFlags, ID3D11Buffer** buffer)
	{
		if (vData.size() == 0)
			return;

		D3D11_BUFFER_DESC BufferDesc;
		ZeroMemory(&BufferDesc, sizeof(D3D11_BUFFER_DESC));
		BufferDesc.Usage = usage;
		BufferDesc.ByteWidth = sizeof(vData.front()) * vData.size();
		BufferDesc.BindFlags = nBindFlags;
		BufferDesc.CPUAccessFlags = nCPUAccessFlags;
		D3D11_SUBRESOURCE_DATA BufferData;
		ZeroMemory(&BufferData, sizeof(D3D11_SUBRESOURCE_DATA));
		BufferData.pSysMem = vData.data();
		pDevice->CreateBuffer(&BufferDesc, &BufferData, buffer);
	}


	//void deleter(VertexBuffers* buffers) 
	//{
	//	std::cout << "hehe" << std::endl;
	//	for(auto buffer : buffers->_buffers)
	//		buffer->Release()
	//};

	template<typename T>
	void AddVertexBuffer(ID3D11Device * pDevice, T & vData, D3D11_USAGE usage = D3D11_USAGE_DEFAULT) {
		if (vData.size() == 0) return;
		if (_vertexBuffers == nullptr) {
			auto deleter = [](VertexBuffers* buffers) 
			{ 
				for (auto buffer : buffers->_buffers)
					buffer->Release();
			};
			std::shared_ptr<VertexBuffers> vertexBuffers(new VertexBuffers, deleter);
			_vertexBuffers = std::move(vertexBuffers);
		//	_vertexBuffers = std::make_shared<VertexBuffers>();
		}

		ID3D11Buffer* buffer;
		CreateBuffer(pDevice, vData, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE, 0, &buffer);
		UINT nStride = sizeof(vData.front());
		UINT nOffset = 0;
		_vertexBuffers->_buffers.push_back(std::move(buffer));
		_vertexBuffers->_nStrides.push_back(std::move(nStride));
		_vertexBuffers->_nOffsets.push_back(std::move(nOffset));
		_vertexBuffers->_nBuffer++;
	}

	void AddIndexBuffer(ID3D11Device * pDevice) {
		CreateBuffer(pDevice, _indexies, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE, 0, _indexBuffer.ReleaseAndGetAddressOf());
	}

	virtual void Render();

protected:
	D3D_PRIMITIVE_TOPOLOGY						_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	std::vector<XMFLOAT3>						_positions;
	std::vector<XMFLOAT3>						_normals;
	std::vector<XMFLOAT2>						_diffuseTexCoords;
	std::vector<UINT>							_indexies;
	
	std::shared_ptr<VertexBuffers>				_vertexBuffers;
	
	ComPtr<ID3D11Buffer>						_indexBuffer = nullptr;
	DXGI_FORMAT									_indexBufferFormat = DXGI_FORMAT_R32_UINT;
	UINT										_indexBufferOffset = 0;
};


#endif