#include "stdafx.h"
#include "IShader.h"

#ifdef DIRECT_X_11_RENDERER

#include "PathManager.h"


CVertexShader::CVertexShader(const ComPtr<ID3D11Device>& device, std::wstring fileName, json layout) : IShader(device, fileName) 
{
	_shaderType = ShaderType::VS;

	ComPtr<ID3DBlob> shaderBlob = nullptr;

	HR(D3DReadFileToBlob(fileName.c_str(), shaderBlob.ReleaseAndGetAddressOf()));

	if (shaderBlob == nullptr)
		return; 

	UINT nSlot = 0;
	std::vector<D3D11_INPUT_ELEMENT_DESC> vpInputElementDesc;
	std::map <std::string, UINT> semanticIdx;

/*
	LPCSTR SemanticName;
	UINT SemanticIndex;
	DXGI_FORMAT Format;
	UINT InputSlot;
	UINT AlignedByteOffset;
	D3D11_INPUT_CLASSIFICATION InputSlotClass;
	UINT InstanceDataStepRate;
*/
	for (const auto& layoutSemantic : layout.items()) {
		std::string semanticName = layoutSemantic.value();

		if ("F32_XYZ" == semanticName) {
			vpInputElementDesc.push_back({ "F32_XYZ", semanticIdx[semanticName]++, DXGI_FORMAT_R32G32B32_FLOAT, nSlot++, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		else if ("F32_XYZW" == semanticName) {
			vpInputElementDesc.push_back({ "F32_XYZW", semanticIdx[semanticName]++, DXGI_FORMAT_R32G32B32A32_FLOAT, nSlot++, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 });
		}
		else if ("INST_F32_4X4" == semanticName) {
			vpInputElementDesc.push_back({ "INST_F32_4X4", semanticIdx[semanticName],	DXGI_FORMAT_R32G32B32A32_FLOAT, nSlot,	 0, D3D11_INPUT_PER_INSTANCE_DATA, 0 });
			vpInputElementDesc.push_back({ "INST_F32_4X4", semanticIdx[semanticName],	DXGI_FORMAT_R32G32B32A32_FLOAT, nSlot,	 16, D3D11_INPUT_PER_INSTANCE_DATA, 0 });
			vpInputElementDesc.push_back({ "INST_F32_4X4", semanticIdx[semanticName],	DXGI_FORMAT_R32G32B32A32_FLOAT, nSlot,	 32, D3D11_INPUT_PER_INSTANCE_DATA, 0 });
			vpInputElementDesc.push_back({ "INST_F32_4X4", semanticIdx[semanticName]++, DXGI_FORMAT_R32G32B32A32_FLOAT, nSlot++, 48, D3D11_INPUT_PER_INSTANCE_DATA, 0 });
		}
	}

	if (vpInputElementDesc.size() > 0) {
		HR(device->CreateInputLayout(
			vpInputElementDesc.data(),
			vpInputElementDesc.size(),
			shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(),
			&_inputLayout
		));
	}

	HR(device->CreateVertexShader(
		shaderBlob->GetBufferPointer(),
		shaderBlob->GetBufferSize(),
		nullptr,
		_shader.ReleaseAndGetAddressOf()
	));
}

CPixelShader::CPixelShader(const ComPtr<ID3D11Device>& device, std::wstring fileName) : IShader(device, fileName) 
{
	_shaderType = ShaderType::PS;
	ComPtr<ID3DBlob> shaderBlob = nullptr;

	HR(D3DReadFileToBlob(fileName.c_str(), shaderBlob.ReleaseAndGetAddressOf()));

	if (shaderBlob != nullptr)
	{
		HR(device->CreatePixelShader(
			shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(),
			nullptr,
			_shader.ReleaseAndGetAddressOf()
		));
	}
}

CComputeShader::CComputeShader(const ComPtr<ID3D11Device>& device, std::wstring fileName) : IShader(device, fileName) 
{
	_shaderType = ShaderType::CS;
	ComPtr<ID3DBlob> shaderBlob = nullptr;

	HR(D3DReadFileToBlob(fileName.c_str(), shaderBlob.ReleaseAndGetAddressOf()));

	if (shaderBlob != nullptr)
	{
		HR(device->CreateComputeShader(
			shaderBlob->GetBufferPointer(),
			shaderBlob->GetBufferSize(),
			nullptr,
			_shader.ReleaseAndGetAddressOf()
		));
	}
}

#endif