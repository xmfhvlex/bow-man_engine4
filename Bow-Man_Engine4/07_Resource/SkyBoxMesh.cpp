#include "stdafx.h"
#include "SkyBoxMesh.h"

#ifdef DIRECT_X_11_RENDERER

#include "DirectX11Device.h"
#include "ShaderManager.h"

SkyBoxMesh::SkyBoxMesh()
{
	float fx = 0.5;
	float fy = 0.5;
	float fz = 0.5;
	//BACK
	_positions.push_back(XMFLOAT3(+fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));
	_positions.push_back(XMFLOAT3(+fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));
	//RIGNT
	_positions.push_back(XMFLOAT3(+fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));
	_positions.push_back(XMFLOAT3(+fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, +fy, -fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));
	//FRONT
	_positions.push_back(XMFLOAT3(-fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));		_positions.push_back(XMFLOAT3(-fx, -fy, +fz));
	_positions.push_back(XMFLOAT3(-fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, +fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));
	//LEFT
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, +fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(-fx, +fy, +fz));		_positions.push_back(XMFLOAT3(-fx, -fy, +fz));
	//TOP
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(+fx, +fy, +fz));		_positions.push_back(XMFLOAT3(-fx, +fy, +fz));
	_positions.push_back(XMFLOAT3(-fx, +fy, -fz));		_positions.push_back(XMFLOAT3(+fx, +fy, -fz));		_positions.push_back(XMFLOAT3(+fx, +fy, +fz));
	//BOTTOM
	_positions.push_back(XMFLOAT3(-fx, -fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));		_positions.push_back(XMFLOAT3(-fx, -fy, -fz));
	_positions.push_back(XMFLOAT3(-fx, -fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, +fz));		_positions.push_back(XMFLOAT3(+fx, -fy, -fz));

	//// Front Face
	//_positions.push_back(XMFLOAT3(-1.0f, -1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(-1.0f, 1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, 1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, -1.0f, -1.0f));

	//// Back Face            
	//_positions.push_back(XMFLOAT3(-1.0f, -1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, -1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, 1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(-1.0f, 1.0f, 1.0f));

	//// Top Face                
	//_positions.push_back(XMFLOAT3(-1.0f, 1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(-1.0f, 1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, 1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, 1.0f, -1.0f));

	//// Bottom Face            
	//_positions.push_back(XMFLOAT3(-1.0f, -1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, -1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, -1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(-1.0f, -1.0f, 1.0f));

	//// Left Face            
	//_positions.push_back(XMFLOAT3(-1.0f, -1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(-1.0f, 1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(-1.0f, 1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(-1.0f, -1.0f, -1.0f));

	//// Right Face            
	//_positions.push_back(XMFLOAT3(1.0f, -1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, 1.0f, -1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, 1.0f, 1.0f));
	//_positions.push_back(XMFLOAT3(1.0f, -1.0f, 1.0f));
	//
	//_indexies =
	//{
	//	// Front Face
	//	0, 1, 2,
	//	0, 2, 3,

	//	// Back Face
	//	4, 5, 6,
	//	4, 6, 7,

	//	// Top Face
	//	8, 9, 10,
	//	8, 10, 11,

	//	// Bottom Face
	//	12, 13, 14,
	//	12, 14, 15,

	//	// Left Face
	//	16, 17, 18,
	//	16, 18, 19,

	//	// Right Face
	//	20, 21, 22,
	//	20, 22, 23
	//};

	_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));
	_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));		_normals.push_back(XMFLOAT3(+0, +0, -1));

	_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));
	_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));		_normals.push_back(XMFLOAT3(+1, +0, +0));

	_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));
	_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));		_normals.push_back(XMFLOAT3(+0, +0, +1));

	_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));
	_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));		_normals.push_back(XMFLOAT3(-1, +0, +0));

	_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));
	_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));		_normals.push_back(XMFLOAT3(+0, +1, +0));

	_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));
	_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));		_normals.push_back(XMFLOAT3(+0, -1, +0));


//	_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
	_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	AddVertexBuffer(Device.Get(), _positions);
//	AddVertexBuffer(Device.Get(), _normals);
	AddIndexBuffer(Device.Get());
}

//void SkyBoxMesh::Render()
//{
//	ImmediateContext->IASetPrimitiveTopology(_primitiveTopology);
//
//	for (int i = 0; i< _vertexBuffers.size(); ++i) {
//		ImmediateContext->IASetVertexBuffers(i, 1, _vertexBuffers[i]->_buffer.GetAddressOf(), &_vertexBuffers[i]->_nStride, &_vertexBuffers[i]->_nOffset);
//	}
//	//	gImmediateContext->IASetVertexBuffers(0, _vertexBuffers.size(), &pVertexBuffer->_pVertexBuffer, &pVertexBuffer->_nStride, &pVertexBuffer->_nOffset);
//
//	if (_indexBuffer) {
//		ImmediateContext->IASetIndexBuffer(_indexBuffer.Get(), _indexBufferFormat, _indexBufferOffset);
//		ImmediateContext->DrawIndexed(_indexies.size(), 0, 0);
//	}
//	else
//		ImmediateContext->Draw(_positions.size(), 0);
//}


#endif