#include "stdafx.h"
#include "SectorMesh.h"

#ifdef DIRECT_X_11_RENDERER


#include "DirectX11Device.h"
#include "ShaderManager.h"

SectorMesh::SectorMesh(float fWidth, float fHeight, float fDepth, int nSectorCollumn, bool bFlat)
{
	XMCOLOR vColor(1, 1, 1, 1);

	int pointNum = nSectorCollumn + 1;
	float fx = fWidth * 0.5f, fy = fHeight * 0.5f, fz = fDepth * 0.5f;
	float leafLength = fWidth / nSectorCollumn;

	UINT nVertex = 0;
	int index = 0;

	switch (bFlat) {
	case true:
		nVertex = pointNum * 4;
		for (int i = 0; i < nVertex; ++i) 
			_positions.push_back(XMFLOAT3(0, 0, 0));

		for (int i = 0; i < pointNum; ++i) {
			_positions[index++] = XMFLOAT3(-fx, 0, leafLength*i - fz);
			_positions[index++] = XMFLOAT3(+fx, 0, leafLength*i - fz);

			_positions[index++] = XMFLOAT3(leafLength*i - fx, 0, -fz);
			_positions[index++] = XMFLOAT3(leafLength*i - fx, 0, +fz);
		}

		for (int i = 0; i < nVertex; ++i) 
			_indexies.push_back(i);
		break;
	case false:
		nVertex = pointNum * pointNum * 2;
		for (int i = 0; i < nVertex; ++i) 
			_positions.push_back(XMFLOAT3(0, 0, 0));

		for (int i = 0; i < pointNum; ++i) {
			for (int j = 0; j < pointNum; ++j) {

				if (i != pointNum - 1 && j != pointNum - 1) {
					_positions[(i*pointNum) + j] = XMFLOAT3(leafLength*j - fx, 0, leafLength*i - fz);
					_positions[(i*pointNum) + j + nVertex / 2] = XMFLOAT3(leafLength*j - fx, fy, leafLength*i - fz);
				}
				else if (j != pointNum - 1) {
					_positions[(i*pointNum) + j] = XMFLOAT3(leafLength*j - fx, 0, leafLength*i - fz - 4);
					_positions[(i*pointNum) + j + nVertex / 2] = XMFLOAT3(leafLength*j - fx, fy, leafLength*i - fz - 4);
				}
				else if (i != pointNum - 1) {
					_positions[(i*pointNum) + j] = XMFLOAT3(leafLength*j - fx - 4, 0, leafLength*i - fz);
					_positions[(i*pointNum) + j + nVertex / 2] = XMFLOAT3(leafLength*j - fx - 4, fy, leafLength*i - fz);
				}
				else {
					_positions[(i*pointNum) + j] = XMFLOAT3(leafLength*j - fx - 4, 0, leafLength*i - fz - 4);
					_positions[(i*pointNum) + j + nVertex / 2] = XMFLOAT3(leafLength*j - fx - 4, fy, leafLength*i - fz - 4);
				}
			}
		}

		for (int i = 0; i < nVertex / 2; ++i) {
			_indexies.push_back(i);									_indexies.push_back(i + nVertex / 2);

			if (i < pointNum) {
				_indexies.push_back(i);								_indexies.push_back(i + (pointNum*(pointNum - 1)));
				_indexies.push_back(i + (pointNum*pointNum));			_indexies.push_back(i + (pointNum*(pointNum - 1)) + (pointNum*pointNum));
			}

			if (i % pointNum == 0) {
				_indexies.push_back(i);								_indexies.push_back(i + (pointNum - 1));
				_indexies.push_back(i + (pointNum*pointNum));			_indexies.push_back(i + (pointNum*pointNum) + (pointNum - 1));
			}
		}
		break;
	}

	std::vector<XMFLOAT4> vvColor;
	for (int i = 0; i < _positions.size(); i++) {
		vvColor.push_back(XMFLOAT4(0.8, 0.8, 0.8, 1));
	}

	_primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_LINELIST;
	AddVertexBuffer(Device.Get(), _positions);
	AddVertexBuffer(Device.Get(), vvColor);
	AddIndexBuffer(Device.Get());
}

#endif