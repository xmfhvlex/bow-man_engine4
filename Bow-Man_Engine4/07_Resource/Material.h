#ifdef DIRECT_X_11_RENDERER

#pragma once

struct MaterialParam
{
	XMFLOAT3	_globalAmbient;
	float		_opacity = 1;
	//----------------------
	XMFLOAT3	_ambientColor;
	float		_indexOfRefraction = 0;
	//----------------------
	XMFLOAT3	_emissiveColor;
	bool		_hasAmbientTexture = false;
	//----------------------
	XMFLOAT3	_diffuseColor;
	bool		_hasEmissiveTexture = false;
	//----------------------
	XMFLOAT3	_specularColor;
	float		_specularIntencity;
	//----------------------
	XMFLOAT3	_reflectance;
	float		_alphaThreshold = 0;
	//----------------------
	bool		_hasDiffuseTexture = false;
	bool		_hasSpecularTexture = false;
	bool		_hasSpecularPowerTexture = false;
	bool		_hasNormalTexture = false;
	//----------------------
	bool		_hasBumpTexture = false;
	bool		_hasOpacityTexture = false;
	float		_bumpIntensity = 0;
	float		_specularScale = 1;
};

//16 Byte Memmory Packing
class Material 
{
public:
	Material();
	~Material();

	void SetMeterialToGPU(const ComPtr<ID3D11DeviceContext>& deviceContext);

private:
	std::vector<ID3D11ShaderResourceView*> _srvTexture;
	MaterialParam _param;
	inline static ComPtr <ID3D11Buffer>	_materialBuffer = nullptr;
};

#endif