#ifdef DIRECT_X_11_RENDERER

#pragma once

#include "Mesh.h"

class SectorMesh : public Mesh
{
public:
	SectorMesh(float fWidth, float fHeight, float fDepth, int nSectorCollumn, bool bFlat);
	~SectorMesh() {}

protected:

private:

};

#endif