#pragma once

template<class T>
void InitMemory(T & data) 
{
	::ZeroMemory(&data, sizeof(T));
}