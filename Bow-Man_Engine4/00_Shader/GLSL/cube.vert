#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (std140, binding = 0) uniform bufferVals 
{
	mat4 mvp;
} myBufferVals;

//layout (location = 0) in vec4 inPosition;
layout (location = 0) in vec2 inPosition;
layout (location = 1) in vec4 inColor;

layout (location = 0) out vec4 outColor;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main() {
//	gl_Position = inPosition;
//	gl_Position = myBufferVals.mvp * pos;

	gl_Position = vec4(inPosition, 0.0, 1.0);
	outColor = inColor;
}