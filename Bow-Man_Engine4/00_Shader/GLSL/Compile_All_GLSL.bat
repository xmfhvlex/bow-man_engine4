glslangValidator.exe -V triangle.vert 	-o ../../../Resource/Shader/GLSL/triangle_vert.spv -v -w
glslangValidator.exe -V triangle.frag 	-o ../../../Resource/Shader/GLSL/triangle_frag.spv -v -w
glslangValidator.exe -V cube.vert 		-o ../../../Resource/Shader/GLSL/cube_vert.spv     -v -w
glslangValidator.exe -V cube.frag 		-o ../../../Resource/Shader/GLSL/cube_frag.spv     -v -w
pause