#include "ConstBuffer.hlsli"

struct VS_IN
{
    float3 position : F32_XYZ0;
};

struct VS_OUT
{
    float4 position : SV_POSITION;
    float3 texCoord : TEXCCOORD;
};

VS_OUT main (VS_IN input)
{
    VS_OUT output = (VS_OUT)0;
    output.position = mul(float4(input.position, 1), gmtxWVP).xyww;
    output.texCoord = input.position;
    return output;
}
