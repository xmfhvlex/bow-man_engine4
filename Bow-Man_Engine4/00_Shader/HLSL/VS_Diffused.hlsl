#include "ConstBuffer.hlsli"

struct VS_IN
{
    float3 position : F32_XYZ0;
    float4 color : F32_XYZW0;
};

struct VS_OUT
{
    float4 position : SV_POSITION;
    float2 texCoord : TEXCCOORD;
    float4 color : COLOR;
};

//VS_OUTPUT main(VS_INPUT input) : SV_POSITION
//VS_OUTPUT main(VS_INPUT input, uint nVertexID : SV_VertexID)
VS_OUT main (VS_IN input, uint nVertexID : SV_VertexID)
{
    VS_OUT output = (VS_OUT)0;
    output.position = mul(float4(input.position, 1), gmtxWVP);
    output.color = input.color;
    return output;
}


