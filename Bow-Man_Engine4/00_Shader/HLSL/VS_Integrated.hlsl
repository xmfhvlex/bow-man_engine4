#include "ConstBuffer.hlsli"

struct VS_IN
{
    float3 position : F32_XYZ0;
    float3 normal : F32_XYZ1;
//    float2 texcoordBase : F32_XY0;
};

struct VS_OUT
{
    float4 position : SV_POSITION;
    float3 positionWS : POSITION;
    float3 normalWS : NORMAL;
//	float2 texCoordBase : TEXCOORD1;
};

//VS_OUTPUT main(VS_INPUT input, uint vtxID : SV_VertexID) : SV_POSITION
VS_OUT main (VS_IN input, uint vtxID : SV_VertexID)
{
    VS_OUT output = (VS_OUT)0;
    output.position = mul(float4(input.position, 1.0f), gmtxWVP);
    output.positionWS = mul(float4(input.position, 1.0f), gmtxWorld).xyz;
    output.normalWS = mul(input.normal, (float3x3) gmtxWorld);
    return output;
}