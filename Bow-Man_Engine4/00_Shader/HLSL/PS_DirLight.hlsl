#include "ConstBuffer.hlsli"
#include "Utility.hlsli"

SamplerState gTexSampler : register(s0);

Texture2D gTexAlbedo : register(t0);
Texture2D gTexDepth : register(t1);
Texture2D gTexNormal : register(t2);
TextureCube gTexCubeMap : register(t3);

struct DirLight
{
    float4 m_vUpperColor;
    float4 m_vDownColor;
    float4 m_vColorSpecular;
    float3 m_vDirection;
    float m_fPower;
};

float4 CalDeferredDirLight (int4 cpPos, uint uInstanceID)
{
    DirLight gArrDirLight[1];

    gArrDirLight[0].m_fPower;
    gArrDirLight[0].m_vUpperColor = float4(0.05, 0.05, 0.05, 1);
    gArrDirLight[0].m_vDownColor = float4(0.2, 0.2, 0.2, 1);
    gArrDirLight[0].m_vColorSpecular = float4(0.6, 0.6, 0.6, 1);
    gArrDirLight[0].m_vDirection = normalize(float3(1, -3, 2));
    gArrDirLight[0].m_fPower = 1;

    float depth = gTexDepth.Load(int3(cpPos.xy, 0)).x;
    if(depth == 1)
        discard;

    float3 albedo = gTexAlbedo.Load(int3(cpPos.xy, 0)).rgb;
    float3 worldPos = WorldPositionFromDepth(cpPos, depth);
    float3 worldNorm = gTexNormal.Load(int3(cpPos.xy, 0)).r;
    float specularPower = 3.0f;


    float3 vToCamera = normalize(gmtxCameraWorld._41_42_43 - worldPos);

//	float4 materialColor = gcMaterial.m_vDiffuseSpec;
    float4 materialColor = float4(1, 1, 1, 10);

    float3 vAmbientColor = gArrDirLight[uInstanceID].m_vColorSpecular.rgb;

	//Semi Globe Light
    float fFactor = ((worldNorm.y + 1) * 0.5);
    float3 vDiffuseColor = (((1 - fFactor) * gArrDirLight[uInstanceID].m_vUpperColor) + (fFactor * gArrDirLight[uInstanceID].m_vDownColor)).rgb;

	//Diffuse
    float fDotNL = dot(worldNorm, -gArrDirLight[uInstanceID].m_vDirection);
    fDotNL = saturate(fDotNL);
    float3 vDiffuse = gArrDirLight[uInstanceID].m_vColorSpecular.rgb * fDotNL;

	//Specular
    float3 vHalf1 = normalize(vToCamera + (-gArrDirLight[uInstanceID].m_vDirection));
    float3 vHalf2 = normalize(-gmtxCameraWorld._31_32_33 + (-gArrDirLight[uInstanceID].m_vDirection));
    float fDotNH = saturate(dot(worldNorm, vHalf1));
    float3 vSpecualarColor = gArrDirLight[uInstanceID].m_vColorSpecular.rgb * pow(fDotNH, materialColor.a) * gArrDirLight[uInstanceID].m_vColorSpecular.a * specularPower * fDotNL;

	//Attenuation Factor
    float3 vFactor = materialColor.rgb * gArrDirLight[uInstanceID].m_fPower;

	//Result
    return float4(((albedo * (vAmbientColor + vDiffuseColor)) + vSpecualarColor) * vFactor, 1);
}

struct VS_OUT
{
    float4 position : SV_Position;
    float2 texCoord : TEXCOORD0;
    uint uInstanceID : INSTANCE_ID;
};

//int a[2][10] = {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}};

static float3 color[] = {float3(0, 0, 0), float3(1, 0, 0), float3(0, 1, 0), float3(0, 0, 1), float3(1, 1, 0), float3(1, 0, 1)};

float4 main (VS_OUT input) : SV_TARGET
{
    float depth = gTexDepth.Load(int3(input.position.xy, 0)).r;
    if(depth == 1)
        discard;
 //   float3 worldPos = WorldPositionFromDepth(input.position, depth);

 //   float3 normal = gTexNormal.Load(int3(input.position.xy, 0));
	//float3 toTarget = worldPos - gmtxCameraWorld._41_42_43;
	//float3 result = reflect(toTarget, normal);
    //float3 result = refract(toTarget, normal, 0.9);

//	float4 color2 = gTexCubeMap.Sample(gTexSampler, result);
//	color2.a = 1;

//    float sonarThickness = 0.5;
//    float sonarRange = 5;
//    float3 sonarColor = float3(1, 1, 1);
//    float distance = length(gmtxCameraWorld._41_42_43 - worldPos);
	
//    float offset = abs(distance - sonarRange) <= sonarThickness ? abs(distance - sonarRange) : sonarThickness;q
//    float sonarFactor = lerp(0, 1, 1-(offset / sonarThickness));

////	float4 sonarResult = float4(sonarColor * sonarFactor, 0);
//	float4 sonarResult = float4(color[(int)(sonarFactor * (5))], 0);

	return CalDeferredDirLight(input.position, input.uInstanceID);
//	return lerp(CalDeferredDirLight(input.position, input.uInstanceID), color2, 0.5f) + sonarResult;
}