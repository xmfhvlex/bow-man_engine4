#pragma message( "BLOCK_SIZE undefined. Default to 16.")

#include "ConstBuffer.hlsli"
#include "Light.hlsli"
#include "Utility.hlsli"

struct ComputeShaderInput
{
    uint3 groupID : SV_GroupID; // 3D index of the thread group in the dispatch.
    uint3 groupThreadID : SV_GroupThreadID; // 3D index of local thread ID in a thread group.
    uint3 dispatchThreadID : SV_DispatchThreadID; // 3D index of global thread ID in the dispatch.
    uint  groupIndex : SV_GroupIndex; // Flattened local index of the thread within a thread group.
};

SamplerState gTexSampler : register(s0);
Texture2D gTexDepth : register(t0);

RWTexture2D<float4> grwHdrBuffer : register(u0);
RWStructuredBuffer<Frustum> out_Frustums : register(u1);

#define THREAD_NUM 16

[numthreads(THREAD_NUM, THREAD_NUM, 1)]
void main(ComputeShaderInput input)
{
//    const float3 eyePos = gmtxCameraWorld._41_42_43;
    const float3 eyePos = float3(0, 0, 0);

    float4 screenSpacePos[4];
	// Top left point
    screenSpacePos[0] = float4(input.dispatchThreadID.xy, 1.0f, 1.0f);
    // Top right point
    screenSpacePos[1] = float4(float2(input.dispatchThreadID.x + 1, input.dispatchThreadID.y) * THREAD_NUM, 1.0f, 1.0f);
    // Bottom left point
    screenSpacePos[2] = float4(float2(input.dispatchThreadID.x, input.dispatchThreadID.y + 1) * THREAD_NUM, 1.0f, 1.0f);
    // Bottom right point
    screenSpacePos[3] = float4(float2(input.dispatchThreadID.x + 1, input.dispatchThreadID.y + 1) * THREAD_NUM, 1.0f, 1.0f);
	
    float3 ViewSpacePos[4];
    for (int i = 0; i < 4; ++i)
    {
        ViewSpacePos[i] = ScreenToWorld(screenSpacePos[i]);
    }

    Frustum frustum;
// Left plane
    frustum.planes[0] = ComputePlane(eyePos, ViewSpacePos[2], ViewSpacePos[0]);
// Right plane
    frustum.planes[1] = ComputePlane(eyePos, ViewSpacePos[1], ViewSpacePos[3]);
// Top plane
    frustum.planes[2] = ComputePlane(eyePos, ViewSpacePos[0], ViewSpacePos[1]);
// Bottom plane
    frustum.planes[3] = ComputePlane(eyePos, ViewSpacePos[3], ViewSpacePos[2]);

    uint xNumThread = gDispatch.x;
    uint yNumThread = gDispatch.y;
// Store the computed frustum in global memory (if our thread ID is in bounds of the grid).
    if (input.dispatchThreadID.x < xNumThread && input.dispatchThreadID.y < yNumThread)
    {
        uint index = input.dispatchThreadID.x + (input.dispatchThreadID.y * xNumThread);
        out_Frustums[index] = frustum;
    }
}
