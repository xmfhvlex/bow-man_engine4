#include "ConstBuffer.hlsli"
#include "Utility.hlsli"

SamplerState gTexSampler : register(s0);
Texture2D gTexHDR : register(t0);

//Texture2D gTexAlbedo : register(t0);
//Texture2D gTexDepth : register(t1);
//Texture2D gTexNormal : register(t2);

//StructuredBuffer<float> gtexAverageLum : register(t1);
//Texture2D<float4>		gtexBloom : register(t2);

struct VS_OUT
{
    float4 position : SV_Position;
    float2 texCoord : TEXCOORD0;
};

[earlydepthstencil]
float4 main (VS_OUT input) : SV_Target
{
//    float3 vHdrColor = gTexHDR.Sample(gTexSampler, input.texCoord).rgb;
    float3 vHdrColor = gTexHDR.Load(int3(input.position.xy, 0)).rgb;
//	vHdrColor += gfBloomScale * gtexBloom.Sample(gSSWarp, input.texCoord).rgb;

    return float4(vHdrColor, 1.0f);

//	float fLScale = dot(vHdrColor, LUM_FACTOR.rgb);
//	fLScale *= gMiddleGrey / gtexAverageLum[0];
//	fLScale = (fLScale + (fLScale * fLScale) / gLumWhiteSqr) / (1.0 + fLScale);

//    float4 cFinalColor = float4(vHdrColor * fLScale, 1);
//	return cFinalColor;

//    return float4(worldSpacePos.xyz,1);
}