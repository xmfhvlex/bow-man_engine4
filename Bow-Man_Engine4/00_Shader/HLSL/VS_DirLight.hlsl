
static const float2 arrScreenPos[4] =
{
    float2(-1.0, 1.0),
	float2(1.0, 1.0),
	float2(-1.0, -1.0),
	float2(1.0, -1.0),
};

static const float2 arrScreenTex[4] =
{
    float2(0, 0),
	float2(1, 0),
	float2(0, 1),
	float2(1, 1),
};

struct VS_OUT
{
    float4 position : SV_Position;
    float2 texCoord : TEXCOORD0;
    uint uInstanceID : INSTANCE_ID;
};

VS_OUT main (uint vertexID : SV_VertexID, uint instanceID : SV_InstanceID)
{
    VS_OUT output;
    output.position = float4(arrScreenPos[vertexID], 0.0, 1.0);
    output.texCoord = arrScreenTex[vertexID];
    output.uInstanceID = instanceID;
    return output;
}