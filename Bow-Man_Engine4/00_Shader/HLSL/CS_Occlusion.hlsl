//#pragma message( "BLOCK_SIZE undefined. Default to 16.")

#include "ConstBuffer.hlsli"
#include "Light.hlsli"
#include "Utility.hlsli"

#define THREAD_NUM 16

struct ComputeShaderInput
{
    uint3 groupID : SV_GroupID; // 3D index of the thread group in the dispatch.
    uint3 groupThreadID : SV_GroupThreadID; // 3D index of local thread ID in a thread group.
    uint3 dispatchThreadID : SV_DispatchThreadID; // 3D index of global thread ID in the dispatch.
    uint groupIndex : SV_GroupIndex; // Flattened local index of the thread within a thread group.
};

Texture2D<uint> gTexEntityIndex : register(t0);
RWStructuredBuffer<uint> Occlusion : register(u0);

[numthreads(THREAD_NUM, THREAD_NUM, 1)]
void main(ComputeShaderInput input)
{
    int2 texCoord = input.dispatchThreadID.xy;
    uint entityIndex = gTexEntityIndex.Load(int3(texCoord, 0)).r;

    //No Need to be Atomic
	if (entityIndex != 0)
		Occlusion[entityIndex]++;
    
////	InterlockedCompareStore(Occlusion[entityIndex], 0, 1);

 //   if (entityIndex != 0 && Occlusion[entityIndex] == 0)
 //   {
	////	InterlockedCompareStore(Occlusion[entityIndex], 0, 1);
 //       uint index;
 //       InterlockedAdd(Occlusion[entityIndex], 1, index);
 //   }
}