#ifndef __LIGHT__
	#define __LIGHT__

#include "ConstBuffer.hlsli"

#define THREAD_NUM 16

#define DIR_LIGHT	0
#define POINT_LIGHT 1
#define SPOT_LIGHT	2

struct Surface
{
	float2	positionSS;
    float3	positionWS;
    float3	normalWS;
};

struct LightingResult
{
	float3 Ambient;
    float3 Diffuse;
    float3 Specular;
	float3 Emission;
};

LightingResult CalDirLight(Surface surface, Material material, Light light)
{
    LightingResult lightResult = (LightingResult) 0;

//Ambient
	lightResult.Ambient = light.color * light.intensity;

    float3 vToCamera = normalize(gmtxCameraWorld._41_42_43 - surface.positionWS);
	float3 vToLight = -light.directionWS;
    float fDotNL = saturate(dot(surface.normalWS, -light.directionWS));

//Diffuse
	float3 vDiffuseColor = light.color * fDotNL;
	lightResult.Diffuse = vDiffuseColor * light.intensity;

	if(fDotNL > 0.001f ){
//Specular
		float3 vHalf = normalize(vToCamera + vToLight);
		float fDotNH = saturate(dot(surface.normalWS, vHalf));
		float3 vSpecualarColor = light.color * pow(fDotNH, material.specularIntencity) * material.specularIntencity;
		lightResult.Specular = vSpecualarColor * light.intensity;
	}
    return lightResult;
}

LightingResult CalPointLight(Surface surface, Material material, Light light)
{
    LightingResult lightResult = (LightingResult) 0;
	float3 vToCamera = normalize(gmtxCameraWorld._41_42_43 - surface.positionWS);

    float3 vToLight = light.positionWS - surface.positionWS;
    float fDistToLight = length(vToLight);
	vToLight /= fDistToLight;

    float fDotNL = saturate(dot(surface.normalWS, vToLight));


	//LightingResult lightResult2 = (LightingResult) 0;

	//float dist = abs(length(surface.positionWS - light.positionWS));
	//if(dist > light.range)
	//	return lightResult2;

	//lightResult2.Diffuse = float3(1, 0, 0);
	//return lightResult2;

//Diffuse
    float3 vDiffuse = light.color * fDotNL;

    float fDistToLightNorm = 1.0 - saturate(fDistToLight * (1 / light.range));
    float attenuation = fDistToLightNorm * fDistToLightNorm;
    float factor = attenuation * light.intensity;

    lightResult.Diffuse = vDiffuse * factor;

	if (fDotNL > 0.001f){
//Specular
		float3 vReflect = normalize(reflect(-vToLight, surface.normalWS));
		float fDotRV = max(dot(vReflect, vToCamera), 0);
		float3 vSpecular = light.color * pow(fDotRV, material.specularIntencity) * material.specularIntencity;
		lightResult.Specular = vSpecular * factor;
	}
    return lightResult;
}

LightingResult CalSpotLight(Surface surface, Material material, Light light)
{
    LightingResult lightResult = (LightingResult) 0;

    float3 vToCamera = normalize(gmtxCameraWorld._41_42_43 - surface.positionWS);
    float3 vToLight = light.positionWS - surface.positionWS;
    float fDistToLight = length(vToLight);
    vToLight /= fDistToLight;

    float fDotNL = saturate(dot(surface.normalWS, vToLight));

    float minCos = cos(radians(light.spotlightAngle));
    float maxCos = lerp(minCos, 1, 0.8f);
    float cosAngle = dot(light.directionWS, -vToLight);
    float spotIntensity = smoothstep(minCos, maxCos, cosAngle);
 
    float fDistToLightNorm = 1.0 - saturate(fDistToLight * (1 / light.range));
    float attenuation = fDistToLightNorm ;
    float factor = spotIntensity * attenuation * light.intensity;
 
//Diffuse
    float3 vDiffuse = light.color * fDotNL;
    lightResult.Diffuse = vDiffuse * factor;

	if (fDotNL > 0.001f){
//Specular
		float3 vReflect = normalize(reflect(-vToLight, surface.normalWS));
		float fDotRV = max(dot(vReflect, vToCamera), 0);
		float3 vSpecular = light.color * pow(fDotRV, material.specularIntencity) * material.specularIntencity;
		lightResult.Specular = vSpecular * factor;
	}

    return lightResult;
}

LightingResult CalFowardLight(Surface surface, Material material)
{
    LightingResult finalLightResult = (LightingResult) 0;

    for (uint i = 0; i<gLightPivot; ++i)
    {
        LightingResult lightResult = (LightingResult) 0;
		
        switch (gLightList[i].type)
        {
		case DIR_LIGHT:
			lightResult = CalDirLight(surface, material, gLightList[i]);
               break;
		case POINT_LIGHT:
			lightResult = CalPointLight(surface, material, gLightList[i]);
            break;
		case SPOT_LIGHT:
            lightResult = CalSpotLight(surface, material, gLightList[i]);
            break;
        }

        finalLightResult.Ambient += lightResult.Ambient;
        finalLightResult.Diffuse += lightResult.Diffuse;
        finalLightResult.Specular += lightResult.Specular;
        finalLightResult.Emission += lightResult.Emission;
    }

    return finalLightResult;
}


StructuredBuffer<uint> LightIndexList : register( t1 );
Texture2D<uint2> LightGrid : register( t2 );

LightingResult CalFowardPlusLight(Surface surface, Material material)
{    
	uint2 tileIndex = uint2(floor(surface.positionSS / THREAD_NUM));
    uint startOffset = LightGrid[tileIndex].x;
    uint lightCount = LightGrid[tileIndex].y;

    LightingResult finalLightResult = CalDirLight(surface, material, gLightList[0]);

    for (uint i = 0; i<lightCount; ++i)
    {
	    uint lightIndex = LightIndexList[startOffset + i];
        Light light = gLightList[lightIndex];

        LightingResult lightResult = (LightingResult) 0;

        switch (light.type)
        {
		case DIR_LIGHT:
			lightResult = CalDirLight(surface, material, light);
               break;
		case POINT_LIGHT:
			lightResult = CalPointLight(surface, material, light);
            break;
		case SPOT_LIGHT:
            lightResult = CalSpotLight(surface, material, light);
            break;
        }

		finalLightResult.Ambient += lightResult.Ambient;
        finalLightResult.Diffuse += lightResult.Diffuse;
        finalLightResult.Specular += lightResult.Specular;
        finalLightResult.Emission += lightResult.Emission;
    }

    return finalLightResult;
}

#endif