#ifndef __CONSTBUFFER__
	#define __CONSTBUFFER__

#define MAX_LIGHT_COUNT 100
#define MAX_MATERIAL_COUNT 50

struct Light
{
	float3	positionWS;
	float	range;
	//----------------------
	float3	positionVS;
	float	spotlightAngle;
	//----------------------
	float3	directionWS;
	float	intensity;
	//----------------------
	float3	directionVS;
	uint	type;
	//----------------------
	float3	color;
	bool	enabled;
	//----------------------
};

struct Material
{
	float3		globalAmbient;
	float		opacity;
	//---------------------
	float3		ambientColor;
	float		indexOfRefraction;
	//---------------------
	float3		emissiveColor;
	bool		hasAmbientTexture;
	//---------------------
	float3		diffuseColor;
	bool		hasEmissiveTexture;
	//---------------------
	float3		specularColor;
	float		specularIntencity;
	//---------------------
	float3		reflectance;
	float		alphaThreshold;
	//---------------------
	bool		hasDiffuseTexture;
	bool		hasSpecularTexture;
	bool		hasSpecularPowerTexture;
	bool		hasNormalTexture;
	//---------------------
	bool		hasBumpTexture;
	bool		hasOpacityTexture;
	float		bumpIntensity;
	float		specularScale;
};

//--------------------------------------------------------------------------------------

cbuffer cbETC : register(b0)
{
    uint2	gvScreenSize : packoffset(c0.x);
    uint2	gDispatch : packoffset(c0.z);
    uint	gMaxEntityCount : packoffset(c1.x);
    uint3	padding3 : packoffset(c1.y);

//    uint gDomain : packoffset(c0.z);
//    uint gGroupSize : packoffset(c0.w);
	//-------------------------------------------
//    float gfAdaption : packoffset(c1.x);
//    float gfBloomThreshold : packoffset(c1.y);
};

//cbuffer cbHdr : register(b0)
//{
//	uint2 gRes : packoffset(c0.x);
//	uint2 gDispatch : packoffset(c0.z);
//}

cbuffer cbCamera : register(b1)
{
    matrix gmtxView : packoffset(c0);
    matrix gmtxProjection : packoffset(c4);
    matrix gmtxVP : packoffset(c8);
    matrix gmtxProjectionInverse : packoffset(c12);
    matrix gmtxViewInverse : packoffset(c16);
    matrix gmtxCameraWorld : packoffset(c20);
    float4 gvCameraSetting : packoffset(c24);
};

cbuffer cbWorldMatrix : register(b2)
{
    matrix	gmtxWVP : packoffset(c0);
    matrix	gmtxWorld : packoffset(c4);
	uint	gEntityIndex : packoffset(c8.x);
	int3	padding2 : packoffset(c8.y);
};

cbuffer cbLight : register(b3)
{
	Light gLightList[MAX_LIGHT_COUNT];
	uint gLightPivot;
	int3 padding1;
};

cbuffer cbMaterial : register(b4)
{
	Material gMaterial;
};

//cbuffer cbMaterial : register(b4)
//{
//    Material gArrMaterial[MAX_MATERIAL_COUNT];
//    int gMaterialPivot;
//};


#endif