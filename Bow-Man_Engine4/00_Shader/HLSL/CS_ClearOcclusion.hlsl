//#pragma message( "BLOCK_SIZE undefined. Default to 16.")

#include "ConstBuffer.hlsli"
#include "Light.hlsli"
#include "Utility.hlsli"

#define THREAD_NUM 16

struct ComputeShaderInput
{
    uint3 groupID : SV_GroupID; // 3D index of the thread group in the dispatch.
    uint3 groupThreadID : SV_GroupThreadID; // 3D index of local thread ID in a thread group.
    uint3 dispatchThreadID : SV_DispatchThreadID; // 3D index of global thread ID in the dispatch.
    uint groupIndex : SV_GroupIndex; // Flattened local index of the thread within a thread group.
};

RWStructuredBuffer<uint> Occlusion : register(u0);

[numthreads(THREAD_NUM, THREAD_NUM, 1)]
void main(ComputeShaderInput input)
{
	//엔티티 카운터 버퍼 초기화
    uint index = input.dispatchThreadID.x + (input.dispatchThreadID.y * gDispatch.x);
    if (index < gMaxEntityCount)
    {
        Occlusion[index] = 0;
    }
}