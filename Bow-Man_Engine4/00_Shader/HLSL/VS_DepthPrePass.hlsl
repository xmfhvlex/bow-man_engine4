#include "ConstBuffer.hlsli"

struct VS_IN
{
    float3 position : F32_XYZ0;
};

struct VS_OUT
{
    float4 position : SV_POSITION;
	uint entityIndex : ENTITY_INDEX;
};

//VS_OUTPUT main(VS_INPUT input, uint vtxID : SV_VertexID) : SV_POSITION
VS_OUT main (VS_IN input, uint vtxID : SV_VertexID)
{
    VS_OUT output = (VS_OUT)0;
    output.position = mul(float4(input.position, 1.0f), gmtxWVP);
	output.entityIndex = gEntityIndex;
    return output;
}