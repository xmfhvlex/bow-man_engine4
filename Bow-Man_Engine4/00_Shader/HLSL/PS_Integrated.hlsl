#include "Light.hlsli"

struct VS_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionWS : POSITION;
    float3 normalWS : NORMAL;
};


//StructuredBuffer<uint> LightIndexList : register( t1 );
//Texture2D<uint2> LightGrid : register( t2 );
//

SamplerState gTexSampler : register(s0);
TextureCube gTexCubeMap : register(t0);

[earlydepthstencil]
float4 main (VS_OUTPUT input) : SV_Target
{
 //   uint2 tileIndex = uint2( floor(input.position.xy / 16) );
 //   uint startOffset = LightGrid[tileIndex].x;
 //   uint lightCount = LightGrid[tileIndex].y;
	//return float4(lightCount, lightCount, lightCount, 1);

	//Reflect, Refract
	float3 normal = input.normalWS;
	float3 toTarget = input.positionWS - gmtxCameraWorld._41_42_43;
	float3 result = reflect(toTarget, normal);
    //float3 result = refract(toTarget, normal, 0.9);
	float3 color = gTexCubeMap.Sample(gTexSampler, result).rgb;

    Material material;
    material.specularIntencity = 1;

    Surface surface;
	surface.positionSS = input.position.xy;
    surface.positionWS = input.positionWS;
    surface.normalWS = normalize(input.normalWS);

    LightingResult lightResult = CalFowardPlusLight(surface, material);

    float3 finalColor;
    finalColor = (lightResult.Ambient + lightResult.Diffuse) * color + lightResult.Specular + lightResult.Emission;

	//uint2 tileIndex = uint2( floor(surface.positionSS / 16) );
 //   float lightDensity = LightGrid[tileIndex].y / 10.0f;
	//finalColor += float3(lightDensity, lightDensity, lightDensity);

    return float4(finalColor, 1);
}