SamplerState gTexSampler : register(s0);
TextureCube gTexCubeMap : register(t0);

struct VS_OUT
{
    float4 position : SV_POSITION;
    float3 texCoord : TEXCCOORD;
};

[earlydepthstencil]
float4 main(VS_OUT input) : SV_Target
{
	//if (input.position.z == 1.0f)
	//	return float4(1, 0, 0, 1);

    return gTexCubeMap.Sample(gTexSampler, input.texCoord);
}
