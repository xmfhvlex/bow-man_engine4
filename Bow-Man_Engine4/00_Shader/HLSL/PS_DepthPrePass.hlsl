#include "Light.hlsli"

struct VS_OUTPUT
{
    float4 position : SV_POSITION;
	uint entityIndex : ENTITY_INDEX;
};

[earlydepthstencil]
uint main(VS_OUTPUT input) : SV_TARGET
{
    return input.entityIndex;
}