SamplerState gTexSampler : register(s0);
Texture2D gtexAlbedo : register(t0);

struct VS_OUT
{
    float4 position : SV_POSITION;
    float2 texCoord : TEXCCOORD;
    float4 color : COLOR;
};

[earlydepthstencil]
float4 main (VS_OUT input) : SV_Target
{
//	return input.color - gtexAlbedo.Sample( gTexSampler, input.texCoord );
//	return Fog(input.color, input.positionW);
    return input.color;
}
