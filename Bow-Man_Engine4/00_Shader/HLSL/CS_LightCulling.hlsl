//#pragma message( "BLOCK_SIZE undefined. Default to 16.")

#include "ConstBuffer.hlsli"
#include "Light.hlsli"
#include "Utility.hlsli"

struct ComputeShaderInput
{
    uint3 groupID : SV_GroupID; // 3D index of the thread group in the dispatch.
    uint3 groupThreadID : SV_GroupThreadID; // 3D index of local thread ID in a thread group.
    uint3 dispatchThreadID : SV_DispatchThreadID; // 3D index of global thread ID in the dispatch.
    uint groupIndex : SV_GroupIndex; // Flattened local index of the thread within a thread group.
};

SamplerState gTexSampler : register(s0);
Texture2D gTexDepth : register(t0);
StructuredBuffer<Frustum> in_Frustums : register(t1);

// Opaque Light
RWStructuredBuffer<uint> o_LightIndexCounter : register(u0);
RWStructuredBuffer<uint> o_LightIndexList : register(u1);
RWTexture2D<uint2> o_LightGrid : register(u2);

// Transparent Light
RWStructuredBuffer<uint> t_LightIndexCounter : register(u3);
RWStructuredBuffer<uint> t_LightIndexList : register(u4);
RWTexture2D<uint2> t_LightGrid : register(u5);

RWTexture2D<float4> grwHdrBuffer : register(u6);

groupshared uint	uMinDepth;
groupshared uint	uMaxDepth;
groupshared Frustum GroupFrustum;

// Opaque geometry light lists.
groupshared uint	o_LightCount;
groupshared uint	o_LightIndexStartOffset;
groupshared uint	o_LightList[256];
 
// Transparent geometry light lists.
groupshared uint	t_LightCount;
groupshared uint	t_LightIndexStartOffset;
groupshared uint	t_LightList[256];

#define THREAD_NUM 16

// Add the light to the visible light list for opaque geometry.
void o_AppendLight( uint lightIndex )
{
	if (o_LightCount < 256)
    {
		uint index = 0; // Index into the visible lights array.
		InterlockedAdd(o_LightCount, 1, index);
		o_LightList[index] = lightIndex;
	}
}
 
// Add the light to the visible light list for transparent geometry.
void t_AppendLight( uint lightIndex )
{
	if (t_LightCount < 256)
    {
        uint index; // Index into the visible lights array.
        InterlockedAdd( t_LightCount, 1, index );
        t_LightList[index] = lightIndex;
    }
}

[numthreads(THREAD_NUM, THREAD_NUM, 1)]
void main(ComputeShaderInput input)
{
	if (input.dispatchThreadID.x == 0 && input.dispatchThreadID.y == 0 && input.dispatchThreadID.z == 0)
	{
		o_LightIndexCounter[0] = 0;
		t_LightIndexCounter[0] = 0;
	}

    if (input.groupIndex == 0) // Avoid contention by other threads in the group.
    {
        uMinDepth = 0xffffffff;
        uMaxDepth = 0;
        o_LightCount = 0;
        t_LightCount = 0;
		GroupFrustum = in_Frustums[input.groupID.x + (input.groupID.y * gDispatch.x)];
    }
	GroupMemoryBarrierWithGroupSync();

    int2 texCoord = input.dispatchThreadID.xy;
    float fDepth = gTexDepth.Load(int3(texCoord, 0)).r;
    uint uDepth = asuint(fDepth);
    InterlockedMin(uMinDepth, uDepth);
    InterlockedMax(uMaxDepth, uDepth);
    GroupMemoryBarrierWithGroupSync();

    float fMinDepth = asfloat(uMinDepth);
    float fMaxDepth = asfloat(uMaxDepth);

	// Convert depth values to view space.
    float minDepthVS = ClipToView(float4(0, 0, fMinDepth, 1)).z;
    float maxDepthVS = ClipToView(float4(0, 0, fMaxDepth, 1)).z;
    float nearClipVS = ClipToView(float4(0, 0, 0, 1)).z;
 
	// Clipping plane for minimum depth value 
	// (used for testing lights within the bounds of opaque geometry).
    Plane minPlane = { float3(0, 0, -1), -minDepthVS };

	for (uint i = input.groupIndex; i < gLightPivot; i += THREAD_NUM * THREAD_NUM)
    {
		Light light = gLightList[i];
        if (light.enabled == true)
        {
            switch (light.type)
            {
                case POINT_LIGHT:
				{
                    float3 positionVS = mul(float4(light.positionWS.xyz, 1), gmtxView).xyz;
                    Sphere sphere = { positionVS, light.range };
                    if (SphereInsideFrustum(sphere, GroupFrustum, nearClipVS, maxDepthVS))
                    {
						if ( SphereOutsidePlane( sphere, minPlane ) == false)
						{
							o_AppendLight(i);
						}

                        t_AppendLight(i);
                    }
				   break;
                }
				case SPOT_LIGHT:
				{
                    float3 positionVS = mul(float4(light.positionWS.xyz, 1), gmtxView).xyz;
					float3 directionVS = mul(float4(light.directionWS.xyz, 0), gmtxView).xyz;
					directionVS = normalize(directionVS);
					float radius = light.range * tan(light.spotlightAngle);
                    Cone cone = { positionVS, directionVS, light.range, radius };
                    if (ConeInsideFrustum(cone, GroupFrustum, nearClipVS, maxDepthVS))
                    {
						if ( ConeOutsidePlane( cone, minPlane ) == false)
						{
							o_AppendLight(i);
						}
						
                        t_AppendLight(i);
                    }
				    break;
                }
			}
        }
    }
    GroupMemoryBarrierWithGroupSync();

    if (input.groupIndex == 0)
    {
	// Update light grid for opaque geometry.
        InterlockedAdd(o_LightIndexCounter[0], o_LightCount, o_LightIndexStartOffset);
        o_LightGrid[input.groupID.xy] = uint2(o_LightIndexStartOffset, o_LightCount);
 
	// Update light grid for transparent geometry.
        InterlockedAdd(t_LightIndexCounter[0], t_LightCount, t_LightIndexStartOffset);
        t_LightGrid[input.groupID.xy] = uint2(t_LightIndexStartOffset, t_LightCount);
    }
    GroupMemoryBarrierWithGroupSync();

	int addOffset = THREAD_NUM * THREAD_NUM;
	// For opaque geometry.
	for (i = input.groupIndex; i < o_LightCount; i += addOffset)
    {
        o_LightIndexList[o_LightIndexStartOffset + i] = o_LightList[i];
    }
	// For transparent geometry.
	for (i = input.groupIndex; i < t_LightCount; i += addOffset)
    {
        t_LightIndexList[t_LightIndexStartOffset + i] = t_LightList[i];
    }

 //   GroupMemoryBarrierWithGroupSync();
	//float a = (float)input.groupIndex / (16*16);
	//grwHdrBuffer[input.dispatchThreadID.xy] = float4(a, a, a, 1); 
}