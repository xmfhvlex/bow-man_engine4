#ifndef __UTILITY__
	#define __UTILITY__

#include "ConstBuffer.hlsli"

float3 WorldPositionFromDepth (int4 cpPos, float depth)
{
    //if(depth == 1.0f)
    //    return float4(0,0,0,1);

 //   float zNear = gvCameraSetting.z;
 //   float zFar = gvCameraSetting.w;
 //   depth = -(zFar * zNear) / (depth * (zFar - zNear) - zFar);
	//// [0 ~ x, 0 ~ y] to [-1 ~ +1, -1 ~ +1]
 //   float2 screenSpace = (float2(input.position.xy) / float2(gvCameraSetting.xy)) * 2.0f - 1.0f;
 //   float3 rayDirection = normalize((gmtxCameraWorld._11_12_13 * screenSpace.x) - (gmtxCameraWorld._21_22_23 * screenSpace.y) + (gmtxCameraWorld._31_32_33));
 //   float distance = depth / dot(gmtxCameraWorld._31_32_33,rayDirection);
 //   float3 position = gmtxCameraWorld._41_42_43 + (rayDirection * distance);
 //   position = float4(ceil(position.x),ceil(position.y),ceil(position.z),1);
 //   return float4(position,1);

    float x = (cpPos.x / gvCameraSetting.x) * 2 - 1;
    float y = (1 - (cpPos.y / gvCameraSetting.y)) * 2 - 1;
    float z = depth;
    float4 worldSpacePos = float4(x, y, z, 1.0);
    worldSpacePos = mul(worldSpacePos, gmtxProjectionInverse);
    worldSpacePos /= worldSpacePos.w;
    worldSpacePos = mul(worldSpacePos, gmtxViewInverse);
//	worldSpacePos = float4(ceil(worldSpacePos.x),ceil(worldSpacePos.y),ceil(worldSpacePos.z),1);
    return worldSpacePos.xyz;
}

float4 ClipToView(float4 clip)
{
    // View space position.
    float4 view = mul(clip, gmtxProjectionInverse);
    // Perspective projection.
    view = view / view.w;
    return view;
}

float3 ViewToWorld(float4 view)
{
    return mul(view, gmtxViewInverse).xyz;
}

float4 ScreenToView( float4 screen )
{
    // Convert to normalized texture coordinates
    float2 texCoord = screen.xy / gvScreenSize;
 
    // Convert to clip space
    float4 clip = float4( float2( texCoord.x, 1.0f - texCoord.y ) * 2.0f - 1.0f, screen.z, screen.w );
 
    return ClipToView( clip );
}

float3 ScreenToWorld(float4 screen)
{
// Convert to normalized texture coordinates
    float2 texCoord = screen.xy / gvScreenSize;
    float4 clip = float4(float2(texCoord.x, 1.0f - texCoord.y) * 2.0f - 1.0f, screen.z, screen.w);
    float4 viewPos = ClipToView(clip);
    return ViewToWorld(viewPos);
}

//--------------------------------------------------------------------------------------------------------- Frustum
struct Plane
{
    float3 normal;
    float distance;
};

struct Frustum
{
    Plane planes[4];
};

Plane ComputePlane(float3 p0, float3 p1, float3 p2)
{
    Plane plane;
    float3 v0 = p1 - p0;
    float3 v1 = p2 - p0 ;
    plane.normal = normalize(cross(v0, v1));
    plane.distance = dot(plane.normal, p0);

    return plane;
}

//---------------------------------------------------------------------------------------------------------- Collision
struct Sphere
{
    float3 position;
    float radius;
};

bool SphereOutsidePlane(Sphere sphere, Plane plane)
{
	return dot(plane.normal, sphere.position) - sphere.radius > plane.distance;
}

bool SphereInsideFrustum(Sphere sphere, Frustum frustum, float zNear, float zFar)
{   
	float center = (zFar + zNear) * 0.5;
	float range = (zFar - zNear) * 0.5 + sphere.radius;
	float distance = abs(center - sphere.position.z);
    
	//Frustum의 근평면, 원평면 사이에 있는지 검사 (사이에 없다면 false 반환)
	if (range < distance)
		return false;
 
    for (int i = 0 ; i < 4 ; ++i)
    {
        if (SphereOutsidePlane(sphere, frustum.planes[i]) == true)
        {
			return false;
		}
    }
	return true;
}

struct Cone
{
    float3 tip;
    float3 direction;
    float height;
    float radius;
};

bool PointOutsidePlane(float3 pos, Plane plane)
{
    return dot(plane.normal, pos) > plane.distance;
}

bool ConeOutsidePlane(Cone cone, Plane plane)
{
    //Direction에 직교하면서 평면방향인 벡터 계산
    float3 m = cross( cross( plane.normal, cone.direction ), cone.direction );
	m = normalize(m);

	//Cone의 원에 놓인 점들 중 평면에 가장 가까운 점 계산.
    float3 Q = cone.tip + (cone.direction * cone.height) + (m * cone.radius);

    return PointOutsidePlane( cone.tip, plane ) && PointOutsidePlane( Q, plane );
}

bool ConeInsideFrustum( Cone cone, Frustum frustum, float zNear, float zFar )
{
    bool result = true;

    Plane nearPlane = { float3( 0, 0, -1 ), -zNear };
    Plane farPlane = { float3( 0, 0, 1 ), zFar };

    if ( ConeOutsidePlane( cone, nearPlane ) || ConeOutsidePlane( cone, farPlane ) )
    {
        result = false;
    }
 
    // Then check frustum planes
    for ( int i = 0 ; i < 4 && result==true ; i++ )
    {
        if ( ConeOutsidePlane( cone, frustum.planes[i] ) == true )
        {
            result = false;
        }
    }
 
    return result;
}

#endif