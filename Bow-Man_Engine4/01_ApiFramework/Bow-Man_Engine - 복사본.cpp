// Bow-Man_Engine4.cpp: 응용 프로그램의 진입점을 정의합니다.
//

#include "stdafx.h"
#include "Bow-Man_Engine.h"
#include "Window.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	//wcout 한글 출력
	std::wcout.imbue(std::locale("kor"));

	//Enable Drag Drop in Window
	//DragAcceptFiles(gHwnd, TRUE);

	srand(time(NULL));

	CWindow win;
	win.Open(hInstance, nCmdShow);
	win.Run();
	win.Close();
}

#if defined(NDEBUG) && defined(__GNUC__)
#define U_ASSERT_ONLY __attribute__((unused))
#else
#define U_ASSERT_ONLY
#endif


//LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
//{
//	switch (message)
//	{
//	case WM_CREATE:
//
//		break;
//	case WM_PAINT:
//	{
//		PAINTSTRUCT ps;
//		HDC hdc = BeginPaint(hWnd, &ps);
//		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다.
//		EndPaint(hWnd, &ps);
//	}
//	break;
//	default:
//		return DefWindowProc(hWnd, message, wParam, lParam);
//	}
//	return 0;
//}