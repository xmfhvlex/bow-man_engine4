#include "stdafx.h"
#include "Window.h"

#ifdef DIRECT_X_11_RENDERER

#include "DirectX11Device.h"
#include "InputManager.h"
#include "Entity.h"
#include "EntityManager.h"
#include "BehaviorManager.h"
#endif

#include "IDevice.h"
#include "VulkanDevice.h"

bool Window::Open() {
	LoadStringW(gHinstance, IDS_APP_TITLE, _szTitle, MAX_LOAD_STRING);
	LoadStringW(gHinstance, IDC_BOWMANENGINE4, _szWindowClass, MAX_LOAD_STRING);

	RegisterMyClass();
	MakeWindow();

	ShowWindow(gHwnd, SW_SHOWMAXIMIZED);
	UpdateWindow(gHwnd);
	return true;
}

bool Window::RegisterMyClass() {
	WNDCLASSEXW wcex;
	UINT retVal;
	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = gHinstance;
	wcex.hIcon = LoadIcon(gHinstance, MAKEINTRESOURCE(IDI_BOWMANENGINE4));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = CreateSolidBrush(RGB(80, 80, 80));
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_BOWMANENGINE4);
	wcex.lpszClassName = _szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	retVal = RegisterClassEx(&wcex);
	return retVal;

	//		wcex.lpfnWndProc = ChildWndProc;
	//		wcex.hbrBackground = CreateSolidBrush(RGB(100, 80, 80));
	//		wcex.lpszClassName = _T("Go");
	//		retVal = RegisterClassEx(&wcex);
	//		if (retVal == 0) return 0;
}

bool Window::MakeWindow() 
{
	LONG FRAME_BUFFER_WIDTH = GetSystemMetrics(SM_CXMAXIMIZED);
	LONG FRAME_BUFFER_HEIGHT = GetSystemMetrics(SM_CYMAXIMIZED);
	LONG FRAME_BUFFER_POS_X = 0;
	LONG FRAME_BUFFER_POS_Y = 0;

	RECT rect = { FRAME_BUFFER_POS_X, FRAME_BUFFER_POS_Y, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT };
	//_dwStyle = WS_POPUP;
	_dwStyle = WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME;

	gHwnd = CreateWindowEx(_dwExStyle, _szWindowClass, _szTitle, _dwStyle, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, nullptr, nullptr, gHinstance, nullptr);
	if (!gHwnd) 
		return false;
	if (!_bShowMenue) 
		SetMenu(gHwnd, NULL);
	SetWindowLongPtr(gHwnd, GWLP_USERDATA, (LONG_PTR)this);

	RECT rcClient;
	GetClientRect(gHwnd, &rcClient);
	screenWidth = rcClient.right - rcClient.left;
	screenHeight = rcClient.bottom - rcClient.top;
	std::cout << "GetClientRect : " << screenWidth << " - " << screenHeight << std::endl;

//	ShowWindow(gHwnd, SW_SHOW);
//	ShowWindow(gHwnd, SW_HIDE);

	return true;
}

void Window::Close() 
{
	GraphicsAPI->DestroyRenderDevice();

	BehaviorMgr->Destroy();
	EntityMgr->Destroy();
}

void Window::ResizeWindow()
{
	RECT rcClient;
	GetClientRect(gHwnd, &rcClient);
	screenWidth = rcClient.right - rcClient.left;
	screenHeight = rcClient.bottom - rcClient.top;
	std::cout << "ResizeWindow : " << screenWidth << " - " << screenHeight << std::endl;

	POINT pos{ 0, 0 };
	ScreenToClient(gHwnd, &pos);
	screenPosX = pos.x;
	screenPosY = pos.y;

	GraphicsAPI->ResizeWindow();
}

void Window::Run() 
{
	HACCEL hAccelTable = LoadAccelerators(gHinstance, MAKEINTRESOURCE(IDC_BOWMANENGINE4));

	TimeMgr->Initialize();

#ifdef DIRECT_X_11_RENDERER
	InputMgr->Initialize(gHwnd);
#else

#endif

	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&gPmc, sizeof(gPmc));

	MSG msg;
	while (_isRunnig){
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			MEMORYSTATUSEX memInfo;
			memInfo.dwLength = sizeof(MEMORYSTATUSEX);
			GlobalMemoryStatusEx(&memInfo);
			DWORDLONG totalVirtualMem = memInfo.ullTotalPageFile;
			DWORDLONG virtualMemUsed = memInfo.ullTotalPageFile - memInfo.ullAvailPageFile;
			DWORDLONG totalPhysMem = memInfo.ullTotalPhys;
			DWORDLONG physMemUsed = memInfo.ullTotalPhys - memInfo.ullAvailPhys;
		
		
			PROCESS_MEMORY_COUNTERS_EX pmc;
			GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
			
			

		
		//	printf("Memory Usage : %u MB, %u, %u MB, %d MB\n", pmc.WorkingSetSize>> 20, pmc.PageFaultCount - gMc.PageFaultCount, physMemUsed >> 20, ((gMemStatus.ullTotalPhys - gMemStatus.ullAvailPhys) - physMemUsed) >> 20);
		




			TimeMgr->Update();

#ifdef DIRECT_X_11_RENDERER
			InputMgr->Update(0);
#endif
			GraphicsAPI->FrameAdvance();

			auto title = std::wstring(_szTitle) + L" [" + std::to_wstring(TimeMgr->GetAvgDeltaTime()) + L" | " + std::to_wstring(static_cast<int>(1/TimeMgr->GetAvgDeltaTime())) + L"FPS]";
			::SetWindowText(gHwnd, title.c_str());
		}
	}

}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	Window * pWindow = reinterpret_cast<Window*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));

	switch (message)
	{
	case WM_CREATE:
		break;
	case WM_EXITSIZEMOVE: {
	//	pWindow->ResizeWindow();
		break;
	}
	case WM_SIZE:
		switch (wParam) {
		case SIZE_MAXIMIZED: case SIZE_MINIMIZED:
			pWindow->ResizeWindow();
			break;
		}
		break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 메뉴 선택을 구문 분석합니다.
		switch (wmId)
		{
		case IDM_ABOUT:
			//              DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			pWindow->SetIsRunning(false);
			//	DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다.
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_MOUSEHOVER:
		//	SetCapture(hWnd);
		//	SetCursor(NULL);
		break;
	case WM_MOUSELEAVE:
		//	ReleaseCapture();
		//	SetCursor(LoadCursor(0, MAKEINTRESOURCE(IDC_CURSOR2)));
		break;
	case WM_MOUSEMOVE:
		TRACKMOUSEEVENT hoverEvent;
		hoverEvent.cbSize = sizeof(hoverEvent);
		hoverEvent.dwFlags = TME_HOVER;
		hoverEvent.hwndTrack = hWnd;
		hoverEvent.dwHoverTime = 1000;
		TrackMouseEvent(&hoverEvent);

		TRACKMOUSEEVENT leaveEvent;
		leaveEvent.cbSize = sizeof(leaveEvent);
		leaveEvent.dwFlags = TME_LEAVE;
		leaveEvent.hwndTrack = hWnd;
		TrackMouseEvent(&leaveEvent);
	case WM_LBUTTONDOWN:	case WM_LBUTTONUP:	case WM_RBUTTONDOWN:	case WM_RBUTTONUP:
	case WM_MBUTTONDOWN:	case WM_MBUTTONUP:	case WM_RBUTTONDBLCLK: case WM_LBUTTONDBLCLK: case WM_MBUTTONDBLCLK:
	case WM_MOUSEWHEEL:		// case WM_MYMSG:
	//	gFramework.MouseInputProc(hWnd, message, wParam, lParam);
#ifdef DIRECT_X_11_RENDERER
		InputMgr->MouseInputProc(hWnd, message, wParam, lParam);
#endif
		break;
	case WM_CHAR:	case WM_IME_CHAR:	case WM_IME_SETCONTEXT:	case WM_IME_STARTCOMPOSITION:
	case WM_IME_ENDCOMPOSITION: 	case WM_IME_CONTROL:	case WM_IME_COMPOSITION:	case WM_IME_NOTIFY:
	case WM_KEYDOWN: case WM_KEYUP:
		switch (message) {
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				pWindow->SetIsRunning(false);
				break;
			}
		}
		//	gFramework.KeyboardInputProc(hWnd, message, wParam, lParam);
		break;
	case WM_DESTROY:
		pWindow->SetIsRunning(false);
		exit(-1);
		//	PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


//#if defined(VULKAN_RENDERER)
////	_renderDevice = std::make_shared<VulkanDevice>();
//#elif defined(DIRECT_X_11_RENDERER)
////	_renderDevice = std::make_shared<DirectX11Device>();
//#endif	
