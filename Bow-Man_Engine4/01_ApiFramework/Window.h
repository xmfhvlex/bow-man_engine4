#pragma once

constexpr int MAX_LOAD_STRING = 100;

class Window {
public:
	Window() {}
	virtual ~Window() {}

	bool Open();
	void Close();
	void ResizeWindow();
	bool RegisterMyClass();
	bool MakeWindow();

	void Run();

//Setter
	void SetIsRunning(const bool& isRunning) { _isRunnig = isRunning; }
	void SetIsRunning(bool&& isRunning) { _isRunnig = std::move(isRunning); }

public:
	inline static int		screenPosX = 0;
	inline static int		screenPosY = 0;
	inline static int		screenWidth = 0;
	inline static int		screenHeight = 0;

private:
	HINSTANCE _hInstance = NULL;

	bool	_isRunnig = true;
	bool	_bShowMenue = false;
	DWORD	_dwStyle;
	DWORD	_dwExStyle;

	WCHAR	_szTitle[MAX_LOAD_STRING];                  // 제목 표시줄 텍스트입니다.
	WCHAR	_szWindowClass[MAX_LOAD_STRING];            // 기본 창 클래스 이름입니다.
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

inline bool g_isResizing = false;
