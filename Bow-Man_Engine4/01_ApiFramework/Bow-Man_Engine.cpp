#include "stdafx.h"
#include "Bow-Man_Engine.h"
#include "Window.h"

// Bow-Man_Engine4.cpp: 응용 프로그램의 진입점을 정의합니다.
//

extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	

	gMemStatus.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&gMemStatus);
	DWORDLONG totalVirtualMem = gMemStatus.ullTotalPageFile;
	DWORDLONG virtualMemUsed = gMemStatus.ullTotalPageFile - gMemStatus.ullAvailPageFile;
	DWORDLONG totalPhysMem = gMemStatus.ullTotalPhys;
	DWORDLONG physMemUsed = gMemStatus.ullTotalPhys - gMemStatus.ullAvailPhys;

	GetProcessMemoryInfo(GetCurrentProcess(), &gMc, sizeof(gMc));
	SIZE_T virtualMemUsedByMe = gMc.PagefileUsage;
	SIZE_T physMemUsedByMe = gMc.WorkingSetSize;

	


	UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);


	//Enable Drag Drop in Window
	//DragAcceptFiles(gHwnd, TRUE);

	//wcout 한글 출력
	std::wcout.imbue(std::locale("kor"));

	srand(static_cast<unsigned int>(time(0)));

	gHinstance = hInstance;


	Window win;
	win.Open();
	win.Run();
	win.Close();
}

#if defined(NDEBUG) && defined(__GNUC__)
#define U_ASSERT_ONLY __attribute__((unused))
#else
#define U_ASSERT_ONLY
#endif


//LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
//{
//	switch (message)
//	{
//	case WM_CREATE:
//
//		break;
//	case WM_PAINT:
//	{
//		PAINTSTRUCT ps;
//		HDC hdc = BeginPaint(hWnd, &ps);
//		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다.
//		EndPaint(hWnd, &ps);
//	}
//	break;
//	default:
//		return DefWindowProc(hWnd, message, wParam, lParam);
//	}
//	return 0;
//}
