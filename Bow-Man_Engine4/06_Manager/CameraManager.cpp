#include "stdafx.h"
#include "CameraManager.h"

#ifdef DIRECT_X_11_RENDERER

#include "Behavior.h"
#include "Camera.h"
#include "Entity.h"

//	void AddCamera(const std::shared_ptr<Camera>& camera) 

void CameraManager::AddCamera(const std::shared_ptr<Behavior>& behavior)
{
	std::shared_ptr<Camera> camera = std::dynamic_pointer_cast<Camera>(behavior);

	if (camera != nullptr) 
		_cameras.push_back(std::move(camera));
	else 
		cout << "Error : " << __FUNCTION__ << " - Camera_Behavior is Nullptr" << endl;
}

#endif