#ifdef DIRECT_X_11_RENDERER

#pragma once

class IShader;
class ShaderPack;

#define ShaderMgr CShaderManager::GetInstance()

class CShaderManager
{
public:
	static CShaderManager* GetInstance() {
		if (_instance == nullptr) {
			_instance = new CShaderManager();
			_instance->Initialize();
		}
		return _instance;
	}

	void Initialize();

	void LoadShader(const ComPtr<ID3D11Device>& device);

	void ReleaseShader() {

	}

	void Activate(const ComPtr<ID3D11DeviceContext>& deviceContext, const std::wstring& shaderName);

	void GetShaderInputLayout(const std::wstring& shaderName);
	std::shared_ptr<ShaderPack> GetShaderPack(const std::wstring& shaderName);

private:
	CShaderManager() { }
	~CShaderManager() { }

private:
	inline static CShaderManager * _instance;
	json _shaderData;

	std::unordered_map<std::wstring, std::shared_ptr <IShader>>		_dicShader;
	std::unordered_map<std::wstring, std::shared_ptr <ShaderPack>>	_dicShaderPack;
};


#endif