#ifdef DIRECT_X_11_RENDERER

#pragma once

#define RenderStateMgr RenderStateManager::GetInstance()

class RenderStatePack
{
public:
	void SetStatePack(const std::wstring& nameDepthStencilstate, const std::wstring& nameRaserizerState, const std::wstring& nameBlendState);

	void SetStatePackToGPU(const ComPtr<ID3D11DeviceContext>& deviceContext);

private:
	ComPtr <ID3D11RasterizerState>		_rasterizerState;
	ComPtr <ID3D11DepthStencilState>	_depthStencilState;
	ComPtr <ID3D11BlendState>			_blendState;
};

class RenderStateManager
{
public:
	static RenderStateManager* GetInstance() {
		if (_instance == nullptr) {
			_instance = new RenderStateManager();
			_instance->Initialize();
		}
		return _instance;
	}
	void Initialize();

	void LoadDataFromFIle(const ComPtr<ID3D11Device>& device);

	ComPtr <ID3D11DepthStencilState> GetDepthStencilState(const std::wstring& nameDepthStencilState)
	{
		try {
			return _depthStencilStates.at(nameDepthStencilState);
		}
		catch (std::out_of_range) {
			cout << "Error not Found : " << __FUNCTION__ << endl;
			return nullptr;
		}
	}

	ComPtr <ID3D11RasterizerState> GetRaserizerState(const std::wstring& nameRaserizerState)
	{
		try {
			return _rasterizerStates.at(nameRaserizerState);
		}
		catch (std::out_of_range) {
			cout << "Error not Found : " << __FUNCTION__ << endl;
			return nullptr;
		}
	}

	ComPtr <ID3D11BlendState> GetBlendState(const std::wstring& nameBlendState) {
		try {
			return _blendStates.at(nameBlendState);
		}
		catch (std::out_of_range) {
			cout << "Error not Found : " << __FUNCTION__ << endl;
			return nullptr;
		}
	}

	std::shared_ptr <RenderStatePack> GetRenderStatePack(const std::wstring& nameRenderStatePack) {
		try {
			return _shaderPacks.at(nameRenderStatePack);
		}
		catch (std::out_of_range) {
			cout << "Error not Found : " << __FUNCTION__ << endl;
			return nullptr;
		}
	}

private:
	RenderStateManager() { }
	~RenderStateManager() { }

private:
	inline static RenderStateManager * _instance;

	std::unordered_map<std::wstring, ComPtr <ID3D11DepthStencilState>>	_depthStencilStates;
	std::unordered_map<std::wstring, ComPtr <ID3D11RasterizerState>>	_rasterizerStates;
	std::unordered_map<std::wstring, ComPtr <ID3D11BlendState>>			_blendStates;
	std::unordered_map<std::wstring, std::shared_ptr <RenderStatePack>>	_shaderPacks;
};

#endif