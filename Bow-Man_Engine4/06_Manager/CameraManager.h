#pragma once

#ifdef DIRECT_X_11_RENDERER

#define CameraMgr CameraManager::GetInstance()

class Camera;
class Behavior;

class CameraManager
{
public:
	static CameraManager* GetInstance() {
		if (_instance == nullptr) {
			_instance = new CameraManager();
		}
		return _instance;
	}

	void AddCamera(const std::shared_ptr<Behavior>& behavior);

	std::shared_ptr<Camera> GetCamera(int index)
	{
		std::shared_ptr<Camera> retVal;
		if (0 <= index && index < _cameras.size())
			retVal = _cameras[index];
		else
			retVal = nullptr;
		return retVal;
	}

private:
	CameraManager() { }
	~CameraManager() { }

private:
	inline static CameraManager * _instance;
	std::vector<std::shared_ptr<Camera>> _cameras;

};

#endif