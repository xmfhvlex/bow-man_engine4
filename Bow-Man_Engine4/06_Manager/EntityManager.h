#pragma once

#ifdef DIRECT_X_11_RENDERER

#define EntityMgr EntityManager::GetInstance()

class Entity;

class EntityManager
{
public:
	static EntityManager* GetInstance() {
		if (_instance == nullptr) {
			_instance = new EntityManager();
		}
		return _instance;
	}

	void Destroy();

	std::shared_ptr<Entity> NewEntity() {
		auto entity = std::make_shared<Entity>();
		_entities.push_back(entity);
		return entity;
	}

	void Update();

private:
	EntityManager() { }
	~EntityManager() { }

private:
	inline static EntityManager * _instance;
	std::list<std::shared_ptr<Entity>> _entities;

};

#endif