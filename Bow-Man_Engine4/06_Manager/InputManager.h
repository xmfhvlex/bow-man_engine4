#pragma once

#ifdef DIRECT_X_11_RENDERER

#define InputMgr		InputManager::GetInstance()
//#define FOR_NOTEBOOK_ERROR

enum MOUSE_STATUS {
	LEFT,
	RIGHT,
	WHEEL,
};

class InputManager
{
public:
	static InputManager* GetInstance() { 
		if (m_pInstance == nullptr) {
			m_pInstance = new InputManager;
		}
		return m_pInstance; 
	}

	void Initialize(HWND hWnd);

	void Release() {}

	void AlterMouseSencitivity(float fAmount) {
		m_fMouseSensitivity += fAmount;
		if (m_fMouseSensitivity < 1) m_fMouseSensitivity = 1;
		else if (m_fMouseSensitivity > 100) m_fMouseSensitivity = 100;
	}

	//현재 마우스의 위치와 마우스 기준 위치를 비교하여 실수값을 뽑아낸다.
	void Update(float fElapsedTime);

	bool IsRightClicked() {
		return m_bRightClick;
	}
	bool IsLeftClicked() {
		return m_bLeftClick;
	}
	bool IsMouseActive() {
		return m_bMouseActive;
	}

	void MouseActive(bool bFlag);
	void MouseOnOff();

	void SetMousePivot() {
		m_PivotMousePos.x = static_cast<int>((m_windowRect.right - m_windowRect.left) / 2);
		m_PivotMousePos.y = static_cast<int>((m_windowRect.bottom - m_windowRect.top) / 2);
	}
	void SetMouseToCenter();

	const POINT& GetMousePos() const {
		return m_NowMousePos;
	}
	DWORD GetPressedKey() {
		return m_dwPressedKey;
	}
	float GetXDelta() {
		return m_fXDelta;
	}
	float GetYDelta() {
		return m_fYDelta;
	}
	bool MouseInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	InputManager() {}
	~InputManager() {}

private:
	inline static InputManager* m_pInstance = nullptr;

	HWND	m_hWnd;
	POINT	m_ScreenPos;
	POINT	m_PivotMousePos;
	POINT	m_PreMousePos;
	POINT	m_NowMousePos;

	RECT	m_windowRect;

	float	m_fXDelta;
	float	m_fYDelta;

	float	m_fMouseSensitivity;

	bool	m_bKeyboardActive;
	bool	m_bMouseActive;
	bool	m_bRightClick;
	bool	m_bLeftClick;
	bool	m_bWheelClick;

	DWORD	m_dwPressedKey;
};

#endif