#include "stdafx.h"
#include "BehaviorManager.h"


#ifdef DIRECT_X_11_RENDERER
#include "DirectX11Device.h"

#include "Behavior.h"
#include "Renderer.h"
#include "Entity.h"

#include "LightManager.h"

void BehaviorManager::Destroy() {

	_updateMethodList.clear();
	_phyicsMethodList.clear();
	for (auto lstRenderMethod : _renderMethod)
		lstRenderMethod.second.clear();
	_renderMethod.clear();
}

void BehaviorManager::Update()
{
	//Execute Update Behavior List
	_updateMethodList.remove_if([&](const auto& method)
	{
		//Remove Method if Entity is Destroied
		if (method.first->IsDestroied() == true) {
			return true;
		}
		else {
			//Method Execute
			method.second();
		}
		return false;
	});
}

void BehaviorManager::Render(RenderLayer layer)
{
	Renderer::_entityIndex = 0;

	try {
		auto& lstMethod = _renderMethod.at(layer);

		lstMethod.remove_if([](const auto& method)
		{
			//Remove Method if Entity is Destroied
			if (method.first->IsDestroied() == true) {
				return true;
			}
			else {
				//Method Execute
				Renderer::_entityIndex++;
				method.second();
			}
			return false;
		});
	}
	catch (std::out_of_range) {
		cout << "Error not Found : " << __FUNCTION__ << endl;
	}
}

#endif