#include "stdafx.h"
#include "LightManager.h"

#ifdef DIRECT_X_11_RENDERER

#include "DirectX11Device.h"

void LightManager::Initialize() 
{
	CreateConstantBuffers<LightDataList>(Device.Get(), _lightListBuffer.ReleaseAndGetAddressOf());
}

void LightManager::AddLight(const LightData & lightData) {
	_lightDataList.gArrDirLight[_lightDataList.gLightPivot] = lightData;
	_lightDataList.gLightPivot++;
}

void LightManager::SetLightToGPU()
{
	UpdateConstantBuffer(ImmediateContext.Get(), _lightListBuffer.Get(), _lightDataList);
	ImmediateContext->PSSetConstantBuffers(ECBuffer::LIGHT, 1, _lightListBuffer.GetAddressOf());
	ImmediateContext->CSSetConstantBuffers(ECBuffer::LIGHT, 1, _lightListBuffer.GetAddressOf());
	_lightDataList.gLightPivot = 0;
}

#endif