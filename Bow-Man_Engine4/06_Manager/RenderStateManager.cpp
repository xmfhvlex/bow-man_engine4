#include "stdafx.h"
#include "RenderStateManager.h"

#ifdef DIRECT_X_11_RENDERER

#include "Utility.h"
#include "DirectX11Device.h"


void RenderStatePack::SetStatePack(const std::wstring & nameDepthStencilstate, const std::wstring & nameRaserizerState, const std::wstring & nameBlendState) {
	_rasterizerState = RenderStateMgr->GetRaserizerState(nameRaserizerState);
	_depthStencilState = RenderStateMgr->GetDepthStencilState(nameDepthStencilstate);
	_blendState = RenderStateMgr->GetBlendState(nameBlendState);
}

void RenderStatePack::SetStatePackToGPU(const ComPtr<ID3D11DeviceContext>& deviceContext)
{
	//deviceContext->OMSetRenderTargets(1, rtvHDR.GetAddressOf(), _dsvDepthStencil.Get());

	//D3D11_RECT rect{0, 0, 2300, 2300};
	//deviceContext->RSSetScissorRects(1, &rect);


	deviceContext->RSSetState(_rasterizerState.Get());
	deviceContext->OMSetDepthStencilState(_depthStencilState.Get(), 1);
	deviceContext->OMSetBlendState(_blendState.Get(), NULL, 0xffffffff);
}

void RenderStateManager::Initialize()
{
	LoadDataFromFIle(Device);
}

void RenderStateManager::LoadDataFromFIle(const ComPtr<ID3D11Device>& device)
{
	_depthStencilStates[L""] = nullptr;
	_rasterizerStates[L""] = nullptr;
	_blendStates[L""] = nullptr;

	ComPtr<ID3D11DepthStencilState> depthStencilState;

	D3D11_DEPTH_STENCIL_DESC DepthStencilDesc;
	InitMemory(DepthStencilDesc);
	DepthStencilDesc.DepthEnable = true;
	DepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	DepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	device->CreateDepthStencilState(&DepthStencilDesc, depthStencilState.ReleaseAndGetAddressOf());
	_depthStencilStates[L"ds_skybox"] = depthStencilState;


	InitMemory(DepthStencilDesc);
	DepthStencilDesc.DepthEnable = true;
	DepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	DepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	device->CreateDepthStencilState(&DepthStencilDesc, depthStencilState.ReleaseAndGetAddressOf());
	_depthStencilStates[L"ds_depth"] = depthStencilState;

	InitMemory(DepthStencilDesc);
	DepthStencilDesc.DepthEnable = true;
	DepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	DepthStencilDesc.DepthFunc = D3D11_COMPARISON_EQUAL;
	device->CreateDepthStencilState(&DepthStencilDesc, depthStencilState.ReleaseAndGetAddressOf());
	_depthStencilStates[L"ds_occlusion"] = depthStencilState;



	auto renderStatePack = std::make_shared<RenderStatePack>();
	renderStatePack->SetStatePack(L"ds_skybox", L"", L"");
	_shaderPacks[L"skybox_pass"] = renderStatePack;

	renderStatePack = std::make_shared<RenderStatePack>();
	renderStatePack->SetStatePack(L"ds_depth", L"", L"");
	_shaderPacks[L"depth_pass"] = renderStatePack;

	renderStatePack = std::make_shared<RenderStatePack>();
	renderStatePack->SetStatePack(L"ds_occlusion", L"", L"");
	_shaderPacks[L"occlusion_pass"] = renderStatePack;
}

#endif