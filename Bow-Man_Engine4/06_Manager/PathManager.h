#pragma once

#define PathMgr PathManager::GetInstance()
#define Path(X) PathManager::GetInstance()->GetPath(X)
#define PathA(X) PathManager::GetInstance()->GetPathA(X)

class PathManager
{
public:
	static PathManager* GetInstance() {
		if (_instance == nullptr) {
			_instance = new PathManager();
			_instance->Load();
		}
		return _instance;
	}

	void Load() {
		//json data;
		//data["dat"] = "hello";
		//std::ofstream output_file("../Resource/Shader/HLSL/option2.json");
		//output_file << std::setw(4) << data;
		//output_file.close();
	

		std::ifstream input_file(_T("../Resource/path_info.json"));
		input_file >> _pathData;
		input_file.close();

		for (const auto& path : _pathData.items()) {
			std::string strKey = path.key();

			std::string strPath = path.value();
			std::wstring wstrPath;
			wstrPath.assign(strPath.begin(), strPath.end());

			_dicCharPath[strKey] = strPath;
			_dicWcharPath[strKey] = wstrPath;
		}
	}

	const std::string& GetPathA(const std::string& str)
	{
		auto itrtr = _dicCharPath.find(str);

		if (_dicCharPath.end() == itrtr) {
			return "";
		}

		return itrtr->second;
	}

	const std::wstring& GetPath(const std::string& str) 
	{	
		auto itrtr = _dicWcharPath.find(str);

		if (_dicWcharPath.end() == itrtr) {
			return _T("");
		}

		return itrtr->second;
	}

	const std::wstring& GetPath(const std::wstring& wstr)
	{
		std::string str;
		str.assign(wstr.begin(), wstr.end());

		auto itrtr = _dicWcharPath.find(str);

		if (_dicWcharPath.end() == itrtr) {
			return _T("");
		}

		return itrtr->second;
	}

private:
	PathManager() { }
	~PathManager() { }

private:
	inline static PathManager *	_instance;
	json _pathData;

	std::unordered_map<std::string, std::string>	_dicCharPath;
	std::unordered_map<std::string, std::wstring>	_dicWcharPath;
};

