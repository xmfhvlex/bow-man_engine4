#pragma once

#define BehaviorMgr BehaviorManager::GetInstance()

class Behavior;
class Entity;

using UpdateMethodData = std::pair<std::shared_ptr<Entity>, std::function<bool(void)>>;
using PhysicsMethodData = std::pair<std::shared_ptr<Entity>, std::function<bool(void)>>;
using RenderMethodData = std::pair<std::shared_ptr<Entity>, std::function<bool(void)>>;

class BehaviorManager
{
public:
	static BehaviorManager* GetInstance() {
		if (_instance == nullptr) {
			_instance = new BehaviorManager();
		}
		return _instance;
	}

	void Destroy();

	void Update();
	void Render(RenderLayer layer);

private:
	BehaviorManager() { }
	~BehaviorManager() { }

private:
	inline static BehaviorManager * _instance;

public:
	std::list<UpdateMethodData>					_updateMethodList;
	std::list<std::function<bool(float)>>		_phyicsMethodList;
	std::unordered_map<RenderLayer, std::list<RenderMethodData>> _renderMethod;
};