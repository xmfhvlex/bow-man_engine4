#include "stdafx.h"
#include "EntityManager.h"

#ifdef DIRECT_X_11_RENDERER

#include "Entity.h"

#include "Renderer.h"
#include "MeshRenderer.h"

void EntityManager::Destroy() {
	for (auto entity : _entities)
		entity->Release();
	_entities.clear();
}

void EntityManager::Update()
{
	_entities.remove_if([](const auto& entity) {
		if (entity->IsDestroied() == true)
			return true;
		else
			return false;
	});
}

#endif