#include "stdafx.h"
#include "ShaderManager.h"

#ifdef DIRECT_X_11_RENDERER

#include "IShader.h"
#include "PathManager.h"

void CShaderManager::Initialize() {
	//json data;
	//data["dat"] = "hello";
	//std::ofstream output_file("../Resource/Shader/HLSL/option2.json");
	//output_file << std::setw(4) << data;
	//output_file.close();

	std::ifstream input_file(Path("hlsl_shader") + _T("shader_info.json"));
	input_file >> _shaderData;
	input_file.close();
}

void CShaderManager::LoadShader(const ComPtr<ID3D11Device>& device)
{
	for (const auto& shaderData : _shaderData.items()) {
		std::string strShaderPack = shaderData.key();
		std::wstring wstrShaderPack;
		wstrShaderPack.assign(strShaderPack.begin(), strShaderPack.end());

		std::shared_ptr<ShaderPack> shaderPack = std::make_shared<ShaderPack>(wstrShaderPack);
		for (const auto& shader : shaderData.value()) {

			std::string strFile = shader["file"].get<std::string>();
			std::wstring wstrFile;
			wstrFile.assign(strFile.begin(), strFile.end());

			auto shaderDirName = Path("hlsl_shader") + wstrFile + _T(".cso");
			std::string strType = shader["type"].get<std::string>();
			std::wstring wstrType;
			wstrType.assign(strType.begin(), strType.end());

			auto itrtr = _dicShader.find(wstrFile);
			if (itrtr == _dicShader.end()) {
				if (wstrType == L"VS") {
					//json item = shader.find("layout");
					_dicShader[wstrFile] = std::make_shared<CVertexShader>(device, shaderDirName, shader["layout"]);
				}
				else if (wstrType == L"GS") {

				}
				else if (wstrType == L"HS") {

				}
				else if (wstrType == L"DS") {

				}
				else if (wstrType == L"PS") {
					_dicShader[wstrFile] = std::make_shared<CPixelShader>(device, shaderDirName);
				}
				else if (wstrType == L"CS") {
					_dicShader[wstrFile] = std::make_shared<CComputeShader>(device, shaderDirName);
				}
			}
			shaderPack->AddShader(_dicShader[wstrFile]);

		}
		_dicShaderPack[wstrShaderPack] = shaderPack;
	}
}

void CShaderManager::Activate(const ComPtr<ID3D11DeviceContext>& deviceContext, const std::wstring & shaderName)
{
	_dicShaderPack[shaderName]->Activate(deviceContext);
}

void CShaderManager::GetShaderInputLayout(const std::wstring & shaderName) {
	std::string strShaderName;

	//auto shader = _shaderData[shaderName];
}

std::shared_ptr<ShaderPack> CShaderManager::GetShaderPack(const std::wstring & shaderName) {
	try {
		return _dicShaderPack.at(shaderName);
	}
	catch (std::out_of_range) {
		return nullptr;
	}
}

#endif