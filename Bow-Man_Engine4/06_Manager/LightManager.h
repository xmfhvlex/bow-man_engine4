#ifdef DIRECT_X_11_RENDERER

#pragma once

constexpr int MAX_LIGHT_COUNT = 100;

constexpr int DIR_LIGHT = 0;
constexpr int POINT_LIGHT = 1;
constexpr int SPOT_LIGHT = 2;

struct LightData
{
	XMFLOAT3	positionWS;
	float		range;
	//----------------------
	XMFLOAT3	positionVS;
	float		spotlightAngle;
	//----------------------
	XMFLOAT3	directionWS;
	float		intensity;
	//----------------------
	XMFLOAT3	directionVS;
	UINT		type;
	//----------------------
	XMFLOAT3	color;
	bool		enabled;
	//----------------------
};

struct LightDataList
{
	LightData gArrDirLight[MAX_LIGHT_COUNT];
	int gLightPivot = 0;
	int padding[3];
};

#define LightMgr LightManager::GetInstance()
class LightManager
{
public:
	static LightManager* GetInstance() {
		if (_instance == nullptr) {
			_instance = new LightManager();
			_instance->Initialize();
		}
		return _instance;
	}

	void AddLight(const LightData& lightData);

	void SetLightToGPU();

private:
	LightManager() { }
	~LightManager() { }

	void Initialize();

private:
	inline static LightManager *		_instance;

	inline static LightDataList			_lightDataList;
	inline static ComPtr <ID3D11Buffer>	_lightListBuffer;
};

#endif