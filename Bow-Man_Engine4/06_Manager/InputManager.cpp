#include "stdafx.h"
#include "InputManager.h"

#ifdef DIRECT_X_11_RENDERER

#include "Window.h"

//현재 마우스의 위치와 마우스 기준 위치를 비교하여 실수값을 뽑아낸다.

void InputManager::Initialize(HWND hWnd) {
	m_hWnd = hWnd;
	GetClientRect(hWnd, &m_windowRect);

	m_PivotMousePos.x = static_cast<int>((m_windowRect.right - m_windowRect.left) / 2);
	m_PivotMousePos.y = static_cast<int>((m_windowRect.bottom - m_windowRect.top) / 2);

	m_bKeyboardActive = true;
	m_bMouseActive = true;
	m_bRightClick = false;
	m_bLeftClick = false;
	m_bWheelClick = false;

	m_fMouseSensitivity = 5;
}

void InputManager::Update(float fElapsedTime) 
{
	m_fXDelta = 0;
	m_fYDelta = 0;

	if (m_bMouseActive == true)
		return;

	POINT nowPos;
	GetCursorPos(&nowPos);

	m_fXDelta = (nowPos.x - m_PreMousePos.x) / m_fMouseSensitivity;
	m_fYDelta = (nowPos.y - m_PreMousePos.y) / m_fMouseSensitivity;

	//cout << nowPos.x << " " << nowPos.y << " | " << Window::screenWidth << " " << Window::screenHeight << endl;

	if (nowPos.x == 0) {
		nowPos = POINT{ Window::screenWidth - 2, nowPos.y };
		SetCursorPos(nowPos.x, nowPos.y);
	}
	//else if (nowPos.x == Window::screenWidth-1) {
	//	nowPos = POINT{ 1, nowPos.y };
	//	SetCursorPos(nowPos.x, nowPos.y);
	//}
	//if (nowPos.y == 0) {
	//	nowPos = POINT{ nowPos.x, Window::screenHeight - 2 };
	//	SetCursorPos(nowPos.x, nowPos.y);
	//}
	//else if (nowPos.y == Window::screenHeight - 1) {
	//	nowPos = POINT{ nowPos.x, 1 };
	//	SetCursorPos(nowPos.x, nowPos.y);
	//}

	m_PreMousePos = nowPos;


	//if (!m_bKeyboardActive) return;
	//m_dwPressedKey = 0;
	//UCHAR pKeyBuffer[256];
	//ZeroMemory(pKeyBuffer, 256);
	//if (GetKeyboardState(pKeyBuffer)) {
	//	if (pKeyBuffer[VK_W] & 0xF0)		m_dwPressedKey |= KEY_FOWARD;
	//	if (pKeyBuffer[VK_S] & 0xF0)		m_dwPressedKey |= KEY_BACKWARD;
	//	if (pKeyBuffer[VK_A] & 0xF0)		m_dwPressedKey |= KEY_LEFTWARD;
	//	if (pKeyBuffer[VK_D] & 0xF0)		m_dwPressedKey |= KEY_RIGHTWARD;
	//	if (pKeyBuffer[VK_E] & 0xF0)		m_dwPressedKey |= KEY_UPWARD;
	//	if (pKeyBuffer[VK_Q] & 0xF0)		m_dwPressedKey |= KEY_DOWNWARD;
	//	if (pKeyBuffer[VK_SHIFT] & 0xF0)	m_dwPressedKey |= KEY_RUN;
	//	if (pKeyBuffer[VK_SPACE] & 0xF0);
	//}
}

void InputManager::MouseActive(bool bFlag) {
	if (bFlag == false) {
		m_bMouseActive = false;
		SetMousePivot();
		SetCapture(m_hWnd);
		SetCursor(NULL);
		SetMouseToCenter();
	}
	else {
		m_bMouseActive = true;
		ReleaseCapture();
	//	SetCursor(LoadCursor(0, MAKEINTRESOURCE(IDC_CURSOR)));
	}
}

void InputManager::MouseOnOff() {
	if (m_bMouseActive) {
		m_bMouseActive = false;
		SetMousePivot();
		SetCapture(m_hWnd);
		SetCursor(NULL);
		SetMouseToCenter();
	}
	else {
		m_bMouseActive = true;
		ReleaseCapture();
	//	SetCursor(LoadCursor(0, MAKEINTRESOURCE(IDC_CURSOR2)));
	}
}

void InputManager::SetMouseToCenter() {
//	SetCursorPos(m_PivotMousePos.x + Window::nScreenWidth, m_PivotMousePos.y + Window::nScreenHeight);
	SetCursorPos(m_PivotMousePos.x, m_PivotMousePos.y);
}

bool InputManager::MouseInputProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	m_NowMousePos = POINT{ LOWORD(lParam) - Window::screenPosX, HIWORD(lParam) - Window::screenPosY };

	switch (message) {
//	case WM_MOUSEMOVE:
//		if (!m_bMouseActive) {
//#ifdef FOR_NOTEBOOK_ERROR
//			if (abs(m_PivotMousePos.y - m_NowMousePos.y) < 2 && abs(m_PivotMousePos.x - m_NowMousePos.x) < 2) break;
//#else
//			if (m_NowMousePos.x == m_PivotMousePos.x && m_NowMousePos.y == m_PivotMousePos.y) break;
//#endif
//			m_fXDelta = static_cast<float>(m_NowMousePos.x - (m_PivotMousePos.x)) / m_fMouseSensitivity;
//			m_fYDelta = static_cast<float>(m_NowMousePos.y - (m_PivotMousePos.y)) / m_fMouseSensitivity;
//			SetCursorPos(m_PivotMousePos.x, m_PivotMousePos.y);
//		}
//		else {
//			if (m_bLeftClick) {
//#ifdef FOR_NOTEBOOK_ERROR
//				if (abs(m_PreMousePos.y - m_NowMousePos.y) < 2 && abs(m_PreMousePos.x - m_NowMousePos.x) < 2) break;
//#else
//				if (m_PreMousePos.x == m_NowMousePos.x && m_PreMousePos.y == m_NowMousePos.y) break;
//#endif
//				m_fXDelta = static_cast<float>(m_NowMousePos.x - m_PreMousePos.x) / m_fMouseSensitivity;
//				m_fYDelta = static_cast<float>(m_NowMousePos.y - m_PreMousePos.y) / m_fMouseSensitivity;
//				SetCursorPos(m_PreMousePos.x, m_PreMousePos.y);
//				SetCapture(m_hWnd);
//				SetCursor(NULL);
//			}
//		}
//		break;
	case WM_LBUTTONDOWN:
		m_bLeftClick = true;
		m_bMouseActive = false;
	//	if (m_bMouseActive) {
			m_PreMousePos = m_NowMousePos;
	//	}
		break;
	case WM_LBUTTONUP:
		m_bLeftClick = false;
		m_bMouseActive = true;
	//	if (m_bMouseActive) {
	//		SetCapture(m_hWnd);
	//		ReleaseCapture();
	//	//	SetCursor(LoadCursor(0, MAKEINTRESOURCE(IDC_CURSOR2)));
	//	}
	//	else {
	//	}
		break;
	case WM_LBUTTONDBLCLK:
		break;
	case WM_RBUTTONDOWN:
		m_bRightClick = true;
		break;
	case WM_RBUTTONUP:
		m_bRightClick = false;
		break;
	case WM_RBUTTONDBLCLK:
		break;
	case WM_MOUSEWHEEL:
		break;
	case WM_MBUTTONDOWN:
		m_bWheelClick = true;
		MouseOnOff();
		break;
	case WM_MBUTTONUP:
		m_bWheelClick = false;
		break;
	case WM_MBUTTONDBLCLK:
		break;
	case WM_MOUSELEAVE:
		break;
	default:
		return false;
	}
	return true;
}


#endif